<?php

/*if(!file_exists('typo3conf/configuration/personalConfiguration.php')){
	echo 'Es existiert keine personalConfiguration! <br />';
	echo 'Bitte lege unter typo3conf/configuration die Datei personalConfiguration.php an.<br />';
	echo 'Fülle sie mit folgenden Zeilen, die nach Bedarf angepasst werden können:<br /><br/>';
	echo '&lt;?php <br /> $local = FALSE; <br /> $remoteHost = "192.168.57.27"; <br />?&gt;';
	exit;
}*/
require_once('configuration/personalConfiguration.php');

if(isset($local) && isset($remoteHost)){
	if ($local === FALSE){
		$GLOBALS['TYPO3_CONF_VARS']['DB']['database'] = 'hpwebseite';
		$GLOBALS['TYPO3_CONF_VARS']['DB']['host']     = $remoteHost;
		$GLOBALS['TYPO3_CONF_VARS']['DB']['username'] = 'root';
		$GLOBALS['TYPO3_CONF_VARS']['DB']['password'] = '';
	}
}


if(isset($securityLevel)){
	$GLOBALS['TYPO3_CONF_VARS']['BE']['loginSecurityLevel'] = $securityLevel;
}