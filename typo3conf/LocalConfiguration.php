<?php
return array(
	'BE' => array(
		'installToolPassword' => '$1$helSaHoW$Yg0jibG3AXTuygK7xCRGN0',
		'loginSecurityLevel' => 'rsa',
	),
	'DB' => array(
		'database' => 'hpwebseite',
		'extTablesDefinitionScript' => 'extTables.php',
		'host' => '192.168.57.27',
		'password' => '',
		'port' => 3306,
		'username' => 'root',
	),
	'EXT' => array(
		'extConf' => array(
			'ejw_calendar' => 'a:0:{}',
			'ejwintern' => 'a:0:{}',
			'hp_webseite' => 'a:0:{}',
			'indexed_search' => 'a:18:{s:8:"pdftools";s:9:"/usr/bin/";s:8:"pdf_mode";s:2:"20";s:5:"unzip";s:9:"/usr/bin/";s:6:"catdoc";s:9:"/usr/bin/";s:6:"xlhtml";s:9:"/usr/bin/";s:7:"ppthtml";s:9:"/usr/bin/";s:5:"unrtf";s:9:"/usr/bin/";s:9:"debugMode";s:1:"0";s:18:"fullTextDataLength";s:1:"0";s:23:"disableFrontendIndexing";s:1:"0";s:21:"enableMetaphoneSearch";s:1:"1";s:6:"minAge";s:2:"24";s:6:"maxAge";s:1:"0";s:16:"maxExternalFiles";s:1:"5";s:26:"useCrawlerForExternalFiles";s:1:"0";s:11:"flagBitMask";s:3:"192";s:16:"ignoreExtensions";s:0:"";s:17:"indexExternalURLs";s:1:"0";}',
			'news' => 'a:13:{s:29:"removeListActionFromFlexforms";s:1:"2";s:20:"pageModuleFieldsNews";s:313:"LLL:EXT:news/Resources/Private/Language/locallang_be.xml:pagemodule_simple=title,datetime;LLL:EXT:news/Resources/Private/Language/locallang_be.xml:pagemodule_advanced=title,datetime,teaser,category;LLL:EXT:news/Resources/Private/Language/locallang_be.xml:pagemodule_complex=title,datetime,teaser,category,archive;";s:24:"pageModuleFieldsCategory";s:17:"title,description";s:11:"archiveDate";s:4:"date";s:13:"prependAtCopy";s:1:"1";s:6:"tagPid";s:1:"1";s:25:"showMediaDescriptionField";s:1:"0";s:12:"rteForTeaser";s:1:"0";s:22:"contentElementRelation";s:1:"1";s:13:"manualSorting";s:1:"0";s:19:"categoryRestriction";s:4:"none";s:12:"showImporter";s:1:"0";s:24:"showAdministrationModule";s:1:"1";}',
			'phpmyadmin' => 'a:5:{s:12:"hideOtherDBs";s:1:"1";s:9:"uploadDir";s:21:"uploads/tx_phpmyadmin";s:10:"allowedIps";s:0:"";s:12:"useDevIpMask";s:1:"0";s:10:"ajaxEnable";s:1:"1";}',
			'pt_extbase' => 'a:0:{}',
			'pt_extlist' => 'a:0:{}',
			'pt_nivoslider' => 'a:0:{}',
			'saltedpasswords' => 'a:2:{s:3:"FE.";a:2:{s:7:"enabled";s:1:"1";s:21:"saltedPWHashingMethod";s:28:"tx_saltedpasswords_salts_md5";}s:3:"BE.";a:1:{s:21:"saltedPWHashingMethod";s:28:"tx_saltedpasswords_salts_md5";}}',
			'tt_news' => 'a:20:{s:13:"useStoragePid";s:1:"1";s:17:"requireCategories";s:1:"0";s:18:"useInternalCaching";s:1:"1";s:11:"cachingMode";s:6:"normal";s:13:"cacheLifetime";s:1:"0";s:13:"cachingEngine";s:8:"internal";s:11:"treeOrderBy";s:3:"uid";s:13:"prependAtCopy";s:1:"1";s:5:"label";s:5:"title";s:9:"label_alt";s:0:"";s:10:"label_alt2";s:0:"";s:15:"label_alt_force";s:1:"0";s:21:"categorySelectedWidth";s:0:"";s:17:"categoryTreeWidth";s:0:"";s:25:"l10n_mode_prefixLangTitle";s:1:"1";s:22:"l10n_mode_imageExclude";s:1:"1";s:20:"hideNewLocalizations";s:1:"0";s:24:"writeCachingInfoToDevlog";s:1:"0";s:23:"writeParseTimesToDevlog";s:1:"0";s:18:"parsetimeThreshold";s:3:"0.1";}',
			'yag' => 'a:3:{s:18:"hashFilesystemRoot";s:13:"typo3temp/yag";s:13:"origFilesRoot";s:13:"fileadmin/yag";s:38:"clearResFileCacheWithCacheClearCommand";s:5:"false";}',
		),
	),
	'FE' => array(
		'loginSecurityLevel' => 'rsa',
	),
	'INSTALL' => array(
		'wizardDone' => array(
			'TYPO3\CMS\Install\Updates\FileIdentifierHashUpdate' => 1,
			'TYPO3\CMS\Install\Updates\TceformsUpdateWizard' => 'tt_content:image,pages:media,pages_language_overlay:media',
			'TYPO3\CMS\Install\Updates\TruncateSysFileProcessedFileTable' => 1,
		),
	),
	'SYS' => array(
		'compat_version' => '6.2',
		'encryptionKey' => '8030d71e6bbdafaf8f11473c38ebb421dc387651de87a2a7bfa84546a9de0107aaf4a75bf572a0591fd4448ab9b8df40',
		'sitename' => 'New TYPO3 site',
	),
);
?>