<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package ejw_calendar
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_EjwCalendar_Controller_AppointmentController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * appointmentRepository
	 *
	 * @var Tx_EjwCalendar_Domain_Repository_AppointmentRepository
	 */
	protected $appointmentRepository;

	/**
	 * injectAppointmentRepository
	 *
	 * @param Tx_EjwCalendar_Domain_Repository_AppointmentRepository $appointmentRepository
	 * @return void
	 */
	public function injectAppointmentRepository(Tx_EjwCalendar_Domain_Repository_AppointmentRepository $appointmentRepository) {
		$this->appointmentRepository = $appointmentRepository;
	}

	/**
	 * categoryRepository
	 *
	 * @var Tx_EjwCalendar_Domain_Repository_CategoryRepository
	 */
	protected $categoryRepository;

	/**
	 * injectCategoryRepository
	 *
	 * @param Tx_EjwCalendar_Domain_Repository_CategoryRepository $categoryRepository
	 * @return void
	 */
	public function injectCategoryRepository(Tx_EjwCalendar_Domain_Repository_CategoryRepository $categoryRepository) {
		$this->categoryRepository = $categoryRepository;
	}

	/**
	 * organizerRepository
	 *
	 * @var Tx_EjwCalendar_Domain_Repository_OrganizerRepository
	 */
	protected $organizerRepository;

	/**
	 * injectOrganizerRepository
	 *
	 * @param Tx_EjwCalendar_Domain_Repository_OrganizerRepository $organizerRepository
	 * @return void
	 */
	public function injectOrganizerRepository(Tx_EjwCalendar_Domain_Repository_OrganizerRepository $organizerRepository) {
		$this->organizerRepository = $organizerRepository;
	}

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$appointmentsArray = array();
		$appointments = $this->appointmentRepository->findAllFutureAppointments();
		foreach($appointments as $appointment){ /** @var $appointment Tx_EjwCalendar_Domain_Model_Appointment*/
			$year = date('Y', $appointment->getStartdate());
			$month = date('m', $appointment->getStartdate());
			$appointmentsArray[$year][$month][] = $appointment;
		}
		$this->view->assign('years', $appointmentsArray);
		$this->view->assign('organizers', $this->organizerRepository->findAll());
		$this->view->assign('categories', $this->categoryRepository->findAll());
	}

	/**
	 * action show
	 *
	 * @param Tx_EjwCalendar_Domain_Model_Appointment $appointment
	 * @return void
	 */
	public function showAction(Tx_EjwCalendar_Domain_Model_Appointment $appointment) {

	}

}
?>