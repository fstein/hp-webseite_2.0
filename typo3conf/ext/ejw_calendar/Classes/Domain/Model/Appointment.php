<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package ejw_calendar
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_EjwCalendar_Domain_Model_Appointment extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * title
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $title;

	/**
	 * startdate
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $startdate;

	/**
	 * enddate
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $enddate;

	/**
	 * startmoment
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $startmoment;

	/**
	 * endmoment
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $endmoment;

	/**
	 * description
	 *
	 * @var string
	 */
	protected $description;


	/**
	 * place
	 *
	 * @var string
	 */
	protected $place;

	/**
	 * categories
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_EjwCalendar_Domain_Model_Category>
	 */
	protected $categories;

	/**
	 * organizers
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_EjwCalendar_Domain_Model_Organizer>
	 */
	protected $organizers;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->categories = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->organizers = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}


	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Adds a Category
	 *
	 * @param Tx_EjwCalendar_Domain_Model_Category $category
	 * @return void
	 */
	public function addCategory(Tx_EjwCalendar_Domain_Model_Category $category) {
		$this->categories->attach($category);
	}

	/**
	 * Removes a Category
	 *
	 * @param Tx_EjwCalendar_Domain_Model_Category $categoryToRemove The Category to be removed
	 * @return void
	 */
	public function removeCategory(Tx_EjwCalendar_Domain_Model_Category $categoryToRemove) {
		$this->categories->detach($categoryToRemove);
	}

	/**
	 * Returns the categories
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_EjwCalendar_Domain_Model_Category> $categories
	 */
	public function getCategories() {
		return $this->categories;
	}

	/**
	 * Sets the categories
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_EjwCalendar_Domain_Model_Category> $categories
	 * @return void
	 */
	public function setCategories(Tx_Extbase_Persistence_ObjectStorage $categories) {
		$this->categories = $categories;
	}

	/**
	 * Adds a Organizer
	 *
	 * @param Tx_EjwCalendar_Domain_Model_Organizer $organizer
	 * @return void
	 */
	public function addOrganizer(Tx_EjwCalendar_Domain_Model_Organizer $organizer) {
		$this->organizers->attach($organizer);
	}

	/**
	 * Removes a Organizer
	 *
	 * @param Tx_EjwCalendar_Domain_Model_Organizer $organizerToRemove The Organizer to be removed
	 * @return void
	 */
	public function removeOrganizer(Tx_EjwCalendar_Domain_Model_Organizer $organizerToRemove) {
		$this->organizers->detach($organizerToRemove);
	}

	/**
	 * Returns the organizers
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_EjwCalendar_Domain_Model_Organizer> $organizers
	 */
	public function getOrganizers() {
		return $this->organizers;
	}

	/**
	 * Sets the organizers
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_EjwCalendar_Domain_Model_Organizer> $organizers
	 * @return void
	 */
	public function setOrganizers(Tx_Extbase_Persistence_ObjectStorage $organizers) {
		$this->organizers = $organizers;
	}

	/**
	 * @param string $enddate
	 */
	public function setEnddate($enddate)
	{
		$this->enddate = $enddate;
	}

	/**
	 * @return string
	 */
	public function getEnddate()
	{
		return $this->enddate;
	}

	/**
	 * @param string $endmoment
	 */
	public function setEndmoment($endmoment)
	{
		$this->endmoment = $endmoment;
	}

	/**
	 * @return string
	 */
	public function getEndmoment()
	{
		return $this->endmoment;
	}

	/**
	 * @param string $startdate
	 */
	public function setStartdate($startdate)
	{
		$this->startdate = $startdate;
	}

	/**
	 * @return string
	 */
	public function getStartdate()
	{
		return $this->startdate;
	}

	/**
	 * @param string $startmoment
	 */
	public function setStartmoment($startmoment)
	{
		$this->startmoment = $startmoment;
	}

	/**
	 * @return string
	 */
	public function getStartmoment()
	{
		return $this->startmoment;
	}

	/**
	 * @param string $place
	 */
	public function setPlace($place)
	{
		$this->place = $place;
	}

	/**
	 * @return string
	 */
	public function getPlace()
	{
		return $this->place;
	}

}
?>