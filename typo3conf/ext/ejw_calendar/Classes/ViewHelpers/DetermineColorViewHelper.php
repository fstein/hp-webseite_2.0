<?php

class Tx_EjwCalendar_ViewHelpers_DetermineColorViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {

	/**
	 * @param Tx_EjwCalendar_Domain_Model_Appointment $appointment
	 * @return string
	 */
	public function render($appointment) {
		$organizers = $appointment->getOrganizers();
		if(count($organizers) == 0){
			return '#FFFFFF';
		} elseif (count($organizers) > 1){
			return '#F9A441';
		} else {
			foreach($organizers as $organizer){
				return $organizer->getColor();
			}
		}

	}

}