<?php

class Tx_EjwCalendar_ViewHelpers_RenderObjectsViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {

	/**
	 * @param object $objects
	 * @param string $delimiter
	 * @param string $space
	 * @return string
	 */
	public function render($objects, $delimiter, $space = " ") {
		try{
			$array = array();
			foreach ($objects as $object){
				$array[] = str_replace(" ", $space, $object->getName());
			}
			return implode($delimiter, $array);
		} catch (Exception $e){
			return '';
		}
	}
}