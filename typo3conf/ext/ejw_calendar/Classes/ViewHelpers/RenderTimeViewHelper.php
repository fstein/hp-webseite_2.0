<?php

class Tx_EjwCalendar_ViewHelpers_RenderTimeViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {

	/**
	 * @param Tx_EjwCalendar_Domain_Model_Appointment $appointment
	 * @return string
	 */
	public function render($appointment) {
		$startmonth = $this->convertMonth(date('m',$appointment->getStartdate()));
		$startday = date('d',$appointment->getStartdate());

		$endmonth = $this->convertMonth(date('m', $appointment->getEnddate()));
		$endday = date('d', $appointment->getEnddate());

		if($appointment->getStartmoment()){
			$startmoment = ', '.date('H:i', $appointment->getStartmoment()).' Uhr';
		} else {
			$startmoment = '';
		}
		if($appointment->getEndmoment()){
			$endmoment = ', '.date('H:i', $appointment->getEndmoment()).' Uhr';
		} else {
			$endmoment = '';
		}


		if(!$startmoment && !$endmoment){
			if ($startmonth == $endmonth){
				if ($startday == $endday){
					return $startday.'. '.$startmonth;
				}
				return $startday.'.–'.$endday.'. '.$startmonth;
			} else {
				return $startday.'. '.$startmonth.' – '.$endday.'. '.$endmonth;
			}
		}

		return $startday.'. '.$startmonth.$startmoment.'<br /> – '.$endday.'. '.$endmonth.$endmoment;
	}


	protected function convertMonth($month){
		switch($month){
			case '01':
				return 'Januar';
			case '02':
				return 'Februar';
			case '03':
				return 'März';
			case '04':
				return 'April';
			case '05':
				return 'Mai';
			case '06':
				return 'Juni';
			case '07':
				return 'Juli';
			case '08':
				return 'August';
			case '09':
				return 'September';
			case '10':
				return 'Oktober';
			case '11':
				return 'November';
			case '12':
				return 'Dezember';
			default:
				return '';
		}
	}

}