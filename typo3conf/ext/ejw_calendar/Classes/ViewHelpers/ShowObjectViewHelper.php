<?php

class Tx_EjwCalendar_ViewHelpers_ShowObjectViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {

	/**
	 * @param object $object
	 * @param string $space
	 * @return string
	 */
	public function render($object, $space = " ") {
		try{
			return str_replace(" ", $space, $object->getName());
		} catch (Exception $e){
			return '';
		}
	}
}