<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_ejwcalendar_domain_model_appointment'] = array(
	'ctrl' => $TCA['tx_ejwcalendar_domain_model_appointment']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, startdate, enddate, startmoment, endmoment, place, description, categories, organizers',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, startdate, enddate, startmoment, endmoment, place, description, categories, organizers,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_ejwcalendar_domain_model_appointment',
				'foreign_table_where' => 'AND tx_ejwcalendar_domain_model_appointment.pid=###CURRENT_PID### AND tx_ejwcalendar_domain_model_appointment.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejw_calendar/Resources/Private/Language/locallang_db.xml:tx_ejwcalendar_domain_model_appointment.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'startdate' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejw_calendar/Resources/Private/Language/locallang_db.xml:tx_ejwcalendar_domain_model_appointment.startdate',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'date,required',
				'checkbox' => 0,
				'default' => 0,
			),
		),
		'enddate' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejw_calendar/Resources/Private/Language/locallang_db.xml:tx_ejwcalendar_domain_model_appointment.enddate',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'date,required',
				'checkbox' => 0,
				'default' => 0,
			),
		),
		'startmoment' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejw_calendar/Resources/Private/Language/locallang_db.xml:tx_ejwcalendar_domain_model_appointment.startmoment',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'time',
				'checkbox' => 0,
				'default' => 0,
			),
		),
		'endmoment' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejw_calendar/Resources/Private/Language/locallang_db.xml:tx_ejwcalendar_domain_model_appointment.endmoment',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'time',
				'checkbox' => 0,
				'default' => 0,
			),
		),
		'place' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejw_calendar/Resources/Private/Language/locallang_db.xml:tx_ejwcalendar_domain_model_appointment.place',
			'config' => array(
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim'
			),
		),
		'description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejw_calendar/Resources/Private/Language/locallang_db.xml:tx_ejwcalendar_domain_model_appointment.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			),
			'defaultExtras' => 'richtext[]',
		),
		'categories' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejw_calendar/Resources/Private/Language/locallang_db.xml:tx_ejwcalendar_domain_model_appointment.categories',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwcalendar_domain_model_category',
				'MM' => 'tx_ejwcalendar_appointment_category_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_ejwcalendar_domain_model_category',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'organizers' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejw_calendar/Resources/Private/Language/locallang_db.xml:tx_ejwcalendar_domain_model_appointment.organizers',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwcalendar_domain_model_organizer',
				'MM' => 'tx_ejwcalendar_appointment_organizer_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_ejwcalendar_domain_model_organizer',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
	),
);

?>