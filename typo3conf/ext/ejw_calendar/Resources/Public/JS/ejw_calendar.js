$(document).ready(function() {

    resizeColorbars();

    $('.tx_ejwcalendar-more').click(function(){
        var description = $(this).siblings('.tx_ejwcalendar-description');
        if (description.is(':visible')){
            description.hide({duration: 1, step: function() {
		resizeColorbars();
		}});
            $(this).text('Weitere Informationen');
        } else {
            description.show({duration: 1, step: function() {
		resizeColorbars();
		}});
            $(this).text('Informationen ausblenden');
        }
    });

    $('.tx_ejwcalendar-filter-more').click(function(){
        var filterCollection = $(this).siblings('.tx_ejwcalendar-filter-collection');
        var searchBar = $('.tx_ejwcalendar-searchbar');
        var filterBox = $('.tx_ejwcalendar-filterbox');
        if (filterCollection.is(':visible')){
            filterBox.css('padding-bottom', '0px');
            filterCollection.animate({
		height: "toggle"
		}, 0, function() {
		// Animation complete.
		});
            searchBar.animate({
		height: "toggle"
		}, 0, function() {
		// Animation complete.
		});
            $(this).text('Filter einblenden');
        } else {
            filterBox.css('padding-bottom', '20px');
            filterCollection.animate({
		height: "toggle"
		},0, function() {
		// Animation complete.
		});
            searchBar.animate({
		height: "toggle"
		}, 0, function() {
		// Animation complete.
		});
            $(this).text('Filter ausblenden');
        }
    });

    $('#tx_ejwcalendar-searchbox').on('input', function(){
        hideAndShowAppointments();
    });

    $('#tx_ejwcalendar-filter-organizers').on('change', function(){
        hideAndShowAppointments();
    });

    $('#tx_ejwcalendar-filter-categories').on('change', function(){
        hideAndShowAppointments();
    });

});

//set height of colorbars
function resizeColorbars(){
    $('.tx_ejwcalendar-appointment').each(function(){
        var height = $(this).children('.tx_ejwcalendar-info').height();
        $(this).children('.tx_ejwcalendar-color').height(height);
    });
}


//manage month titles
function manageMonthTitles(){
    $('.tx_ejwcalendar-month').each(function(){
        var show = false;
        $(this).children('.tx_ejwcalendar-appointment').each(function(){
            if ($(this).is(':visible')){
                show = true;
            }
        });
        if (show){
            $(this).children('h2').show(0);
        } else {
            $(this).children('h2').hide(0);
        }
    });
}


//hide and show appointments
function hideAndShowAppointments(){
    var searchboxValue = $('#tx_ejwcalendar-searchbox').val();

    var activeOrganizer = $('#tx_ejwcalendar-filter-organizers option:selected').text();
    var activeCategory = $('#tx_ejwcalendar-filter-categories option:selected').text();

    $('.tx_ejwcalendar-appointment').each(function(){

        //prepare
        var hide = false;
        var organizers = $(this).children('.tx_ejwcalendar-info').children('.tx_ejwcalendar-organizers').text();
        var categories = $(this).children('.tx_ejwcalendar-info').children('.tx_ejwcalendar-categories').text();

        //evaluate searchbox if not empty
        if(searchboxValue != ''){
            var title = $(this).children('.tx_ejwcalendar-info').children('.tx_ejwcalendar-title').text();
            if (title.toLowerCase().indexOf(searchboxValue.toLowerCase()) >= 0 || categories.toLowerCase().indexOf(searchboxValue.toLowerCase()) >= 0 || organizers.toLowerCase().indexOf(searchboxValue.toLowerCase()) >= 0){
                // Do nothing
            } else {
                hide = true;
            }
        }

        //evaluate filterbox
        if (activeCategory != 'Alle'){
            if (categories.toLowerCase().indexOf(activeCategory.toLowerCase()) >= 0){
                //Do nothing
            } else {
                hide = true;
            }
        }

        if (activeOrganizer != 'Alle'){
            if (organizers.toLowerCase().indexOf(activeOrganizer.toLowerCase()) >= 0){
                //Do nothing
            } else {
                hide = true;
            }
        }

        //hide if necessary
        if(hide){
            $(this).hide(0);
        } else {
            $(this).show(0);
        }
    });

    manageMonthTitles();
    checkForEmptyResult();
}


//check for empty result
function checkForEmptyResult(){
    var empty = true;
    $('.tx_ejwcalendar-appointment').each(function(){
        if($(this).is(':visible')){
            empty = false;
        }
    });

    var message = $('#tx_ejwcalendar-message');

    if (empty){
        message.text('Zu ihrer Suchanfrage wurden leider keine Ergebnisse gefunden.');
    } else {
       message.text('');
    }
}