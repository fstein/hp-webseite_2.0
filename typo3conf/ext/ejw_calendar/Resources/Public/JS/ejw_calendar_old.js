$(document).ready(function() {
    $('#tx_ejwcalendar').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": false,
        "bInfo": false,
        "bAutoWidth": false
    });

    $('.tx_ejwcalendar-appointment').click(function(){
        var id = $(this).attr('id');
        $('#'+id+'-info').toggle();
        $('#'+id+' .tx_ejwcalendar-appointment-td').toggleClass('colorful');
    });

    $('.tx_ejwcalendar-filter-checkbox').click(function(){
        var organizers = $('.tx_ejwcalendar-filter-checkbox-organizer');
        var categories = $('.tx_ejwcalendar-filter-checkbox-category');
        var activeOrganizers = new Array();
        var activeCategories = new Array();
        organizers.each(function(){
            var checked = $(this).prop('checked');
            if (checked){
                activeOrganizers.push($(this).attr('value'));
            }
        });
        categories.each(function(){
            var checked = $(this).prop('checked');
            if (checked){
                activeCategories.push($(this).attr('value'));
            }
        });
        var appointments = $('.tx_ejwcalendar-appointment');
        appointments.show();
        appointments.each(function(){
            var appointment = $(this);
            var hide = false;
            if (activeOrganizers.length > 0){
                hide = true;
                activeOrganizers.forEach(function(entry){
                   if (appointment.hasClass(entry)){
                       hide = false;
                   }
                });
            }

            if (activeCategories.length > 0){
                hide = true;
                activeCategories.forEach(function(entry){
                    if (appointment.hasClass(entry)){
                        hide = false;
                    }
                });
            }

            if(hide){
                $(this).hide();
            }
        });
    });

    $('#tx_ejwcalendar-filter-searchbox').on('input', function(){
       console.log('changed');
    });


} );