<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_EjwCalendar_Domain_Model_Appointment.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EJW Kalender
 *
 */
class Tx_EjwCalendar_Domain_Model_AppointmentTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_EjwCalendar_Domain_Model_Appointment
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_EjwCalendar_Domain_Model_Appointment();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle() { 
		$this->fixture->setTitle('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getTitle()
		);
	}
	
	/**
	 * @test
	 */
	public function getStartReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setStartForStringSetsStart() { 
		$this->fixture->setStart('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getStart()
		);
	}
	
	/**
	 * @test
	 */
	public function getEndReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setEndForStringSetsEnd() { 
		$this->fixture->setEnd('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getEnd()
		);
	}
	
	/**
	 * @test
	 */
	public function getDescriptionReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setDescriptionForStringSetsDescription() { 
		$this->fixture->setDescription('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getDescription()
		);
	}
	
	/**
	 * @test
	 */
	public function getCategoriesReturnsInitialValueForObjectStorageContainingTx_EjwCalendar_Domain_Model_Category() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getCategories()
		);
	}

	/**
	 * @test
	 */
	public function setCategoriesForObjectStorageContainingTx_EjwCalendar_Domain_Model_CategorySetsCategories() { 
		$category = new Tx_EjwCalendar_Domain_Model_Category();
		$objectStorageHoldingExactlyOneCategories = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneCategories->attach($category);
		$this->fixture->setCategories($objectStorageHoldingExactlyOneCategories);

		$this->assertSame(
			$objectStorageHoldingExactlyOneCategories,
			$this->fixture->getCategories()
		);
	}
	
	/**
	 * @test
	 */
	public function addCategoryToObjectStorageHoldingCategories() {
		$category = new Tx_EjwCalendar_Domain_Model_Category();
		$objectStorageHoldingExactlyOneCategory = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneCategory->attach($category);
		$this->fixture->addCategory($category);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneCategory,
			$this->fixture->getCategories()
		);
	}

	/**
	 * @test
	 */
	public function removeCategoryFromObjectStorageHoldingCategories() {
		$category = new Tx_EjwCalendar_Domain_Model_Category();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($category);
		$localObjectStorage->detach($category);
		$this->fixture->addCategory($category);
		$this->fixture->removeCategory($category);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getCategories()
		);
	}
	
	/**
	 * @test
	 */
	public function getOrganizersReturnsInitialValueForObjectStorageContainingTx_EjwCalendar_Domain_Model_Organizer() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getOrganizers()
		);
	}

	/**
	 * @test
	 */
	public function setOrganizersForObjectStorageContainingTx_EjwCalendar_Domain_Model_OrganizerSetsOrganizers() { 
		$organizer = new Tx_EjwCalendar_Domain_Model_Organizer();
		$objectStorageHoldingExactlyOneOrganizers = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneOrganizers->attach($organizer);
		$this->fixture->setOrganizers($objectStorageHoldingExactlyOneOrganizers);

		$this->assertSame(
			$objectStorageHoldingExactlyOneOrganizers,
			$this->fixture->getOrganizers()
		);
	}
	
	/**
	 * @test
	 */
	public function addOrganizerToObjectStorageHoldingOrganizers() {
		$organizer = new Tx_EjwCalendar_Domain_Model_Organizer();
		$objectStorageHoldingExactlyOneOrganizer = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneOrganizer->attach($organizer);
		$this->fixture->addOrganizer($organizer);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneOrganizer,
			$this->fixture->getOrganizers()
		);
	}

	/**
	 * @test
	 */
	public function removeOrganizerFromObjectStorageHoldingOrganizers() {
		$organizer = new Tx_EjwCalendar_Domain_Model_Organizer();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($organizer);
		$localObjectStorage->detach($organizer);
		$this->fixture->addOrganizer($organizer);
		$this->fixture->removeOrganizer($organizer);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getOrganizers()
		);
	}
	
}
?>