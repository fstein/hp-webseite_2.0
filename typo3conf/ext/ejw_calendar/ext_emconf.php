<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "ejw_calendar".
 *
 * Auto generated 19-07-2014 16:39
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'EJW Kalender',
	'description' => '',
	'category' => 'plugin',
	'author' => '',
	'author_email' => '',
	'author_company' => '',
	'state' => 'beta',
	'uploadfolder' => 0,
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '0.0.0',
	'constraints' => 
	array (
		'depends' => 
		array (
			'extbase' => '1.3',
			'fluid' => '1.3',
			'typo3' => '4.5-0.0.0',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
	'_md5_values_when_last_written' => 'a:62:{s:12:"ext_icon.gif";s:4:"e922";s:17:"ext_localconf.php";s:4:"1346";s:14:"ext_tables.php";s:4:"e5c1";s:14:"ext_tables.sql";s:4:"86ba";s:21:"ExtensionBuilder.json";s:4:"a66b";s:44:"Classes/Controller/AppointmentController.php";s:4:"659a";s:41:"Classes/Controller/CategoryController.php";s:4:"25b6";s:42:"Classes/Controller/OrganizerController.php";s:4:"8758";s:36:"Classes/Domain/Model/Appointment.php";s:4:"5bff";s:33:"Classes/Domain/Model/Category.php";s:4:"db02";s:34:"Classes/Domain/Model/Organizer.php";s:4:"2c10";s:51:"Classes/Domain/Repository/AppointmentRepository.php";s:4:"fac2";s:48:"Classes/Domain/Repository/CategoryRepository.php";s:4:"88cd";s:49:"Classes/Domain/Repository/OrganizerRepository.php";s:4:"4eed";s:48:"Classes/ViewHelpers/DetermineColorViewHelper.php";s:4:"ea0e";s:45:"Classes/ViewHelpers/RenderMonthViewHelper.php";s:4:"ca3d";s:47:"Classes/ViewHelpers/RenderObjectsViewHelper.php";s:4:"344e";s:44:"Classes/ViewHelpers/RenderTimeViewHelper.php";s:4:"2347";s:44:"Classes/ViewHelpers/ShowObjectViewHelper.php";s:4:"2743";s:44:"Configuration/ExtensionBuilder/settings.yaml";s:4:"85cd";s:33:"Configuration/TCA/Appointment.php";s:4:"1c3c";s:30:"Configuration/TCA/Category.php";s:4:"90f8";s:31:"Configuration/TCA/Organizer.php";s:4:"6634";s:38:"Configuration/TypoScript/constants.txt";s:4:"6aa1";s:34:"Configuration/TypoScript/setup.txt";s:4:"18a8";s:40:"Resources/Private/Language/locallang.xml";s:4:"5d3c";s:84:"Resources/Private/Language/locallang_csh_tx_ejwcalendar_domain_model_appointment.xml";s:4:"909f";s:81:"Resources/Private/Language/locallang_csh_tx_ejwcalendar_domain_model_category.xml";s:4:"d536";s:82:"Resources/Private/Language/locallang_csh_tx_ejwcalendar_domain_model_organizer.xml";s:4:"7450";s:43:"Resources/Private/Language/locallang_db.xml";s:4:"bc9d";s:38:"Resources/Private/Layouts/Default.html";s:4:"96e9";s:54:"Resources/Private/Partials/Appointment/Properties.html";s:4:"83fd";s:49:"Resources/Private/Templates/Appointment/List.html";s:4:"dc31";s:49:"Resources/Private/Templates/Appointment/Show.html";s:4:"6047";s:37:"Resources/Public/CSS/ejw_calendar.css";s:4:"cabc";s:35:"Resources/Public/Icons/relation.gif";s:4:"e615";s:66:"Resources/Public/Icons/tx_ejwcalendar_domain_model_appointment.gif";s:4:"905a";s:63:"Resources/Public/Icons/tx_ejwcalendar_domain_model_category.gif";s:4:"905a";s:64:"Resources/Public/Icons/tx_ejwcalendar_domain_model_organizer.gif";s:4:"905a";s:35:"Resources/Public/JS/ejw_calendar.js";s:4:"e54e";s:39:"Resources/Public/JS/ejw_calendar_old.js";s:4:"8092";s:40:"Resources/Public/JS/jquery-1.11.0.min.js";s:4:"8fc2";s:41:"Resources/Public/images/back_disabled.png";s:4:"574c";s:40:"Resources/Public/images/back_enabled.png";s:4:"2998";s:46:"Resources/Public/images/back_enabled_hover.png";s:4:"9d29";s:35:"Resources/Public/images/favicon.ico";s:4:"c30d";s:44:"Resources/Public/images/forward_disabled.png";s:4:"72ea";s:43:"Resources/Public/images/forward_enabled.png";s:4:"a8c6";s:49:"Resources/Public/images/forward_enabled_hover.png";s:4:"9be5";s:36:"Resources/Public/images/sort_asc.png";s:4:"6c56";s:45:"Resources/Public/images/sort_asc_disabled.png";s:4:"64f2";s:37:"Resources/Public/images/sort_both.png";s:4:"94b3";s:37:"Resources/Public/images/sort_desc.png";s:4:"8f88";s:46:"Resources/Public/images/sort_desc_disabled.png";s:4:"d484";s:41:"Resources/Public/images/Sorting icons.psd";s:4:"9c28";s:51:"Tests/Unit/Controller/AppointmentControllerTest.php";s:4:"672a";s:48:"Tests/Unit/Controller/CategoryControllerTest.php";s:4:"262f";s:49:"Tests/Unit/Controller/OrganizerControllerTest.php";s:4:"0df6";s:43:"Tests/Unit/Domain/Model/AppointmentTest.php";s:4:"ca68";s:40:"Tests/Unit/Domain/Model/CategoryTest.php";s:4:"a2e2";s:41:"Tests/Unit/Domain/Model/OrganizerTest.php";s:4:"295e";s:14:"doc/manual.sxw";s:4:"8d2d";}',
);

