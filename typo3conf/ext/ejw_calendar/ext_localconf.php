<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::configurePlugin(
	$_EXTKEY,
	'Ejwcalendar',
	array(
		'Appointment' => 'list, show',
	),
	// non-cacheable actions
	array(
		'Appointment' => 'list, show',
	)
);

?>