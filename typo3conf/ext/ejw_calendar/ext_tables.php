<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Ejwcalendar',
	'EJW Kalender'
);

t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'EJW Kalender');

t3lib_extMgm::addLLrefForTCAdescr('tx_ejwcalendar_domain_model_organizer', 'EXT:ejw_calendar/Resources/Private/Language/locallang_csh_tx_ejwcalendar_domain_model_organizer.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_ejwcalendar_domain_model_organizer');
$TCA['tx_ejwcalendar_domain_model_organizer'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ejw_calendar/Resources/Private/Language/locallang_db.xml:tx_ejwcalendar_domain_model_organizer',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'default_sortby' => 'ORDER BY name ASC',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,color,',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Organizer.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_ejwcalendar_domain_model_organizer.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_ejwcalendar_domain_model_category', 'EXT:ejw_calendar/Resources/Private/Language/locallang_csh_tx_ejwcalendar_domain_model_category.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_ejwcalendar_domain_model_category');
$TCA['tx_ejwcalendar_domain_model_category'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ejw_calendar/Resources/Private/Language/locallang_db.xml:tx_ejwcalendar_domain_model_category',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'default_sortby' => 'ORDER BY name ASC',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Category.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_ejwcalendar_domain_model_category.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_ejwcalendar_domain_model_appointment', 'EXT:ejw_calendar/Resources/Private/Language/locallang_csh_tx_ejwcalendar_domain_model_appointment.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_ejwcalendar_domain_model_appointment');
$TCA['tx_ejwcalendar_domain_model_appointment'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ejw_calendar/Resources/Private/Language/locallang_db.xml:tx_ejwcalendar_domain_model_appointment',
		'label' => 'title',
		'label_alt' => 'startdate',
        	'label_alt_force' => 1,  
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'default_sortby' => 'ORDER BY startdate DESC',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,startdate,enddate,startmoment,endmoment,description,categories,organizers,',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Appointment.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_ejwcalendar_domain_model_appointment.gif'
	),
);

?>