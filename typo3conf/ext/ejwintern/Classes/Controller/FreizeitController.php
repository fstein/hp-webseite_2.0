<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package ejwintern
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Ejwintern_Controller_FreizeitController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * freizeitRepository
	 *
	 * @var Tx_Ejwintern_Domain_Repository_FreizeitRepository
	 */
	protected $freizeitRepository;

	/**
	 * personenRepository
	 *
	 * @var Tx_Ejwintern_Domain_Repository_PersonenRepository
	 */
	protected $personenRepository;

	/**
	 * leistungRepository
	 *
	 * @var Tx_Ejwintern_Domain_Repository_LeistungRepository
	 */
	protected $leistungRepository;

	/**
	 * injectFreizeitRepository
	 *
	 * @param Tx_Ejwintern_Domain_Repository_FreizeitRepository $freizeitRepository
	 * @return void
	 */
	public function injectFreizeitRepository(Tx_Ejwintern_Domain_Repository_FreizeitRepository $freizeitRepository) {
		$this->freizeitRepository = $freizeitRepository;
	}

	/**
	 * injectPersonenRepository
	 *
	 * @param Tx_Ejwintern_Domain_Repository_PersonenRepository $personenRepository
	 * @return void
	 */
	public function injectPersonenRepository(Tx_Ejwintern_Domain_Repository_PersonenRepository $personenRepository) {
		$this->personenRepository = $personenRepository;
	}

	/**
	 * injectLeistungRepository
	 *
	 * @param Tx_Ejwintern_Domain_Repository_LeistungRepository $leistungRepository
	 * @return void
	 */
	public function injectLeistungRepository(Tx_Ejwintern_Domain_Repository_LeistungRepository $leistungRepository) {
		$this->leistungRepository = $leistungRepository;
	}

	/**
	 * action show
	 *
	 * @param mixed $freizeit
	 * @return void
	 * @dontvalidate $freizeit
	 * @dontverifyrequesthash 
	 */
	public function showAction(Tx_Ejwintern_Domain_Model_Freizeit $freizeit) {

		$this->view->assign('freizeit', $freizeit);
		$this->view->assign('leitungs', $this->personenRepository->findPersonenByFreizeit($freizeit));
		$this->view->assign('leistungs', $this->leistungRepository->findLeistungByRelation($freizeit));

	}

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		#$freizeits = $this->freizeitRepository->findAll();
		$freizeits = $this->freizeitRepository->listByNr();
		$this->view->assign('freizeits', $freizeits);
	}

}
?>