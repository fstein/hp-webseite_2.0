<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package ejwintern
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Ejwintern_Controller_GemeindeController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * gemeindeRepository
	 *
	 * @var Tx_Ejwintern_Domain_Repository_GemeindeRepository
	 */
	protected $gemeindeRepository;

	/**
	 * personenRepository
	 *
	 * @var Tx_Ejwintern_Domain_Repository_PersonenRepository
	 */
	protected $personenRepository;

	/**
	 * gruppeRepository
	 *
	 * @var Tx_Ejwintern_Domain_Repository_GruppeRepository
	 */
	protected $gruppeRepository;	
	
	/**
	 * kategorienRepository
	 *
	 * @var Tx_Ejwintern_Domain_Repository_KategorienRepository
	 */
	protected $kategorienRepository;	
	
	/**
	 * injectGemeindeRepository
	 *
	 * @param Tx_Ejwintern_Domain_Repository_GemeindeRepository $gemeindeRepository
	 * @return void
	 */
	public function injectGemeindeRepository(Tx_Ejwintern_Domain_Repository_GemeindeRepository $gemeindeRepository) {
		$this->gemeindeRepository = $gemeindeRepository;
	}

	/**
	 * injectPersonenRepository
	 *
	 * @param Tx_Ejwintern_Domain_Repository_PersonenRepository $personenRepository
	 * @return void
	 */
	public function injectPersonenRepository(Tx_Ejwintern_Domain_Repository_PersonenRepository $personenRepository) {
		$this->personenRepository = $personenRepository;
	}
	
	/**
	 * injectGruppeRepository
	 *
	 * @param Tx_Ejwintern_Domain_Repository_GruppeRepository $gruppeRepository
	 * @return void
	 */
	public function injectGruppeRepository(Tx_Ejwintern_Domain_Repository_GruppeRepository $gruppeRepository) {
		$this->gruppeRepository = $gruppeRepository;
	}
	
	/**
	 * injectKategorienRepository
	 *
	 * @param Tx_Ejwintern_Domain_Repository_KategorienRepository $kategorienRepository
	 * @return void
	 */
	public function injectKategorienRepository(Tx_Ejwintern_Domain_Repository_KategorienRepository $kategorienRepository) {
		$this->kategorienRepository = $kategorienRepository;
	}

	/**
	 * action show
	 *
	 * @param mixed $gemeinde
	 * @return void
	 * @dontvalidate $gemeinde
	 * @dontverifyrequesthash
	 */
	public function showAction(Tx_Ejwintern_Domain_Model_Gemeinde $gemeinde) {
	
		$this->view->assign('gemeinde', $gemeinde);
		$this->view->assign('ansprechpartners', $this->personenRepository->findPersonenByGemeinde($gemeinde));
		//$this->view->assign('kategoriens', $this->kategorienRepository->findAll());
		$this->view->assign('kategoriens', $this->kategorienRepository->findKategorien());
		$this->view->assign('gruppes', $this->gruppeRepository->findByGemeinde($gemeinde));
		
	}
	
	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$this->view->assign('gemeindes', $this->gemeindeRepository->findAll());
		
	}

}
?>