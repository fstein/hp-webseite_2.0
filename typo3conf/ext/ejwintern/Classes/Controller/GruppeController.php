<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package ejwintern
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Ejwintern_Controller_GruppeController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * gruppeRepository
	 *
	 * @var Tx_Ejwintern_Domain_Repository_GruppeRepository
	 */
	protected $gruppeRepository;

	/**
	 * gemeindeRepository
	 *
	 * @var Tx_Ejwintern_Domain_Repository_GemeindeRepository
	 */
	protected $gemeindeRepository;

	/**
	 * injectGruppeRepository
	 *
	 * @param Tx_Ejwintern_Domain_Repository_GruppeRepository $gruppeRepository
	 * @return void
	 */
	public function injectGruppeRepository(Tx_Ejwintern_Domain_Repository_GruppeRepository $gruppeRepository) {
		$this->gruppeRepository = $gruppeRepository;
	}

	/**
	 * injectGemeindeRepository
	 *
	 * @param Tx_Ejwintern_Domain_Repository_GemeindeRepository $gemeindeRepository
	 * @return void
	 */
	public function injectGemeindeRepository(Tx_Ejwintern_Domain_Repository_GemeindeRepository $gemeindeRepository) {
		$this->gemeindeRepository = $gemeindeRepository;
	}
	
	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		//var_dump($this->gruppeRepository->findAll()->toArray());exit;
		$this->view->assign('gruppes', $this->gruppeRepository->findAll());
		$this->view->assign('gemeindes', $this->gemeindeRepository->findGemeinden());
	}

}
?>