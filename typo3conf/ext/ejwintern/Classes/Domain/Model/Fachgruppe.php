<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package ejwintern
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Ejwintern_Domain_Model_Fachgruppe extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * kurzName
	 *
	 * @var string
	 */
	protected $kurzName;

	/**
	 * gruppe
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gruppe>
	 */
	protected $gruppe;

	/**
	 * freizeit
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Freizeit>
	 */
	protected $freizeit;

	/**
	 * ansprechpartner
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen>
	 */
	protected $ansprechpartner;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->gruppe = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->freizeit = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->ansprechpartner = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the kurzName
	 *
	 * @return string $kurzName
	 */
	public function getKurzName() {
		return $this->kurzName;
	}

	/**
	 * Sets the kurzName
	 *
	 * @param string $kurzName
	 * @return void
	 */
	public function setKurzName($kurzName) {
		$this->kurzName = $kurzName;
	}

	/**
	 * Adds a Gruppe
	 *
	 * @param Tx_Ejwintern_Domain_Model_Gruppe $gruppe
	 * @return void
	 */
	public function addGruppe(Tx_Ejwintern_Domain_Model_Gruppe $gruppe) {
		$this->gruppe->attach($gruppe);
	}

	/**
	 * Removes a Gruppe
	 *
	 * @param Tx_Ejwintern_Domain_Model_Gruppe $gruppeToRemove The Gruppe to be removed
	 * @return void
	 */
	public function removeGruppe(Tx_Ejwintern_Domain_Model_Gruppe $gruppeToRemove) {
		$this->gruppe->detach($gruppeToRemove);
	}

	/**
	 * Returns the gruppe
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gruppe> $gruppe
	 */
	public function getGruppe() {
		return $this->gruppe;
	}

	/**
	 * Sets the gruppe
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gruppe> $gruppe
	 * @return void
	 */
	public function setGruppe(Tx_Extbase_Persistence_ObjectStorage $gruppe) {
		$this->gruppe = $gruppe;
	}

	/**
	 * Adds a Freizeit
	 *
	 * @param Tx_Ejwintern_Domain_Model_Freizeit $freizeit
	 * @return void
	 */
	public function addFreizeit(Tx_Ejwintern_Domain_Model_Freizeit $freizeit) {
		$this->freizeit->attach($freizeit);
	}

	/**
	 * Removes a Freizeit
	 *
	 * @param Tx_Ejwintern_Domain_Model_Freizeit $freizeitToRemove The Freizeit to be removed
	 * @return void
	 */
	public function removeFreizeit(Tx_Ejwintern_Domain_Model_Freizeit $freizeitToRemove) {
		$this->freizeit->detach($freizeitToRemove);
	}

	/**
	 * Returns the freizeit
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Freizeit> $freizeit
	 */
	public function getFreizeit() {
		return $this->freizeit;
	}

	/**
	 * Sets the freizeit
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Freizeit> $freizeit
	 * @return void
	 */
	public function setFreizeit(Tx_Extbase_Persistence_ObjectStorage $freizeit) {
		$this->freizeit = $freizeit;
	}

	/**
	 * Adds a Personen
	 *
	 * @param Tx_Ejwintern_Domain_Model_Personen $ansprechpartner
	 * @return void
	 */
	public function addAnsprechpartner(Tx_Ejwintern_Domain_Model_Personen $ansprechpartner) {
		$this->ansprechpartner->attach($ansprechpartner);
	}

	/**
	 * Removes a Personen
	 *
	 * @param Tx_Ejwintern_Domain_Model_Personen $ansprechpartnerToRemove The Personen to be removed
	 * @return void
	 */
	public function removeAnsprechpartner(Tx_Ejwintern_Domain_Model_Personen $ansprechpartnerToRemove) {
		$this->ansprechpartner->detach($ansprechpartnerToRemove);
	}

	/**
	 * Returns the ansprechpartner
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen> $ansprechpartner
	 */
	public function getAnsprechpartner() {
		return $this->ansprechpartner;
	}

	/**
	 * Sets the ansprechpartner
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen> $ansprechpartner
	 * @return void
	 */
	public function setAnsprechpartner(Tx_Extbase_Persistence_ObjectStorage $ansprechpartner) {
		$this->ansprechpartner = $ansprechpartner;
	}

}
?>