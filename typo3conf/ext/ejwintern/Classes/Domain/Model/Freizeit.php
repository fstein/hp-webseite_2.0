<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package ejwintern
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Ejwintern_Domain_Model_Freizeit extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $name;

	/**
	 * ziel
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $ziel;

	/**
	 * alterStart
	 *
	 * @var integer
	 * @validate NotEmpty
	 */
	protected $alterStart;

	/**
	 * alterEnde
	 *
	 * @var integer
	 * @validate NotEmpty
	 */
	protected $alterEnde;

	/**
	 * datumStart
	 *
	 * @var DateTime
	 * @validate NotEmpty
	 */
	protected $datumStart;

	/**
	 * datumEnde
	 *
	 * @var DateTime
	 * @validate NotEmpty
	 */
	protected $datumEnde;

	/**
	 * teamerAnzahl
	 *
	 * @var integer
	 * @validate NotEmpty
	 */
	protected $teamerAnzahl;

	/**
	 * unterbringung
	 *
	 * @var string
	 */
	protected $unterbringung;

	/**
	 * beschreibung
	 *
	 * @var string
	 */
	protected $beschreibung;

	/**
	 * preis
	 *
	 * @var integer
	 * @validate NotEmpty
	 */
	protected $preis;

	/**
	 * zusatzkosten
	 *
	 * @var string
	 */
	protected $zusatzkosten;

	/**
	 * image
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $image;

	/**
	 * Eindeutige Freizeit Nummer
	 *
	 * @var integer
	 * @validate NotEmpty
	 */
	protected $nr;

	/**
	 * anreise
	 *
	 * @var string
	 */
	protected $anreise;

	/**
	 * teilnehmerzahl
	 *
	 * @var string
	 */
	protected $teilnehmerzahl;

	/**
	 * fachgruppe
	 *
	 * @var Tx_Ejwintern_Domain_Model_Fachgruppe
	 */
	protected $fachgruppe;

	/**
	 * leistungen
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Leistung>
	 */
	protected $leistungen;

	/**
	 * leitung
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen>
	 */
	protected $leitung;

	/**
	 * zielgruppe
	 *
	 * @var Tx_Ejwintern_Domain_Model_Kategorien
	 */
	protected $zielgruppe;

	/**
	 * belegt
	 *
	 * @var integer
	 */
	protected $belegt;	
	
	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->leistungen = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->leitung = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the ziel
	 *
	 * @return string $ziel
	 */
	public function getZiel() {
		return $this->ziel;
	}

	/**
	 * Sets the ziel
	 *
	 * @param string $ziel
	 * @return void
	 */
	public function setZiel($ziel) {
		$this->ziel = $ziel;
	}

	/**
	 * Returns the alterStart
	 *
	 * @return integer $alterStart
	 */
	public function getAlterStart() {
		return $this->alterStart;
	}

	/**
	 * Sets the alterStart
	 *
	 * @param integer $alterStart
	 * @return void
	 */
	public function setAlterStart($alterStart) {
		$this->alterStart = $alterStart;
	}

	/**
	 * Returns the alterEnde
	 *
	 * @return integer $alterEnde
	 */
	public function getAlterEnde() {
		return $this->alterEnde;
	}

	/**
	 * Sets the alterEnde
	 *
	 * @param integer $alterEnde
	 * @return void
	 */
	public function setAlterEnde($alterEnde) {
		$this->alterEnde = $alterEnde;
	}

	/**
	 * Returns the datumStart
	 *
	 * @return DateTime $datumStart
	 */
	public function getDatumStart() {
		return $this->datumStart;
	}

	/**
	 * Sets the datumStart
	 *
	 * @param DateTime $datumStart
	 * @return void
	 */
	public function setDatumStart($datumStart) {
		$this->datumStart = $datumStart;
	}

	/**
	 * Returns the datumEnde
	 *
	 * @return DateTime $datumEnde
	 */
	public function getDatumEnde() {
		return $this->datumEnde;
	}

	/**
	 * Sets the datumEnde
	 *
	 * @param DateTime $datumEnde
	 * @return void
	 */
	public function setDatumEnde($datumEnde) {
		$this->datumEnde = $datumEnde;
	}

	/**
	 * Returns the teamerAnzahl
	 *
	 * @return integer $teamerAnzahl
	 */
	public function getTeamerAnzahl() {
		return $this->teamerAnzahl;
	}

	/**
	 * Sets the teamerAnzahl
	 *
	 * @param integer $teamerAnzahl
	 * @return void
	 */
	public function setTeamerAnzahl($teamerAnzahl) {
		$this->teamerAnzahl = $teamerAnzahl;
	}

	/**
	 * Returns the unterbringung
	 *
	 * @return string $unterbringung
	 */
	public function getUnterbringung() {
		return $this->unterbringung;
	}

	/**
	 * Sets the unterbringung
	 *
	 * @param string $unterbringung
	 * @return void
	 */
	public function setUnterbringung($unterbringung) {
		$this->unterbringung = $unterbringung;
	}

	/**
	 * Returns the beschreibung
	 *
	 * @return string $beschreibung
	 */
	public function getBeschreibung() {
		return $this->beschreibung;
	}

	/**
	 * Sets the beschreibung
	 *
	 * @param string $beschreibung
	 * @return void
	 */
	public function setBeschreibung($beschreibung) {
		$this->beschreibung = $beschreibung;
	}

	/**
	 * Returns the preis
	 *
	 * @return integer $preis
	 */
	public function getPreis() {
		return $this->preis;
	}

	/**
	 * Sets the preis
	 *
	 * @param integer $preis
	 * @return void
	 */
	public function setPreis($preis) {
		$this->preis = $preis;
	}
	
	/**
	 * Returns the zusatzkosten
	 *
	 * @return string $zusatzkosten
	 */
	public function getZusatzkosten() {
		return $this->zusatzkosten;
	}

	/**
	 * Sets the zusatzkosten
	 *
	 * @param string $zusatzkosten
	 * @return void
	 */
	public function setZusatzkosten($zusatzkosten) {
		$this->zusatzkosten = $zusatzkosten;
	}

	/**
	 * Returns the image
	 *
	 * @return string $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 *
	 * @param string $image
	 * @return void
	 */
	public function setImage($image) {
		$this->image = $image;
	}

	/**
	 * Returns the nr
	 *
	 * @return integer $nr
	 */
	public function getNr() {
		return $this->nr;
	}

	/**
	 * Sets the nr
	 *
	 * @param integer $nr
	 * @return void
	 */
	public function setNr($nr) {
		$this->nr = $nr;
	}

	/**
	 * Returns the anreise
	 *
	 * @return string $anreise
	 */
	public function getAnreise() {
		return $this->anreise;
	}

	/**
	 * Sets the anreise
	 *
	 * @param string $anreise
	 * @return void
	 */
	public function setAnreise($anreise) {
		$this->anreise = $anreise;
	}

	/**
	 * Returns the teilnehmerzahl
	 *
	 * @return string $teilnehmerzahl
	 */
	public function getTeilnehmerzahl() {
		return $this->teilnehmerzahl;
	}

	/**
	 * Sets the teilnehmerzahl
	 *
	 * @param string $teilnehmerzahl
	 * @return void
	 */
	public function setTeilnehmerzahl($teilnehmerzahl) {
		$this->teilnehmerzahl = $teilnehmerzahl;
	}	
	
	/**
	 * Returns the fachgruppe
	 *
	 * @return Tx_Ejwintern_Domain_Model_Fachgruppe $fachgruppe
	 */
	public function getFachgruppe() {
		return $this->fachgruppe;
	}

	/**
	 * Sets the fachgruppe
	 *
	 * @param Tx_Ejwintern_Domain_Model_Fachgruppe $fachgruppe
	 * @return void
	 */
	public function setFachgruppe(Tx_Ejwintern_Domain_Model_Fachgruppe $fachgruppe) {
		$this->fachgruppe = $fachgruppe;
	}

	/**
	 * Adds a Leistung
	 *
	 * @param Tx_Ejwintern_Domain_Model_Leistung $leistungen
	 * @return void
	 */
	public function addLeistungen(Tx_Ejwintern_Domain_Model_Leistung $leistungen) {
		$this->leistungen->attach($leistungen);
	}

	/**
	 * Removes a Leistung
	 *
	 * @param Tx_Ejwintern_Domain_Model_Leistung $leistungenToRemove The Leistung to be removed
	 * @return void
	 */
	public function removeLeistungen(Tx_Ejwintern_Domain_Model_Leistung $leistungenToRemove) {
		$this->leistungen->detach($leistungenToRemove);
	}

	/**
	 * Returns the leistungen
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Leistung> $leistungen
	 */
	public function getLeistungen() {
		return $this->leistungen;
	}

	/**
	 * Sets the leistungen
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Leistung> $leistungen
	 * @return void
	 */
	public function setLeistungen(Tx_Extbase_Persistence_ObjectStorage $leistungen) {
		$this->leistungen = $leistungen;
	}

	/**
	 * Adds a Personen
	 *
	 * @param Tx_Ejwintern_Domain_Model_Personen $leitung
	 * @return void
	 */
	public function addLeitung(Tx_Ejwintern_Domain_Model_Personen $leitung) {
		$this->leitung->attach($leitung);
	}

	/**
	 * Removes a Personen
	 *
	 * @param Tx_Ejwintern_Domain_Model_Personen $leitungToRemove The Personen to be removed
	 * @return void
	 */
	public function removeLeitung(Tx_Ejwintern_Domain_Model_Personen $leitungToRemove) {
		$this->leitung->detach($leitungToRemove);
	}

	/**
	 * Returns the leitung
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen> $leitung
	 */
	public function getLeitung() {
		return $this->leitung;
	}

	/**
	 * Sets the leitung
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen> $leitung
	 * @return void
	 */
	public function setLeitung(Tx_Extbase_Persistence_ObjectStorage $leitung) {
		$this->leitung = $leitung;
	}

	/**
	 * Returns the zielgruppe
	 *
	 * @return Tx_Ejwintern_Domain_Model_Kategorien $zielgruppe
	 */
	public function getZielgruppe() {
		return $this->zielgruppe;
	}

	/**
	 * Sets the zielgruppe
	 *
	 * @param Tx_Ejwintern_Domain_Model_Kategorien $zielgruppe
	 * @return void
	 */
	public function setZielgruppe(Tx_Ejwintern_Domain_Model_Kategorien $zielgruppe) {
		$this->zielgruppe = $zielgruppe;
	}
	
	/**
	 * Returns the belegt
	 *
	 * @return integer $belegt
	 */
	public function getBelegt() {
		return $this->belegt;
	}

	/**
	 * Sets the belegt
	 *
	 * @param integer $belegt
	 * @return void
	 */
	public function setBelegt($belegt) {
		$this->belegt = $belegt;
	}
}
?>