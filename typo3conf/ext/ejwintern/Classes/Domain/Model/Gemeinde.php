<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package ejwintern
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Ejwintern_Domain_Model_Gemeinde extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $name;

	/**
	 * strasse
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $strasse;

	/**
	 * hausnummer
	 *
	 * @var integer
	 * @validate NotEmpty
	 */
	protected $hausnummer;

	/**
	 * plz
	 *
	 * @var integer
	 * @validate NotEmpty
	 */
	protected $plz;

	/**
	 * ort
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $ort;

	/**
	 * telefon
	 *
	 * @var string
	 */
	protected $telefon;

	/**
	 * webseite
	 *
	 * @var string
	 */
	protected $webseite;

	/**
	 * pfarrer
	 *
	 * @var string
	 */
	protected $pfarrer;

	/**
	 * koordinaten
	 *
	 * @var string
	 */
	protected $koordinaten;

	/**
	 * ortswerk
	 *
	 * @var Tx_Ejwintern_Domain_Model_Ortswerk
	 */
	protected $ortswerk;

	/**
	 * ansprechpartner
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen>
	 */
	protected $ansprechpartner;

	/**
	 * gruppe
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gruppe>
	 */
	protected $gruppe;

	/**
	 * ansprechpartnerhp
	 *
	 * @var Tx_Ejwintern_Domain_Model_Personen
	 */
	protected $ansprechpartnerhp;	

	/**
	 * ansprechpartnerhmp
	 *
	 * @var Tx_Ejwintern_Domain_Model_Personen
	 */
	protected $ansprechpartnerhmp;
	
	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->ansprechpartner = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->gruppe = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the strasse
	 *
	 * @return string $strasse
	 */
	public function getStrasse() {
		return $this->strasse;
	}

	/**
	 * Sets the strasse
	 *
	 * @param string $strasse
	 * @return void
	 */
	public function setStrasse($strasse) {
		$this->strasse = $strasse;
	}

	/**
	 * Returns the hausnummer
	 *
	 * @return integer $hausnummer
	 */
	public function getHausnummer() {
		return $this->hausnummer;
	}

	/**
	 * Sets the hausnummer
	 *
	 * @param integer $hausnummer
	 * @return void
	 */
	public function setHausnummer($hausnummer) {
		$this->hausnummer = $hausnummer;
	}

	/**
	 * Returns the plz
	 *
	 * @return integer $plz
	 */
	public function getPlz() {
		return $this->plz;
	}

	/**
	 * Sets the plz
	 *
	 * @param integer $plz
	 * @return void
	 */
	public function setPlz($plz) {
		$this->plz = $plz;
	}

	/**
	 * Returns the ort
	 *
	 * @return string $ort
	 */
	public function getOrt() {
		return $this->ort;
	}

	/**
	 * Sets the ort
	 *
	 * @param string $ort
	 * @return void
	 */
	public function setOrt($ort) {
		$this->ort = $ort;
	}

	/**
	 * Returns the telefon
	 *
	 * @return string $telefon
	 */
	public function getTelefon() {
		return $this->telefon;
	}

	/**
	 * Sets the telefon
	 *
	 * @param string $telefon
	 * @return void
	 */
	public function setTelefon($telefon) {
		$this->telefon = $telefon;
	}

	/**
	 * Returns the webseite
	 *
	 * @return string $webseite
	 */
	public function getWebseite() {
		return $this->webseite;
	}

	/**
	 * Sets the webseite
	 *
	 * @param string $webseite
	 * @return void
	 */
	public function setWebseite($webseite) {
		$this->webseite = $webseite;
	}

	/**
	 * Returns the pfarrer
	 *
	 * @return string $pfarrer
	 */
	public function getPfarrer() {
		return $this->pfarrer;
	}

	/**
	 * Sets the pfarrer
	 *
	 * @param string $pfarrer
	 * @return void
	 */
	public function setPfarrer($pfarrer) {
		$this->pfarrer = $pfarrer;
	}

	/**
	 * Returns the koordinaten
	 *
	 * @return string $koordinaten
	 */
	public function getKoordinaten() {
		return $this->koordinaten;
	}

	/**
	 * Sets the koordinaten
	 *
	 * @param string $koordinaten
	 * @return void
	 */
	public function setKoordinaten($koordinaten) {
		$this->koordinaten = $koordinaten;
	}

	/**
	 * Returns the ortswerk
	 *
	 * @return Tx_Ejwintern_Domain_Model_Ortswerk $ortswerk
	 */
	public function getOrtswerk() {
		return $this->ortswerk;
	}

	/**
	 * Sets the ortswerk
	 *
	 * @param Tx_Ejwintern_Domain_Model_Ortswerk $ortswerk
	 * @return void
	 */
	public function setOrtswerk(Tx_Ejwintern_Domain_Model_Ortswerk $ortswerk) {
		$this->ortswerk = $ortswerk;
	}

	/**
	 * Adds a Personen
	 *
	 * @param Tx_Ejwintern_Domain_Model_Personen $ansprechpartner
	 * @return void
	 */
	public function addAnsprechpartner(Tx_Ejwintern_Domain_Model_Personen $ansprechpartner) {
		$this->ansprechpartner->attach($ansprechpartner);
	}

	/**
	 * Removes a Personen
	 *
	 * @param Tx_Ejwintern_Domain_Model_Personen $ansprechpartnerToRemove The Personen to be removed
	 * @return void
	 */
	public function removeAnsprechpartner(Tx_Ejwintern_Domain_Model_Personen $ansprechpartnerToRemove) {
		$this->ansprechpartner->detach($ansprechpartnerToRemove);
	}

	/**
	 * Returns the ansprechpartner
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen> $ansprechpartner
	 */
	public function getAnsprechpartner() {
		return $this->ansprechpartner;
	}

	/**
	 * Sets the ansprechpartner
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen> $ansprechpartner
	 * @return void
	 */
	public function setAnsprechpartner(Tx_Extbase_Persistence_ObjectStorage $ansprechpartner) {
		$this->ansprechpartner = $ansprechpartner;
	}

	/**
	 * Adds a Gruppe
	 *
	 * @param Tx_Ejwintern_Domain_Model_Gruppe $gruppe
	 * @return void
	 */
	public function addGruppe(Tx_Ejwintern_Domain_Model_Gruppe $gruppe) {
		$this->gruppe->attach($gruppe);
	}

	/**
	 * Removes a Gruppe
	 *
	 * @param Tx_Ejwintern_Domain_Model_Gruppe $gruppeToRemove The Gruppe to be removed
	 * @return void
	 */
	public function removeGruppe(Tx_Ejwintern_Domain_Model_Gruppe $gruppeToRemove) {
		$this->gruppe->detach($gruppeToRemove);
	}

	/**
	 * Returns the gruppe
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gruppe> $gruppe
	 */
	public function getGruppe() {
		return $this->gruppe;
	}

	/**
	 * Sets the gruppe
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gruppe> $gruppe
	 * @return void
	 */
	public function setGruppe(Tx_Extbase_Persistence_ObjectStorage $gruppe) {
		$this->gruppe = $gruppe;
	}

	/**
	 * Returns the ansprechpartnerhp
	 *
	 * @return Tx_Ejwintern_Domain_Model_Personen $ansprechpartnerhp
	 */
	public function getAnsprechpartnerhp() {
		return $this->ansprechpartnerhp;
	}

	/**
	 * Sets the ansprechpartnerhp
	 *
	 * @param Tx_Ejwintern_Domain_Model_Personen $ansprechpartnerhp
	 * @return void
	 */
	public function setAnsprechpartnerhp(Tx_Extbase_Persistence_ObjectStorage $ansprechpartnerhp) {
		$this->ansprechpartnerhp = $ansprechpartnerhp;
	}

	/**
	 * Returns the ansprechpartnerhmp
	 *
	 * @return Tx_Ejwintern_Domain_Model_Personen $ansprechpartnerhmp
	 */
	public function getAnsprechpartnerhmp() {
		return $this->ansprechpartnerhmp;
	}

	/**
	 * Sets the ansprechpartnerhmp
	 *
	 * @param Tx_Ejwintern_Domain_Model_Personen $ansprechpartnerhmp
	 * @return void
	 */
	public function setAnsprechpartnerhmp(Tx_Extbase_Persistence_ObjectStorage $ansprechpartnerhmp) {
		$this->ansprechpartnerhmp = $ansprechpartnerhmp;
	}

}
?>