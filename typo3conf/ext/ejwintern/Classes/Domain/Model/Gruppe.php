<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package ejwintern
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Ejwintern_Domain_Model_Gruppe extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $name;

	/**
	 * alterStart
	 *
	 * @var integer
	 */
	protected $alterStart;

	/**
	 * alterEnde
	 *
	 * @var integer
	 */
	protected $alterEnde;

	/**
	 * uhrzeitStart
	 *
	 * @var string
	 */
	protected $uhrzeitStart;

	/**
	 * uhrzeitEnde
	 *
	 * @var string
	 */
	protected $uhrzeitEnde;

	/**
	 * wochentag
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $wochentag;

	/**
	 * strasse
	 *
	 * @var string
	 */
	protected $strasse;

	/**
	 * hausnummer
	 *
	 * @var string
	 */
	protected $hausnummer;

	/**
	 * plz
	 *
	 * @var string
	 */
	protected $plz;

	/**
	 * ort
	 *
	 * @var string
	 */
	protected $ort;

	/**
	 * gruppenleiter
	 *
	 * @var string
	 */
	protected $gruppenleiter;

	/**
	 * koordinaten
	 *
	 * @var string
	 */
	protected $koordinaten;	
	
	/**
	 * gemeinde
	 *
	 * @var Tx_Ejwintern_Domain_Model_Gemeinde
	 */
	protected $gemeinde;

	/**
	 * fachgruppe
	 *
	 * @var Tx_Ejwintern_Domain_Model_Fachgruppe
	 */
	protected $fachgruppe;

	/**
	 * ansprechpartner
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen>
	 */
	protected $ansprechpartner;

	/**
	 * turnus
	 *
	 * @var string
	 */
	protected $turnus;

	/**
	 * zielgruppe
	 *
	 * @var Tx_Ejwintern_Domain_Model_Kategorien
	 */
	protected $zielgruppe;

	/**
	 * geschlecht
	 *
	 * @var string
	 */
	protected $geschlecht;	
	
	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->ansprechpartner = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the alterStart
	 *
	 * @return integer $alterStart
	 */
	public function getAlterStart() {
		return $this->alterStart;
	}

	/**
	 * Sets the alterStart
	 *
	 * @param integer $alterStart
	 * @return void
	 */
	public function setAlterStart($alterStart) {
		$this->alterStart = $alterStart;
	}

	/**
	 * Returns the alterEnde
	 *
	 * @return integer $alterEnde
	 */
	public function getAlterEnde() {
		return $this->alterEnde;
	}

	/**
	 * Sets the alterEnde
	 *
	 * @param integer $alterEnde
	 * @return void
	 */
	public function setAlterEnde($alterEnde) {
		$this->alterEnde = $alterEnde;
	}

	/**
	 * Returns the uhrzeitStart
	 *
	 * @return string $uhrzeitStart
	 */
	public function getUhrzeitStart() {
		return $this->uhrzeitStart;
	}

	/**
	 * Sets the uhrzeitStart
	 *
	 * @param string $uhrzeitStart
	 * @return void
	 */
	public function setUhrzeitStart($uhrzeitStart) {
		$this->uhrzeitStart = $uhrzeitStart;
	}

	/**
	 * Returns the uhrzeitEnde
	 *
	 * @return string $uhrzeitEnde
	 */
	public function getUhrzeitEnde() {
		return $this->uhrzeitEnde;
	}

	/**
	 * Sets the uhrzeitEnde
	 *
	 * @param string $uhrzeitEnde
	 * @return void
	 */
	public function setUhrzeitEnde($uhrzeitEnde) {
		$this->uhrzeitEnde = $uhrzeitEnde;
	}

	/**
	 * Returns the wochentag
	 *
	 * @return string $wochentag
	 */
	public function getWochentag() {
		return $this->wochentag;
	}

	/**
	 * Sets the wochentag
	 *
	 * @param string $wochentag
	 * @return void
	 */
	public function setWochentag($wochentag) {
		$this->wochentag = $wochentag;
	}

	/**
	 * Returns the strasse
	 *
	 * @return string $strasse
	 */
	public function getStrasse() {
		return $this->strasse;
	}

	/**
	 * Sets the strasse
	 *
	 * @param string $strasse
	 * @return void
	 */
	public function setStrasse($strasse) {
		$this->strasse = $strasse;
	}

	/**
	 * Returns the hausnummer
	 *
	 * @return string $hausnummer
	 */
	public function getHausnummer() {
		return $this->hausnummer;
	}

	/**
	 * Sets the hausnummer
	 *
	 * @param string $hausnummer
	 * @return void
	 */
	public function setHausnummer($hausnummer) {
		$this->hausnummer = $hausnummer;
	}

	/**
	 * Returns the plz
	 *
	 * @return string $plz
	 */
	public function getPlz() {
		return $this->plz;
	}

	/**
	 * Sets the plz
	 *
	 * @param string $plz
	 * @return void
	 */
	public function setPlz($plz) {
		$this->plz = $plz;
	}

	/**
	 * Returns the ort
	 *
	 * @return string $ort
	 */
	public function getOrt() {
		return $this->ort;
	}

	/**
	 * Sets the ort
	 *
	 * @param string $ort
	 * @return void
	 */
	public function setOrt($ort) {
		$this->ort = $ort;
	}
	
	/**
	 * Returns the geschlecht
	 *
	 * @return string $geschlecht
	 */
	public function getGeschlecht() {
		return $this->geschlecht;
	}

	/**
	 * Sets the geschlecht
	 *
	 * @param string $geschlecht
	 * @return void
	 */
	public function setGeschlecht($geschlecht) {
		$this->geschlecht = $geschlecht;
	}

	/**
	 * Returns the gruppenleiter
	 *
	 * @return string $gruppenleiter
	 */
	public function getGruppenleiter() {
		return $this->gruppenleiter;
	}

	/**
	 * Sets the gruppenleiter
	 *
	 * @param string $gruppenleiter
	 * @return void
	 */
	public function setGruppenleiter($gruppenleiter) {
		$this->gruppenleiter = $gruppenleiter;
	}

	/**
	 * Returns the koordinaten
	 *
	 * @return string $koordinaten
	 */
	public function getKoordinaten() {
		return $this->koordinaten;
	}

	/**
	 * Sets the koordinaten
	 *
	 * @param string $koordinaten
	 * @return void
	 */
	public function setKoordinaten($koordinaten) {
		$this->koordinaten = $koordinaten;
	}
	
	/**
	 * Returns the gemeinde
	 *
	 * @return Tx_Ejwintern_Domain_Model_Gemeinde $gemeinde
	 */
	public function getGemeinde() {
		return $this->gemeinde;
	}

	/**
	 * Sets the gemeinde
	 *
	 * @param Tx_Ejwintern_Domain_Model_Gemeinde $gemeinde
	 * @return void
	 */
	public function setGemeinde(Tx_Ejwintern_Domain_Model_Gemeinde $gemeinde) {
		$this->gemeinde = $gemeinde;
	}

	/**
	 * Returns the fachgruppe
	 *
	 * @return Tx_Ejwintern_Domain_Model_Fachgruppe $fachgruppe
	 */
	public function getFachgruppe() {
		return $this->fachgruppe;
	}

	/**
	 * Sets the fachgruppe
	 *
	 * @param Tx_Ejwintern_Domain_Model_Fachgruppe $fachgruppe
	 * @return void
	 */
	public function setFachgruppe(Tx_Ejwintern_Domain_Model_Fachgruppe $fachgruppe) {
		$this->fachgruppe = $fachgruppe;
	}

	/**
	 * Adds a Personen
	 *
	 * @param Tx_Ejwintern_Domain_Model_Personen $ansprechpartner
	 * @return void
	 */
	public function addAnsprechpartner(Tx_Ejwintern_Domain_Model_Personen $ansprechpartner) {
		$this->ansprechpartner->attach($ansprechpartner);
	}

	/**
	 * Removes a Personen
	 *
	 * @param Tx_Ejwintern_Domain_Model_Personen $ansprechpartnerToRemove The Personen to be removed
	 * @return void
	 */
	public function removeAnsprechpartner(Tx_Ejwintern_Domain_Model_Personen $ansprechpartnerToRemove) {
		$this->ansprechpartner->detach($ansprechpartnerToRemove);
	}

	/**
	 * Returns the ansprechpartner
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen> $ansprechpartner
	 */
	public function getAnsprechpartner() {
		return $this->ansprechpartner;
	}

	/**
	 * Sets the ansprechpartner
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen> $ansprechpartner
	 * @return void
	 */
	public function setAnsprechpartner(Tx_Extbase_Persistence_ObjectStorage $ansprechpartner) {
		$this->ansprechpartner = $ansprechpartner;
	}

	/**
	 * Returns the turnus
	 *
	 * @return string $turnus
	 */
	public function getTurnus() {
		return $this->turnus;
	}

	/**
	 * Sets the turnus
	 *
	 * @param string $turnus
	 * @return void
	 */
	public function setTurnus(string $turnus) {
		$this->turnus = $turnus;
	}

	/**
	 * Returns the zielgruppe
	 *
	 * @return Tx_Ejwintern_Domain_Model_Kategorien $zielgruppe
	 */
	public function getZielgruppe() {
		return $this->zielgruppe;
	}

	/**
	 * Sets the zielgruppe
	 *
	 * @param Tx_Ejwintern_Domain_Model_Kategorien $zielgruppe
	 * @return void
	 */
	public function setZielgruppe(Tx_Ejwintern_Domain_Model_Kategorien $zielgruppe) {
		$this->zielgruppe = $zielgruppe;
	}

}
?>