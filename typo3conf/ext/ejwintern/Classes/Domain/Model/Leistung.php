<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package ejwintern
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Ejwintern_Domain_Model_Leistung extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $name;

	/**
	 * freizeiten
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Freizeit>
	 */
	protected $freizeiten;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->freizeiten = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Adds a Freizeit
	 *
	 * @param Tx_Ejwintern_Domain_Model_Freizeit $freizeiten
	 * @return void
	 */
	public function addFreizeiten(Tx_Ejwintern_Domain_Model_Freizeit $freizeiten) {
		$this->freizeiten->attach($freizeiten);
	}

	/**
	 * Removes a Freizeit
	 *
	 * @param Tx_Ejwintern_Domain_Model_Freizeit $freizeitenToRemove The Freizeit to be removed
	 * @return void
	 */
	public function removeFreizeiten(Tx_Ejwintern_Domain_Model_Freizeit $freizeitenToRemove) {
		$this->freizeiten->detach($freizeitenToRemove);
	}

	/**
	 * Returns the freizeiten
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Freizeit> $freizeiten
	 */
	public function getFreizeiten() {
		return $this->freizeiten;
	}

	/**
	 * Sets the freizeiten
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Freizeit> $freizeiten
	 * @return void
	 */
	public function setFreizeiten(Tx_Extbase_Persistence_ObjectStorage $freizeiten) {
		$this->freizeiten = $freizeiten;
	}

}
?>