<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package ejwintern
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Ejwintern_Domain_Model_Ortswerk extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $name;

	/**
	 * website
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $website;

	/**
	 * ansprechpartner
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen>
	 */
	protected $ansprechpartner;

	/**
	 * gemeinde
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gemeinde>
	 */
	protected $gemeinde;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->ansprechpartner = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->gemeinde = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the website
	 *
	 * @return string $website
	 */
	public function getWebsite() {
		return $this->website;
	}

	/**
	 * Sets the website
	 *
	 * @param string $website
	 * @return void
	 */
	public function setWebsite($website) {
		$this->website = $website;
	}

	/**
	 * Adds a Personen
	 *
	 * @param Tx_Ejwintern_Domain_Model_Personen $ansprechpartner
	 * @return void
	 */
	public function addAnsprechpartner(Tx_Ejwintern_Domain_Model_Personen $ansprechpartner) {
		$this->ansprechpartner->attach($ansprechpartner);
	}

	/**
	 * Removes a Personen
	 *
	 * @param Tx_Ejwintern_Domain_Model_Personen $ansprechpartnerToRemove The Personen to be removed
	 * @return void
	 */
	public function removeAnsprechpartner(Tx_Ejwintern_Domain_Model_Personen $ansprechpartnerToRemove) {
		$this->ansprechpartner->detach($ansprechpartnerToRemove);
	}

	/**
	 * Returns the ansprechpartner
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen> $ansprechpartner
	 */
	public function getAnsprechpartner() {
		return $this->ansprechpartner;
	}

	/**
	 * Sets the ansprechpartner
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Personen> $ansprechpartner
	 * @return void
	 */
	public function setAnsprechpartner(Tx_Extbase_Persistence_ObjectStorage $ansprechpartner) {
		$this->ansprechpartner = $ansprechpartner;
	}

	/**
	 * Adds a Gemeinde
	 *
	 * @param Tx_Ejwintern_Domain_Model_Gemeinde $gemeinde
	 * @return void
	 */
	public function addGemeinde(Tx_Ejwintern_Domain_Model_Gemeinde $gemeinde) {
		$this->gemeinde->attach($gemeinde);
	}

	/**
	 * Removes a Gemeinde
	 *
	 * @param Tx_Ejwintern_Domain_Model_Gemeinde $gemeindeToRemove The Gemeinde to be removed
	 * @return void
	 */
	public function removeGemeinde(Tx_Ejwintern_Domain_Model_Gemeinde $gemeindeToRemove) {
		$this->gemeinde->detach($gemeindeToRemove);
	}

	/**
	 * Returns the gemeinde
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gemeinde> $gemeinde
	 */
	public function getGemeinde() {
		return $this->gemeinde;
	}

	/**
	 * Sets the gemeinde
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gemeinde> $gemeinde
	 * @return void
	 */
	public function setGemeinde(Tx_Extbase_Persistence_ObjectStorage $gemeinde) {
		$this->gemeinde = $gemeinde;
	}

}
?>