<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package ejwintern
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Ejwintern_Domain_Model_Personen extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Name der Person
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $name;

	/**
	 * Geburtsdatum: tt.mm.jjjj
	 *
	 * @var string
	 */
	protected $geburtsdatum;

	/**
	 * telephone
	 *
	 * @var string
	 */
	protected $telephone;

	/**
	 * email
	 *
	 * @var string
	 */
	protected $email;

	/**
	 * image
	 *
	 * @var string
	 */
	protected $image;

	/**
	 * hauptamtlich
	 *
	 * @var boolean
	 * @validate NotEmpty
	 */
	protected $hauptamtlich;

	/**
	 * Informationen
	 *
	 * @var string
	 */
	protected $info;

	/**
	 * sorting
	 *
	 * @var int
	 */
	protected $sorting;

	/**
	 * ortswerk
	 *
	 * @var Tx_Ejwintern_Domain_Model_Ortswerk
	 */
	protected $ortswerk;

	/**
	 * fachgruppe
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Fachgruppe>
	 */
	protected $fachgruppe;

	/**
	 * gemeinde
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gemeinde>
	 */
	protected $gemeinde;

	/**
	 * gruppe
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gruppe>
	 */
	protected $gruppe;

	/**
	 * freizeit
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Freizeit>
	 */
	protected $freizeit;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->fachgruppe = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->gemeinde = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->gruppe = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->freizeit = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the geburtsdatum
	 *
	 * @return string $geburtsdatum
	 */
	public function getGeburtsdatum() {
		return $this->geburtsdatum;
	}

	/**
	 * Sets the geburtsdatum
	 *
	 * @param string $geburtsdatum
	 * @return void
	 */
	public function setGeburtsdatum($geburtsdatum) {
		$this->geburtsdatum = $geburtsdatum;
	}

	/**
	 * Returns the telephone
	 *
	 * @return string $telephone
	 */
	public function getTelephone() {
		return $this->telephone;
	}

	/**
	 * Sets the telephone
	 *
	 * @param string $telephone
	 * @return void
	 */
	public function setTelephone($telephone) {
		$this->telephone = $telephone;
	}

	/**
	 * Returns the sorting
	 *
	 * @return int $sorting
	 */
	public function getSorting() {
		return $this->sorting;
	}

	/**
	 * Sets the sorting
	 *
	 * @param int $sorting
	 * @return void
	 */
	public function setSorting($sorting) {
		$this->sorting = $sorting;
	}

	/**
	 * Returns the email
	 *
	 * @return string $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Sets the email
	 *
	 * @param string $email
	 * @return void
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * Returns the image
	 *
	 * @return string $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 *
	 * @param string $image
	 * @return void
	 */
	public function setImage($image) {
		$this->image = $image;
	}

	/**
	 * Returns the hauptamtlich
	 *
	 * @return boolean $hauptamtlich
	 */
	public function getHauptamtlich() {
		return $this->hauptamtlich;
	}

	/**
	 * Sets the hauptamtlich
	 *
	 * @param boolean $hauptamtlich
	 * @return void
	 */
	public function setHauptamtlich($hauptamtlich) {
		$this->hauptamtlich = $hauptamtlich;
	}

	/**
	 * Returns the boolean state of hauptamtlich
	 *
	 * @return boolean
	 */
	public function isHauptamtlich() {
		return $this->getHauptamtlich();
	}

	/**
	 * Returns the ortswerk
	 *
	 * @return Tx_Ejwintern_Domain_Model_Ortswerk $ortswerk
	 */
	public function getOrtswerk() {
		return $this->ortswerk;
	}

	/**
	 * Sets the ortswerk
	 *
	 * @param Tx_Ejwintern_Domain_Model_Ortswerk $ortswerk
	 * @return void
	 */
	public function setOrtswerk(Tx_Ejwintern_Domain_Model_Ortswerk $ortswerk) {
		$this->ortswerk = $ortswerk;
	}

	/**
	 * Adds a Fachgruppe
	 *
	 * @param Tx_Ejwintern_Domain_Model_Fachgruppe $fachgruppe
	 * @return void
	 */
	public function addFachgruppe(Tx_Ejwintern_Domain_Model_Fachgruppe $fachgruppe) {
		$this->fachgruppe->attach($fachgruppe);
	}

	/**
	 * Removes a Fachgruppe
	 *
	 * @param Tx_Ejwintern_Domain_Model_Fachgruppe $fachgruppeToRemove The Fachgruppe to be removed
	 * @return void
	 */
	public function removeFachgruppe(Tx_Ejwintern_Domain_Model_Fachgruppe $fachgruppeToRemove) {
		$this->fachgruppe->detach($fachgruppeToRemove);
	}

	/**
	 * Returns the fachgruppe
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Fachgruppe> $fachgruppe
	 */
	public function getFachgruppe() {
		return $this->fachgruppe;
	}

	/**
	 * Sets the fachgruppe
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Fachgruppe> $fachgruppe
	 * @return void
	 */
	public function setFachgruppe(Tx_Extbase_Persistence_ObjectStorage $fachgruppe) {
		$this->fachgruppe = $fachgruppe;
	}

	/**
	 * Adds a Gemeinde
	 *
	 * @param Tx_Ejwintern_Domain_Model_Gemeinde $gemeinde
	 * @return void
	 */
	public function addGemeinde(Tx_Ejwintern_Domain_Model_Gemeinde $gemeinde) {
		$this->gemeinde->attach($gemeinde);
	}

	/**
	 * Removes a Gemeinde
	 *
	 * @param Tx_Ejwintern_Domain_Model_Gemeinde $gemeindeToRemove The Gemeinde to be removed
	 * @return void
	 */
	public function removeGemeinde(Tx_Ejwintern_Domain_Model_Gemeinde $gemeindeToRemove) {
		$this->gemeinde->detach($gemeindeToRemove);
	}

	/**
	 * Returns the gemeinde
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gemeinde> $gemeinde
	 */
	public function getGemeinde() {
		return $this->gemeinde;
	}

	/**
	 * Sets the gemeinde
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gemeinde> $gemeinde
	 * @return void
	 */
	public function setGemeinde(Tx_Extbase_Persistence_ObjectStorage $gemeinde) {
		$this->gemeinde = $gemeinde;
	}

	/**
	 * Adds a Gruppe
	 *
	 * @param Tx_Ejwintern_Domain_Model_Gruppe $gruppe
	 * @return void
	 */
	public function addGruppe(Tx_Ejwintern_Domain_Model_Gruppe $gruppe) {
		$this->gruppe->attach($gruppe);
	}

	/**
	 * Removes a Gruppe
	 *
	 * @param Tx_Ejwintern_Domain_Model_Gruppe $gruppeToRemove The Gruppe to be removed
	 * @return void
	 */
	public function removeGruppe(Tx_Ejwintern_Domain_Model_Gruppe $gruppeToRemove) {
		$this->gruppe->detach($gruppeToRemove);
	}

	/**
	 * Returns the gruppe
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gruppe> $gruppe
	 */
	public function getGruppe() {
		return $this->gruppe;
	}

	/**
	 * Sets the gruppe
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Gruppe> $gruppe
	 * @return void
	 */
	public function setGruppe(Tx_Extbase_Persistence_ObjectStorage $gruppe) {
		$this->gruppe = $gruppe;
	}

	/**
	 * Adds a Freizeit
	 *
	 * @param Tx_Ejwintern_Domain_Model_Freizeit $freizeit
	 * @return void
	 */
	public function addFreizeit(Tx_Ejwintern_Domain_Model_Freizeit $freizeit) {
		$this->freizeit->attach($freizeit);
	}

	/**
	 * Removes a Freizeit
	 *
	 * @param Tx_Ejwintern_Domain_Model_Freizeit $freizeitToRemove The Freizeit to be removed
	 * @return void
	 */
	public function removeFreizeit(Tx_Ejwintern_Domain_Model_Freizeit $freizeitToRemove) {
		$this->freizeit->detach($freizeitToRemove);
	}

	/**
	 * Returns the freizeit
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Freizeit> $freizeit
	 */
	public function getFreizeit() {
		return $this->freizeit;
	}

	/**
	 * Sets the freizeit
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_Ejwintern_Domain_Model_Freizeit> $freizeit
	 * @return void
	 */
	public function setFreizeit(Tx_Extbase_Persistence_ObjectStorage $freizeit) {
		$this->freizeit = $freizeit;
	}

	/**
	 * Returns the info
	 *
	 * @return string $info
	 */
	public function getInfo() {
		return $this->info;
	}

	/**
	 * Sets the info
	 *
	 * @param string $info
	 * @return void
	 */
	public function setInfo($info) {
		$this->info = $info;
	}

}
?>