<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package ejwintern
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Ejwintern_Domain_Repository_PersonenRepository extends Tx_Extbase_Persistence_Repository {

	/**
	 * findPersonenByFreizeit
	 *
	 */
    public function findPersonenByFreizeit($freizeit) {
		
		$query = $this->createQuery();      
		return $query->matching(
							// werte �ber m:n relation auslesen (ref1 = spalten name in aktueller tabelle / $ref1 = wert in der verkn�pften tabelle der heraus gesucht werden soll.
                            $query->contains('freizeit', $freizeit)
                    )
                    ->execute();
    }

	/**
	 * findPersonenByGemeinde
	 *
	 */
    public function findPersonenByGemeinde($gemeinde) {
		
		$query = $this->createQuery();      
		return $query->matching( $query->contains('gemeinde', $gemeinde) )->execute();
		
    }

	/*
	 * Finde Hauptamtliche
	 *
	 */
	public function findHauptamtliche() {
	
        $query = $this->createQuery();
        $query->matching($query->equals('hauptamtlich',1) );
        $query->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_DESCENDING, 'name' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING) );
		return $query->execute();

	}
}
?>