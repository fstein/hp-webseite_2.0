<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2011 Paul Rohrbeck
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
 
class Tx_Ejwintern_ViewHelpers_FreizeitdatumViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {
 
	/**
	 * Render the supplied DateTime objects as a formatted date using strftime.
	 *
	 * @param mixed $startdatum either a DateTime object or a string (UNIX-Timestamp)
	 * @param mixed $enddatum either a DateTime object or a string (UNIX-Timestamp)
	 * @return string Formatted date
	 */
	public function render($startdatum = NULL, $enddatum = NULL) {
	
		if ($startdatum === NULL) { return ''; }
		if ($enddatum === NULL) { return ''; }
		
		if ($startdatum instanceof DateTime && $enddatum instanceof DateTime) {
			try {
				
				// Now check if they are in the same month:
				$start_monat = strftime('%m', $startdatum->format('U'));
				$end_monat = strftime('%m', $enddatum->format('U'));
				
				if($start_monat == $end_monat) {
					return strftime('%e.', $startdatum->format('U')) . ' - ' . strftime('%e. %B %G', $enddatum->format('U'));
				} else {
					return strftime('%e. %B', $startdatum->format('U')) . ' - ' . strftime('%e. %B %G', $enddatum->format('U'));
				}
						
			} catch (Exception $exception) {
				throw new Tx_Fluid_Core_ViewHelper_Exception('"' . $startdatum . $enddatum . '" was DateTime and could not be converted to UNIX-Timestamp by DateTime.', 200000001);
			}
		}
		
	}
 
}
?>