<?php

class Tx_Ejwintern_ViewHelpers_Format_PaddingViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {

	/**
	 * Format the arguments with the given printf format string.
	 *
	 * @param integer $padLength Length of the resulting string. If the value of pad_length is negative or less than the length of the input string, no padding takes place.
	 * @param string $padString The padding string
	 * @param integer $padType
	 * @return string The formatted value
	 * @author Bastian Waidelich <bastian at typo3.org>
	 * @api
	 */
	public function render($padLength, $padString = ' ', $padType=STR_PAD_RIGHT) {
		$string = $this->renderChildren();
		$return = str_pad($string, $padLength, $padString, $padType );
		return $return;
	}
	
}

?>