<?php
/**
 * Formats a Timestamp or DateTime-Object in strftime()
 * @api
 */
class Tx_Ejwintern_ViewHelpers_Format_StrftimeViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {
 
	/**
	 * Render the supplied DateTime object as a formatted date using strftime.
	 *
	 * @param mixed $date either a DateTime object or a string (UNIX-Timestamp)
	 * @param string $format Format String which is taken to format the Date/Time
	 * @return string Formatted date
	 * @api
	 */
	public function render($date = NULL, $format = '%A, %d. %B %Y') {
		if ($date === NULL) {
			$date = $this->renderChildren();
			if ($date === NULL) {
				return '';
			}
		}
		
		if ($date instanceof DateTime) {
			try {
				//return strftime($format, $date->getTimestamp());
				return strftime($format, $date->format('U'));
			} catch (Exception $exception) {
				throw new Tx_Fluid_Core_ViewHelper_Exception('"' . $date . '" was DateTime and could not be converted to UNIX-Timestamp by DateTime.', 200000001);
			}
		}
		
		return strftime($format, (int)$date);
	}
}
?>