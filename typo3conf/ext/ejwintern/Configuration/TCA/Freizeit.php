<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_ejwintern_domain_model_freizeit'] = array(
	'ctrl' => $TCA['tx_ejwintern_domain_model_freizeit']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, ziel, alter_start, alter_ende, datum_start, datum_ende, teamer_anzahl, unterbringung, beschreibung, preis, zusatzkosten, image, nr, anreise, teilnehmerzahl, fachgruppe, leistungen, leitung, zielgruppe, belegt, yag_album_uid',
	),
	'types' => array(
		'1' => array('showitem' => 'nr, name, ziel, beschreibung;;;richtext:rte_transform[mode=ts_css], image, teamer_anzahl, leitung,--div--;Datum & Kosten, datum_start, datum_ende, preis, zusatzkosten,--div--;Teilnehmer & Zielgruppe, alter_start, alter_ende, teilnehmerzahl, zielgruppe, fachgruppe,--div--;Unterbringung & Sonstiges, leistungen, unterbringung, anreise,--div--;Belegt & Sichtbarkeit, belegt, hidden'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_ejwintern_domain_model_freizeit',
				'foreign_table_where' => 'AND tx_ejwintern_domain_model_freizeit.pid=###CURRENT_PID### AND tx_ejwintern_domain_model_freizeit.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'name' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'ziel' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.ziel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'alter_start' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.alter_start',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int,required'
			),
		),
		'alter_ende' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.alter_ende',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		),
		'datum_start' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.datum_start',
			'config' => array(
				'type' => 'input',
				'size' => 12,
				'max' => 20,
				'eval' => 'datetime,required',
				'checkbox' => 1,
				'default' => time()
			),
		),
		'datum_ende' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.datum_ende',
			'config' => array(
				'type' => 'input',
				'size' => 12,
				'max' => 20,
				'eval' => 'datetime,required',
				'checkbox' => 1,
				'default' => time()
			),
		),
		'teamer_anzahl' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.teamer_anzahl',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int,required'
			),
		),
		'unterbringung' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.unterbringung',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'beschreibung' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.beschreibung',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
        		'wizards' => array(
        			'_PADDING' => 4,
        			'RTE' => array(
        				'notNewRecords' => 1,
        				'RTEonly' => 1,
        				'type' => 'script',
        				'title' => 'LLL:EXT:cms/locallang_ttc.php:bodytext.W.RTE',
        				'icon' => 'wizard_rte2.gif',
        				'script' => 'wizard_rte.php',
        			),
        		),				
			),
		),
		'preis' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.preis',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int,required'
			),
		),
		'zusatzkosten' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.zusatzkosten',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'image' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.image',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'file',
				'show_thumbs' => 1,
				'size' => 10,
				'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
				'disallowed' => '',
				'minitems' => 0,
				'maxitems' => 8,
			),
		),
		'nr' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.nr',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int,required'
			),
		),
		'anreise' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.anreise',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'teilnehmerzahl' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.teilnehmerzahl',
			'config' => array(
				'type' => 'input',
				'size' => 5,
				'eval' => 'trim'
			),
		),
		'fachgruppe' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.fachgruppe',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_fachgruppe',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'leistungen' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.leistungen',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_leistung',
				'MM' => 'tx_ejwintern_freizeit_leistung_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_ejwintern_domain_model_leistung',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'leitung' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.leitung',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_personen',
				'MM' => 'tx_ejwintern_freizeit_personen_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_ejwintern_domain_model_personen',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'zielgruppe' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.zielgruppe',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_kategorien',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'belegt' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit.belegt',
			'config' => array(
				'type' => 'check',
				'default' => 0
			),
		),
	),
);
?>