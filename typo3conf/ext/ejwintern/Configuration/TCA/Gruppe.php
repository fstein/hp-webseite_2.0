<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_ejwintern_domain_model_gruppe'] = array(
	'ctrl' => $TCA['tx_ejwintern_domain_model_gruppe']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, alter_start, alter_ende, uhrzeit_start, uhrzeit_ende, wochentag, strasse, hausnummer, plz, ort, gruppenleiter, koordinaten, gemeinde, fachgruppe, ansprechpartner, turnus, zielgruppe, geschlecht',
	),
	'types' => array(
		'1' => array('showitem' => 'hidden;;1, name, alter_start, alter_ende, fachgruppe, zielgruppe, geschlecht, --div--;Zeit, uhrzeit_start, uhrzeit_ende, wochentag, turnus, --div--;Gemeinde & Leitung, gruppenleiter, gemeinde, ansprechpartner, --div--;Alternative Adresse, strasse, hausnummer, plz, ort, koordinaten'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_ejwintern_domain_model_gruppe',
				'foreign_table_where' => 'AND tx_ejwintern_domain_model_gruppe.pid=###CURRENT_PID### AND tx_ejwintern_domain_model_gruppe.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'name' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'alter_start' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.alter_start',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		),
		'alter_ende' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.alter_ende',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		),
		'uhrzeit_start' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.uhrzeit_start',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'uhrzeit_ende' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.uhrzeit_ende',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'wochentag' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.wochentag',
			'config' => array(
                'type' => 'select',
				'items' => array (
					array('', ''),
					array('Montag', 'Montag'),
					array('Dienstag', 'Dienstag'),
					array('Mittwoch', 'Mittwoch'),
					array('Donnerstag', 'Donnerstag'),
					array('Freitag', 'Freitag'),
					array('Samstag', 'Samstag'),
					array('Sonntag', 'Sonntag'),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => 'required',
			),
		),
		'strasse' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.strasse',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'hausnummer' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.hausnummer',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'plz' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.plz',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'ort' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.ort',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'gruppenleiter' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.gruppenleiter',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'koordinaten' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.koordinaten',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'gemeinde' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.gemeinde',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_gemeinde',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'fachgruppe' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.fachgruppe',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_fachgruppe',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'ansprechpartner' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.ansprechpartner',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_personen',
				'MM' => 'tx_ejwintern_gruppe_personen_mm',
				'MM_opposite_field' => 'gruppe',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_ejwintern_domain_model_personen',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'turnus' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.turnus',
			'config' => array(
				'type' => 'select',
				'items' => array (
					array('', ''),
					array('Jeden', 'Jeden'),
					array('Jeden 1.', 'Jeden 1.'),
					array('Jeden 2.', 'Jeden 2.'),
					array('Jeden 3.', 'Jeden 3.'),
					array('Jeden 4.', 'Jeden 4.'),
					array('Am 1.', 'Am 1.'),
					array('Am 1. & 3.', 'Am 1. & 3.'),
					array('Am 2. & 4.', 'Am 2. & 4.'),
					array('Alle zwei Wochen', 'Alle zwei Wochen'),
					array('Einmal im Monat', 'Einmal im Monat'),
				),
				'size' => 1,
				'maxitems' => 1,
			),
		),
		'zielgruppe' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.zielgruppe',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_kategorien',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'geschlecht' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe.geschlecht',
			'config' => array(
                'type' => 'select',
				'items' => array (
					array('', ''),
					array('Jungen', 'Jungen'),
					array('Mädchen', 'Mädchen'),
				),
				'size' => 1,
				'maxitems' => 1,
			),
		),		
	),
);
?>