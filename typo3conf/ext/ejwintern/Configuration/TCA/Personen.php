<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_ejwintern_domain_model_personen'] = array(
	'ctrl' => $TCA['tx_ejwintern_domain_model_personen']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, geburtsdatum, telephone, email, image, hauptamtlich, info, ortswerk, fachgruppe, gemeinde, gruppe, freizeit, sorting',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, name, geburtsdatum, telephone, email, image, hauptamtlich, info;;;richtext:rte_transform[mode=ts_css], ortswerk, fachgruppe, gemeinde, gruppe, freizeit, sorting,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_ejwintern_domain_model_personen',
				'foreign_table_where' => 'AND tx_ejwintern_domain_model_personen.pid=###CURRENT_PID### AND tx_ejwintern_domain_model_personen.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'name' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'geburtsdatum' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.geburtsdatum',
			'config' => array(
				'type' => 'input',
				'size' => 12,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 1
			),
		),
		'telephone' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.telephone',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'email' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'image' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.image',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'file',
				'show_thumbs' => 1,
				'size' => 1,
				'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
				'disallowed' => '',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'hauptamtlich' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.hauptamtlich',
			'config' => array(
				'type' => 'check',
				'default' => 0
			),
		),
		'info' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.info',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
        		'wizards' => array(
        			'_PADDING' => 4,
        			'RTE' => array(
        				'notNewRecords' => 1,
        				'RTEonly' => 1,
        				'type' => 'script',
        				'title' => 'LLL:EXT:cms/locallang_ttc.php:bodytext.W.RTE',
        				'icon' => 'wizard_rte2.gif',
        				'script' => 'wizard_rte.php',
        			),
        		),				
			),
		),
		'ortswerk' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.ortswerk',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_ortswerk',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'fachgruppe' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.fachgruppe',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_fachgruppe',
				'MM' => 'tx_ejwintern_fachgruppe_personen_mm',
				'MM_opposite_field' => 'ansprechpartner',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_ejwintern_domain_model_fachgruppe',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'gemeinde' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.gemeinde',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_gemeinde',
				'MM' => 'tx_ejwintern_gemeinde_personen_mm',
				'MM_opposite_field' => 'ansprechpartner',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_ejwintern_domain_model_gemeinde',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'gruppe' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.gruppe',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_gruppe',
				'MM' => 'tx_ejwintern_gruppe_personen_mm',
				'MM_opposite_field' => 'ansprechpartner',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_ejwintern_domain_model_gruppe',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'freizeit' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.freizeit',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_ejwintern_domain_model_freizeit',
				'MM' => 'tx_ejwintern_freizeit_personen_mm',
				'MM_opposite_field' => 'leitung',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_ejwintern_domain_model_freizeit',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'sorting' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen.sorting',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		),		
	),
);
?>