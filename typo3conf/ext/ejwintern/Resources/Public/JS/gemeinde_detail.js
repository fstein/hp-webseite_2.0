var map, current_koordinaten, marker;
var default_zoom = 12;

$(document).ready(function() {
    if ($('#gemeinde_map_canvas').length != 0) {
		
		// -----------------------------------------------------------------------------------------		
		// are coordinates set?
		var current_koordinaten_xy = ($('#gemeinde-koordinaten').text() != '') ? $('#gemeinde-koordinaten').text() : '';
				
		// if there are coordinates: split them in x/y, create google position and add marker
		if(current_koordinaten_xy != ''){
			current_koordinaten_xy = current_koordinaten_xy.split(',');
			current_koordinaten = new google.maps.LatLng(current_koordinaten_xy[0], current_koordinaten_xy[1]);				
		}

		// -----------------------------------------------------------------------------------------		
		//var mapCenter = new google.maps.LatLng(50.22120, 8.63525); // Default: Frankfurt		
        map = new google.maps.Map(document.getElementById('gemeinde_map_canvas'), {
          zoom: default_zoom,
          center: current_koordinaten,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
		  scrollwheel: false
        });

		// -----------------------------------------------------------------------------------------		
		// set Marker
		markerImg = new google.maps.MarkerImage('typo3conf/ext/ejwintern/Resources/Public/Images/GooglePin-standort.png');					
		marker = new google.maps.Marker({
			position: current_koordinaten,
			map: map,		
			icon: markerImg
		});

	}
	
	// -----------------------------------------------------------------------------------------		
	// hide zielgruppe-divs if empty
	$('.zielgruppe').each(function(){
		if( $(this).children().length == 1){
			$(this).hide();
		}
	});

});
