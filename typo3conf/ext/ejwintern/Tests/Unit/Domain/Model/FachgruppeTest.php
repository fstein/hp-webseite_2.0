<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_Ejwintern_Domain_Model_Fachgruppe.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EJW Intern
 *
 * @author Paul Rohrbeck <paul@paul-rohrbeck.de>
 */
class Tx_Ejwintern_Domain_Model_FachgruppeTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_Ejwintern_Domain_Model_Fachgruppe
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_Ejwintern_Domain_Model_Fachgruppe();
	}

	public function tearDown() {
		unset($this->fixture);
	}
	
	
	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setNameForStringSetsName() { 
		$this->fixture->setName('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getName()
		);
	}
	
	/**
	 * @test
	 */
	public function getKurzNameReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setKurzNameForStringSetsKurzName() { 
		$this->fixture->setKurzName('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getKurzName()
		);
	}
	
	/**
	 * @test
	 */
	public function getGruppeReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Gruppe() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getGruppe()
		);
	}

	/**
	 * @test
	 */
	public function setGruppeForObjectStorageContainingTx_Ejwintern_Domain_Model_GruppeSetsGruppe() { 
		$gruppe = new Tx_Ejwintern_Domain_Model_Gruppe();
		$objectStorageHoldingExactlyOneGruppe = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneGruppe->attach($gruppe);
		$this->fixture->setGruppe($objectStorageHoldingExactlyOneGruppe);

		$this->assertSame(
			$objectStorageHoldingExactlyOneGruppe,
			$this->fixture->getGruppe()
		);
	}
	
	/**
	 * @test
	 */
	public function addGruppeToObjectStorageHoldingGruppe() {
		$gruppe = new Tx_Ejwintern_Domain_Model_Gruppe();
		$objectStorageHoldingExactlyOneGruppe = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneGruppe->attach($gruppe);
		$this->fixture->addGruppe($gruppe);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneGruppe,
			$this->fixture->getGruppe()
		);
	}

	/**
	 * @test
	 */
	public function removeGruppeFromObjectStorageHoldingGruppe() {
		$gruppe = new Tx_Ejwintern_Domain_Model_Gruppe();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($gruppe);
		$localObjectStorage->detach($gruppe);
		$this->fixture->addGruppe($gruppe);
		$this->fixture->removeGruppe($gruppe);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getGruppe()
		);
	}
	
	/**
	 * @test
	 */
	public function getFreizeitReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Freizeit() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getFreizeit()
		);
	}

	/**
	 * @test
	 */
	public function setFreizeitForObjectStorageContainingTx_Ejwintern_Domain_Model_FreizeitSetsFreizeit() { 
		$freizeit = new Tx_Ejwintern_Domain_Model_Freizeit();
		$objectStorageHoldingExactlyOneFreizeit = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneFreizeit->attach($freizeit);
		$this->fixture->setFreizeit($objectStorageHoldingExactlyOneFreizeit);

		$this->assertSame(
			$objectStorageHoldingExactlyOneFreizeit,
			$this->fixture->getFreizeit()
		);
	}
	
	/**
	 * @test
	 */
	public function addFreizeitToObjectStorageHoldingFreizeit() {
		$freizeit = new Tx_Ejwintern_Domain_Model_Freizeit();
		$objectStorageHoldingExactlyOneFreizeit = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneFreizeit->attach($freizeit);
		$this->fixture->addFreizeit($freizeit);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneFreizeit,
			$this->fixture->getFreizeit()
		);
	}

	/**
	 * @test
	 */
	public function removeFreizeitFromObjectStorageHoldingFreizeit() {
		$freizeit = new Tx_Ejwintern_Domain_Model_Freizeit();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($freizeit);
		$localObjectStorage->detach($freizeit);
		$this->fixture->addFreizeit($freizeit);
		$this->fixture->removeFreizeit($freizeit);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getFreizeit()
		);
	}
	
	/**
	 * @test
	 */
	public function getAnsprechpartnerReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Personen() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getAnsprechpartner()
		);
	}

	/**
	 * @test
	 */
	public function setAnsprechpartnerForObjectStorageContainingTx_Ejwintern_Domain_Model_PersonenSetsAnsprechpartner() { 
		$ansprechpartner = new Tx_Ejwintern_Domain_Model_Personen();
		$objectStorageHoldingExactlyOneAnsprechpartner = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneAnsprechpartner->attach($ansprechpartner);
		$this->fixture->setAnsprechpartner($objectStorageHoldingExactlyOneAnsprechpartner);

		$this->assertSame(
			$objectStorageHoldingExactlyOneAnsprechpartner,
			$this->fixture->getAnsprechpartner()
		);
	}
	
	/**
	 * @test
	 */
	public function addAnsprechpartnerToObjectStorageHoldingAnsprechpartner() {
		$ansprechpartner = new Tx_Ejwintern_Domain_Model_Personen();
		$objectStorageHoldingExactlyOneAnsprechpartner = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneAnsprechpartner->attach($ansprechpartner);
		$this->fixture->addAnsprechpartner($ansprechpartner);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneAnsprechpartner,
			$this->fixture->getAnsprechpartner()
		);
	}

	/**
	 * @test
	 */
	public function removeAnsprechpartnerFromObjectStorageHoldingAnsprechpartner() {
		$ansprechpartner = new Tx_Ejwintern_Domain_Model_Personen();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($ansprechpartner);
		$localObjectStorage->detach($ansprechpartner);
		$this->fixture->addAnsprechpartner($ansprechpartner);
		$this->fixture->removeAnsprechpartner($ansprechpartner);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getAnsprechpartner()
		);
	}
	
}
?>