<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_Ejwintern_Domain_Model_Freizeit.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EJW Intern
 *
 * @author Paul Rohrbeck <paul@paul-rohrbeck.de>
 */
class Tx_Ejwintern_Domain_Model_FreizeitTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_Ejwintern_Domain_Model_Freizeit
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_Ejwintern_Domain_Model_Freizeit();
	}

	public function tearDown() {
		unset($this->fixture);
	}
	
	
	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setNameForStringSetsName() { 
		$this->fixture->setName('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getName()
		);
	}
	
	/**
	 * @test
	 */
	public function getZielReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setZielForStringSetsZiel() { 
		$this->fixture->setZiel('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getZiel()
		);
	}
	
	/**
	 * @test
	 */
	public function getAlterStartReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getAlterStart()
		);
	}

	/**
	 * @test
	 */
	public function setAlterStartForIntegerSetsAlterStart() { 
		$this->fixture->setAlterStart(12);

		$this->assertSame(
			12,
			$this->fixture->getAlterStart()
		);
	}
	
	/**
	 * @test
	 */
	public function getAlterEndeReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getAlterEnde()
		);
	}

	/**
	 * @test
	 */
	public function setAlterEndeForIntegerSetsAlterEnde() { 
		$this->fixture->setAlterEnde(12);

		$this->assertSame(
			12,
			$this->fixture->getAlterEnde()
		);
	}
	
	/**
	 * @test
	 */
	public function getDatumStartReturnsInitialValueForDateTime() { }

	/**
	 * @test
	 */
	public function setDatumStartForDateTimeSetsDatumStart() { }
	
	/**
	 * @test
	 */
	public function getDatumEndeReturnsInitialValueForDateTime() { }

	/**
	 * @test
	 */
	public function setDatumEndeForDateTimeSetsDatumEnde() { }
	
	/**
	 * @test
	 */
	public function getTeamerAnzahlReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getTeamerAnzahl()
		);
	}

	/**
	 * @test
	 */
	public function setTeamerAnzahlForIntegerSetsTeamerAnzahl() { 
		$this->fixture->setTeamerAnzahl(12);

		$this->assertSame(
			12,
			$this->fixture->getTeamerAnzahl()
		);
	}
	
	/**
	 * @test
	 */
	public function getUnterbringungReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setUnterbringungForStringSetsUnterbringung() { 
		$this->fixture->setUnterbringung('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getUnterbringung()
		);
	}
	
	/**
	 * @test
	 */
	public function getBeschreibungReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setBeschreibungForStringSetsBeschreibung() { 
		$this->fixture->setBeschreibung('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getBeschreibung()
		);
	}
	
	/**
	 * @test
	 */
	public function getPreisReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getPreis()
		);
	}

	/**
	 * @test
	 */
	public function setPreisForIntegerSetsPreis() { 
		$this->fixture->setPreis(12);

		$this->assertSame(
			12,
			$this->fixture->getPreis()
		);
	}
	
	/**
	 * @test
	 */
	public function getImageReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setImageForStringSetsImage() { 
		$this->fixture->setImage('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getImage()
		);
	}
	
	/**
	 * @test
	 */
	public function getNrReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getNr()
		);
	}

	/**
	 * @test
	 */
	public function setNrForIntegerSetsNr() { 
		$this->fixture->setNr(12);

		$this->assertSame(
			12,
			$this->fixture->getNr()
		);
	}
	
	/**
	 * @test
	 */
	public function getAnreiseReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setAnreiseForStringSetsAnreise() { 
		$this->fixture->setAnreise('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getAnreise()
		);
	}
	
	/**
	 * @test
	 */
	public function getTeilnehmerzahlReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getTeilnehmerzahl()
		);
	}

	/**
	 * @test
	 */
	public function setTeilnehmerzahlForIntegerSetsTeilnehmerzahl() { 
		$this->fixture->setTeilnehmerzahl(12);

		$this->assertSame(
			12,
			$this->fixture->getTeilnehmerzahl()
		);
	}
	
	/**
	 * @test
	 */
	public function getFachgruppeReturnsInitialValueForTx_Ejwintern_Domain_Model_Fachgruppe() { 
		$this->assertEquals(
			NULL,
			$this->fixture->getFachgruppe()
		);
	}

	/**
	 * @test
	 */
	public function setFachgruppeForTx_Ejwintern_Domain_Model_FachgruppeSetsFachgruppe() { 
		$dummyObject = new Tx_Ejwintern_Domain_Model_Fachgruppe();
		$this->fixture->setFachgruppe($dummyObject);

		$this->assertSame(
			$dummyObject,
			$this->fixture->getFachgruppe()
		);
	}
	
	/**
	 * @test
	 */
	public function getLeistungenReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Leistung() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getLeistungen()
		);
	}

	/**
	 * @test
	 */
	public function setLeistungenForObjectStorageContainingTx_Ejwintern_Domain_Model_LeistungSetsLeistungen() { 
		$leistungen = new Tx_Ejwintern_Domain_Model_Leistung();
		$objectStorageHoldingExactlyOneLeistungen = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneLeistungen->attach($leistungen);
		$this->fixture->setLeistungen($objectStorageHoldingExactlyOneLeistungen);

		$this->assertSame(
			$objectStorageHoldingExactlyOneLeistungen,
			$this->fixture->getLeistungen()
		);
	}
	
	/**
	 * @test
	 */
	public function addLeistungenToObjectStorageHoldingLeistungen() {
		$leistungen = new Tx_Ejwintern_Domain_Model_Leistung();
		$objectStorageHoldingExactlyOneLeistungen = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneLeistungen->attach($leistungen);
		$this->fixture->addLeistungen($leistungen);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneLeistungen,
			$this->fixture->getLeistungen()
		);
	}

	/**
	 * @test
	 */
	public function removeLeistungenFromObjectStorageHoldingLeistungen() {
		$leistungen = new Tx_Ejwintern_Domain_Model_Leistung();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($leistungen);
		$localObjectStorage->detach($leistungen);
		$this->fixture->addLeistungen($leistungen);
		$this->fixture->removeLeistungen($leistungen);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getLeistungen()
		);
	}
	
	/**
	 * @test
	 */
	public function getLeitungReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Personen() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getLeitung()
		);
	}

	/**
	 * @test
	 */
	public function setLeitungForObjectStorageContainingTx_Ejwintern_Domain_Model_PersonenSetsLeitung() { 
		$leitung = new Tx_Ejwintern_Domain_Model_Personen();
		$objectStorageHoldingExactlyOneLeitung = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneLeitung->attach($leitung);
		$this->fixture->setLeitung($objectStorageHoldingExactlyOneLeitung);

		$this->assertSame(
			$objectStorageHoldingExactlyOneLeitung,
			$this->fixture->getLeitung()
		);
	}
	
	/**
	 * @test
	 */
	public function addLeitungToObjectStorageHoldingLeitung() {
		$leitung = new Tx_Ejwintern_Domain_Model_Personen();
		$objectStorageHoldingExactlyOneLeitung = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneLeitung->attach($leitung);
		$this->fixture->addLeitung($leitung);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneLeitung,
			$this->fixture->getLeitung()
		);
	}

	/**
	 * @test
	 */
	public function removeLeitungFromObjectStorageHoldingLeitung() {
		$leitung = new Tx_Ejwintern_Domain_Model_Personen();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($leitung);
		$localObjectStorage->detach($leitung);
		$this->fixture->addLeitung($leitung);
		$this->fixture->removeLeitung($leitung);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getLeitung()
		);
	}
	
	/**
	 * @test
	 */
	public function getZielgruppeReturnsInitialValueForTx_Ejwintern_Domain_Model_Kategorien() { 
		$this->assertEquals(
			NULL,
			$this->fixture->getZielgruppe()
		);
	}

	/**
	 * @test
	 */
	public function setZielgruppeForTx_Ejwintern_Domain_Model_KategorienSetsZielgruppe() { 
		$dummyObject = new Tx_Ejwintern_Domain_Model_Kategorien();
		$this->fixture->setZielgruppe($dummyObject);

		$this->assertSame(
			$dummyObject,
			$this->fixture->getZielgruppe()
		);
	}
	
}
?>