<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_Ejwintern_Domain_Model_Gemeinde.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EJW Intern
 *
 * @author Paul Rohrbeck <paul@paul-rohrbeck.de>
 */
class Tx_Ejwintern_Domain_Model_GemeindeTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_Ejwintern_Domain_Model_Gemeinde
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_Ejwintern_Domain_Model_Gemeinde();
	}

	public function tearDown() {
		unset($this->fixture);
	}
	
	
	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setNameForStringSetsName() { 
		$this->fixture->setName('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getName()
		);
	}
	
	/**
	 * @test
	 */
	public function getStrasseReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setStrasseForStringSetsStrasse() { 
		$this->fixture->setStrasse('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getStrasse()
		);
	}
	
	/**
	 * @test
	 */
	public function getHausnumerReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getHausnumer()
		);
	}

	/**
	 * @test
	 */
	public function setHausnumerForIntegerSetsHausnumer() { 
		$this->fixture->setHausnumer(12);

		$this->assertSame(
			12,
			$this->fixture->getHausnumer()
		);
	}
	
	/**
	 * @test
	 */
	public function getPlzReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getPlz()
		);
	}

	/**
	 * @test
	 */
	public function setPlzForIntegerSetsPlz() { 
		$this->fixture->setPlz(12);

		$this->assertSame(
			12,
			$this->fixture->getPlz()
		);
	}
	
	/**
	 * @test
	 */
	public function getOrtReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setOrtForStringSetsOrt() { 
		$this->fixture->setOrt('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getOrt()
		);
	}
	
	/**
	 * @test
	 */
	public function getTelefonReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setTelefonForStringSetsTelefon() { 
		$this->fixture->setTelefon('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getTelefon()
		);
	}
	
	/**
	 * @test
	 */
	public function getWebseiteReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setWebseiteForStringSetsWebseite() { 
		$this->fixture->setWebseite('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getWebseite()
		);
	}
	
	/**
	 * @test
	 */
	public function getPfarrerReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setPfarrerForStringSetsPfarrer() { 
		$this->fixture->setPfarrer('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getPfarrer()
		);
	}
	
	/**
	 * @test
	 */
	public function getOrtswerkReturnsInitialValueForTx_Ejwintern_Domain_Model_Ortswerk() { 
		$this->assertEquals(
			NULL,
			$this->fixture->getOrtswerk()
		);
	}

	/**
	 * @test
	 */
	public function setOrtswerkForTx_Ejwintern_Domain_Model_OrtswerkSetsOrtswerk() { 
		$dummyObject = new Tx_Ejwintern_Domain_Model_Ortswerk();
		$this->fixture->setOrtswerk($dummyObject);

		$this->assertSame(
			$dummyObject,
			$this->fixture->getOrtswerk()
		);
	}
	
	/**
	 * @test
	 */
	public function getAnsprechpartnerReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Personen() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getAnsprechpartner()
		);
	}

	/**
	 * @test
	 */
	public function setAnsprechpartnerForObjectStorageContainingTx_Ejwintern_Domain_Model_PersonenSetsAnsprechpartner() { 
		$ansprechpartner = new Tx_Ejwintern_Domain_Model_Personen();
		$objectStorageHoldingExactlyOneAnsprechpartner = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneAnsprechpartner->attach($ansprechpartner);
		$this->fixture->setAnsprechpartner($objectStorageHoldingExactlyOneAnsprechpartner);

		$this->assertSame(
			$objectStorageHoldingExactlyOneAnsprechpartner,
			$this->fixture->getAnsprechpartner()
		);
	}
	
	/**
	 * @test
	 */
	public function addAnsprechpartnerToObjectStorageHoldingAnsprechpartner() {
		$ansprechpartner = new Tx_Ejwintern_Domain_Model_Personen();
		$objectStorageHoldingExactlyOneAnsprechpartner = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneAnsprechpartner->attach($ansprechpartner);
		$this->fixture->addAnsprechpartner($ansprechpartner);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneAnsprechpartner,
			$this->fixture->getAnsprechpartner()
		);
	}

	/**
	 * @test
	 */
	public function removeAnsprechpartnerFromObjectStorageHoldingAnsprechpartner() {
		$ansprechpartner = new Tx_Ejwintern_Domain_Model_Personen();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($ansprechpartner);
		$localObjectStorage->detach($ansprechpartner);
		$this->fixture->addAnsprechpartner($ansprechpartner);
		$this->fixture->removeAnsprechpartner($ansprechpartner);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getAnsprechpartner()
		);
	}
	
	/**
	 * @test
	 */
	public function getGruppeReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Gruppe() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getGruppe()
		);
	}

	/**
	 * @test
	 */
	public function setGruppeForObjectStorageContainingTx_Ejwintern_Domain_Model_GruppeSetsGruppe() { 
		$gruppe = new Tx_Ejwintern_Domain_Model_Gruppe();
		$objectStorageHoldingExactlyOneGruppe = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneGruppe->attach($gruppe);
		$this->fixture->setGruppe($objectStorageHoldingExactlyOneGruppe);

		$this->assertSame(
			$objectStorageHoldingExactlyOneGruppe,
			$this->fixture->getGruppe()
		);
	}
	
	/**
	 * @test
	 */
	public function addGruppeToObjectStorageHoldingGruppe() {
		$gruppe = new Tx_Ejwintern_Domain_Model_Gruppe();
		$objectStorageHoldingExactlyOneGruppe = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneGruppe->attach($gruppe);
		$this->fixture->addGruppe($gruppe);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneGruppe,
			$this->fixture->getGruppe()
		);
	}

	/**
	 * @test
	 */
	public function removeGruppeFromObjectStorageHoldingGruppe() {
		$gruppe = new Tx_Ejwintern_Domain_Model_Gruppe();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($gruppe);
		$localObjectStorage->detach($gruppe);
		$this->fixture->addGruppe($gruppe);
		$this->fixture->removeGruppe($gruppe);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getGruppe()
		);
	}
	
}
?>