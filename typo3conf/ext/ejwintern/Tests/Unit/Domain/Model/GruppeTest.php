<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_Ejwintern_Domain_Model_Gruppe.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EJW Intern
 *
 * @author Paul Rohrbeck <paul@paul-rohrbeck.de>
 */
class Tx_Ejwintern_Domain_Model_GruppeTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_Ejwintern_Domain_Model_Gruppe
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_Ejwintern_Domain_Model_Gruppe();
	}

	public function tearDown() {
		unset($this->fixture);
	}
	
	
	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setNameForStringSetsName() { 
		$this->fixture->setName('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getName()
		);
	}
	
	/**
	 * @test
	 */
	public function getAlterStartReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getAlterStart()
		);
	}

	/**
	 * @test
	 */
	public function setAlterStartForIntegerSetsAlterStart() { 
		$this->fixture->setAlterStart(12);

		$this->assertSame(
			12,
			$this->fixture->getAlterStart()
		);
	}
	
	/**
	 * @test
	 */
	public function getAlterEndeReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getAlterEnde()
		);
	}

	/**
	 * @test
	 */
	public function setAlterEndeForIntegerSetsAlterEnde() { 
		$this->fixture->setAlterEnde(12);

		$this->assertSame(
			12,
			$this->fixture->getAlterEnde()
		);
	}
	
	/**
	 * @test
	 */
	public function getUhrzeitStartReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setUhrzeitStartForStringSetsUhrzeitStart() { 
		$this->fixture->setUhrzeitStart('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getUhrzeitStart()
		);
	}
	
	/**
	 * @test
	 */
	public function getUhrzeitEndeReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setUhrzeitEndeForStringSetsUhrzeitEnde() { 
		$this->fixture->setUhrzeitEnde('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getUhrzeitEnde()
		);
	}
	
	/**
	 * @test
	 */
	public function getWochentagReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setWochentagForStringSetsWochentag() { 
		$this->fixture->setWochentag('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getWochentag()
		);
	}
	
	/**
	 * @test
	 */
	public function getStrasseReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setStrasseForStringSetsStrasse() { 
		$this->fixture->setStrasse('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getStrasse()
		);
	}
	
	/**
	 * @test
	 */
	public function getHausnummerReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setHausnummerForStringSetsHausnummer() { 
		$this->fixture->setHausnummer('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getHausnummer()
		);
	}
	
	/**
	 * @test
	 */
	public function getPlzReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setPlzForStringSetsPlz() { 
		$this->fixture->setPlz('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getPlz()
		);
	}
	
	/**
	 * @test
	 */
	public function getOrtReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setOrtForStringSetsOrt() { 
		$this->fixture->setOrt('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getOrt()
		);
	}
	
	/**
	 * @test
	 */
	public function getGruppenleiterReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setGruppenleiterForStringSetsGruppenleiter() { 
		$this->fixture->setGruppenleiter('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getGruppenleiter()
		);
	}
	
	/**
	 * @test
	 */
	public function getGemeindeReturnsInitialValueForTx_Ejwintern_Domain_Model_Gemeinde() { 
		$this->assertEquals(
			NULL,
			$this->fixture->getGemeinde()
		);
	}

	/**
	 * @test
	 */
	public function setGemeindeForTx_Ejwintern_Domain_Model_GemeindeSetsGemeinde() { 
		$dummyObject = new Tx_Ejwintern_Domain_Model_Gemeinde();
		$this->fixture->setGemeinde($dummyObject);

		$this->assertSame(
			$dummyObject,
			$this->fixture->getGemeinde()
		);
	}
	
	/**
	 * @test
	 */
	public function getFachgruppeReturnsInitialValueForTx_Ejwintern_Domain_Model_Fachgruppe() { 
		$this->assertEquals(
			NULL,
			$this->fixture->getFachgruppe()
		);
	}

	/**
	 * @test
	 */
	public function setFachgruppeForTx_Ejwintern_Domain_Model_FachgruppeSetsFachgruppe() { 
		$dummyObject = new Tx_Ejwintern_Domain_Model_Fachgruppe();
		$this->fixture->setFachgruppe($dummyObject);

		$this->assertSame(
			$dummyObject,
			$this->fixture->getFachgruppe()
		);
	}
	
	/**
	 * @test
	 */
	public function getAnsprechpartnerReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Personen() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getAnsprechpartner()
		);
	}

	/**
	 * @test
	 */
	public function setAnsprechpartnerForObjectStorageContainingTx_Ejwintern_Domain_Model_PersonenSetsAnsprechpartner() { 
		$ansprechpartner = new Tx_Ejwintern_Domain_Model_Personen();
		$objectStorageHoldingExactlyOneAnsprechpartner = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneAnsprechpartner->attach($ansprechpartner);
		$this->fixture->setAnsprechpartner($objectStorageHoldingExactlyOneAnsprechpartner);

		$this->assertSame(
			$objectStorageHoldingExactlyOneAnsprechpartner,
			$this->fixture->getAnsprechpartner()
		);
	}
	
	/**
	 * @test
	 */
	public function addAnsprechpartnerToObjectStorageHoldingAnsprechpartner() {
		$ansprechpartner = new Tx_Ejwintern_Domain_Model_Personen();
		$objectStorageHoldingExactlyOneAnsprechpartner = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneAnsprechpartner->attach($ansprechpartner);
		$this->fixture->addAnsprechpartner($ansprechpartner);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneAnsprechpartner,
			$this->fixture->getAnsprechpartner()
		);
	}

	/**
	 * @test
	 */
	public function removeAnsprechpartnerFromObjectStorageHoldingAnsprechpartner() {
		$ansprechpartner = new Tx_Ejwintern_Domain_Model_Personen();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($ansprechpartner);
		$localObjectStorage->detach($ansprechpartner);
		$this->fixture->addAnsprechpartner($ansprechpartner);
		$this->fixture->removeAnsprechpartner($ansprechpartner);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getAnsprechpartner()
		);
	}
	
	/**
	 * @test
	 */
	public function getTurnusReturnsInitialValueForTx_Ejwintern_Domain_Model_Turnus() { 
		$this->assertEquals(
			NULL,
			$this->fixture->getTurnus()
		);
	}

	/**
	 * @test
	 */
	public function setTurnusForTx_Ejwintern_Domain_Model_TurnusSetsTurnus() { 
		$dummyObject = new Tx_Ejwintern_Domain_Model_Turnus();
		$this->fixture->setTurnus($dummyObject);

		$this->assertSame(
			$dummyObject,
			$this->fixture->getTurnus()
		);
	}
	
	/**
	 * @test
	 */
	public function getZielgruppeReturnsInitialValueForTx_Ejwintern_Domain_Model_Kategorien() { 
		$this->assertEquals(
			NULL,
			$this->fixture->getZielgruppe()
		);
	}

	/**
	 * @test
	 */
	public function setZielgruppeForTx_Ejwintern_Domain_Model_KategorienSetsZielgruppe() { 
		$dummyObject = new Tx_Ejwintern_Domain_Model_Kategorien();
		$this->fixture->setZielgruppe($dummyObject);

		$this->assertSame(
			$dummyObject,
			$this->fixture->getZielgruppe()
		);
	}
	
}
?>