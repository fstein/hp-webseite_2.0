<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_Ejwintern_Domain_Model_Leistung.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EJW Intern
 *
 * @author Paul Rohrbeck <paul@paul-rohrbeck.de>
 */
class Tx_Ejwintern_Domain_Model_LeistungTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_Ejwintern_Domain_Model_Leistung
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_Ejwintern_Domain_Model_Leistung();
	}

	public function tearDown() {
		unset($this->fixture);
	}
	
	
	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setNameForStringSetsName() { 
		$this->fixture->setName('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getName()
		);
	}
	
	/**
	 * @test
	 */
	public function getFreizeitenReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Freizeit() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getFreizeiten()
		);
	}

	/**
	 * @test
	 */
	public function setFreizeitenForObjectStorageContainingTx_Ejwintern_Domain_Model_FreizeitSetsFreizeiten() { 
		$freizeiten = new Tx_Ejwintern_Domain_Model_Freizeit();
		$objectStorageHoldingExactlyOneFreizeiten = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneFreizeiten->attach($freizeiten);
		$this->fixture->setFreizeiten($objectStorageHoldingExactlyOneFreizeiten);

		$this->assertSame(
			$objectStorageHoldingExactlyOneFreizeiten,
			$this->fixture->getFreizeiten()
		);
	}
	
	/**
	 * @test
	 */
	public function addFreizeitenToObjectStorageHoldingFreizeiten() {
		$freizeiten = new Tx_Ejwintern_Domain_Model_Freizeit();
		$objectStorageHoldingExactlyOneFreizeiten = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneFreizeiten->attach($freizeiten);
		$this->fixture->addFreizeiten($freizeiten);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneFreizeiten,
			$this->fixture->getFreizeiten()
		);
	}

	/**
	 * @test
	 */
	public function removeFreizeitenFromObjectStorageHoldingFreizeiten() {
		$freizeiten = new Tx_Ejwintern_Domain_Model_Freizeit();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($freizeiten);
		$localObjectStorage->detach($freizeiten);
		$this->fixture->addFreizeiten($freizeiten);
		$this->fixture->removeFreizeiten($freizeiten);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getFreizeiten()
		);
	}
	
}
?>