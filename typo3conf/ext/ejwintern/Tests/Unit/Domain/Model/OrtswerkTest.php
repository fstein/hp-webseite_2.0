<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_Ejwintern_Domain_Model_Ortswerk.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EJW Intern
 *
 * @author Paul Rohrbeck <paul@paul-rohrbeck.de>
 */
class Tx_Ejwintern_Domain_Model_OrtswerkTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_Ejwintern_Domain_Model_Ortswerk
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_Ejwintern_Domain_Model_Ortswerk();
	}

	public function tearDown() {
		unset($this->fixture);
	}
	
	
	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setNameForStringSetsName() { 
		$this->fixture->setName('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getName()
		);
	}
	
	/**
	 * @test
	 */
	public function getWebsiteReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setWebsiteForStringSetsWebsite() { 
		$this->fixture->setWebsite('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getWebsite()
		);
	}
	
	/**
	 * @test
	 */
	public function getAnsprechpartnerReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Personen() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getAnsprechpartner()
		);
	}

	/**
	 * @test
	 */
	public function setAnsprechpartnerForObjectStorageContainingTx_Ejwintern_Domain_Model_PersonenSetsAnsprechpartner() { 
		$ansprechpartner = new Tx_Ejwintern_Domain_Model_Personen();
		$objectStorageHoldingExactlyOneAnsprechpartner = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneAnsprechpartner->attach($ansprechpartner);
		$this->fixture->setAnsprechpartner($objectStorageHoldingExactlyOneAnsprechpartner);

		$this->assertSame(
			$objectStorageHoldingExactlyOneAnsprechpartner,
			$this->fixture->getAnsprechpartner()
		);
	}
	
	/**
	 * @test
	 */
	public function addAnsprechpartnerToObjectStorageHoldingAnsprechpartner() {
		$ansprechpartner = new Tx_Ejwintern_Domain_Model_Personen();
		$objectStorageHoldingExactlyOneAnsprechpartner = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneAnsprechpartner->attach($ansprechpartner);
		$this->fixture->addAnsprechpartner($ansprechpartner);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneAnsprechpartner,
			$this->fixture->getAnsprechpartner()
		);
	}

	/**
	 * @test
	 */
	public function removeAnsprechpartnerFromObjectStorageHoldingAnsprechpartner() {
		$ansprechpartner = new Tx_Ejwintern_Domain_Model_Personen();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($ansprechpartner);
		$localObjectStorage->detach($ansprechpartner);
		$this->fixture->addAnsprechpartner($ansprechpartner);
		$this->fixture->removeAnsprechpartner($ansprechpartner);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getAnsprechpartner()
		);
	}
	
	/**
	 * @test
	 */
	public function getGemeindeReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Gemeinde() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getGemeinde()
		);
	}

	/**
	 * @test
	 */
	public function setGemeindeForObjectStorageContainingTx_Ejwintern_Domain_Model_GemeindeSetsGemeinde() { 
		$gemeinde = new Tx_Ejwintern_Domain_Model_Gemeinde();
		$objectStorageHoldingExactlyOneGemeinde = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneGemeinde->attach($gemeinde);
		$this->fixture->setGemeinde($objectStorageHoldingExactlyOneGemeinde);

		$this->assertSame(
			$objectStorageHoldingExactlyOneGemeinde,
			$this->fixture->getGemeinde()
		);
	}
	
	/**
	 * @test
	 */
	public function addGemeindeToObjectStorageHoldingGemeinde() {
		$gemeinde = new Tx_Ejwintern_Domain_Model_Gemeinde();
		$objectStorageHoldingExactlyOneGemeinde = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneGemeinde->attach($gemeinde);
		$this->fixture->addGemeinde($gemeinde);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneGemeinde,
			$this->fixture->getGemeinde()
		);
	}

	/**
	 * @test
	 */
	public function removeGemeindeFromObjectStorageHoldingGemeinde() {
		$gemeinde = new Tx_Ejwintern_Domain_Model_Gemeinde();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($gemeinde);
		$localObjectStorage->detach($gemeinde);
		$this->fixture->addGemeinde($gemeinde);
		$this->fixture->removeGemeinde($gemeinde);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getGemeinde()
		);
	}
	
}
?>