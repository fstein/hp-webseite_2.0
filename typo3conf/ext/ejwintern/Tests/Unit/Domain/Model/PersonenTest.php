<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Paul Rohrbeck <paul@paul-rohrbeck.de>, paul-rohrbeck.de
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_Ejwintern_Domain_Model_Personen.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EJW Intern
 *
 * @author Paul Rohrbeck <paul@paul-rohrbeck.de>
 */
class Tx_Ejwintern_Domain_Model_PersonenTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_Ejwintern_Domain_Model_Personen
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_Ejwintern_Domain_Model_Personen();
	}

	public function tearDown() {
		unset($this->fixture);
	}
	
	
	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setNameForStringSetsName() { 
		$this->fixture->setName('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getName()
		);
	}
	
	/**
	 * @test
	 */
	public function getGeburtsdatumReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setGeburtsdatumForStringSetsGeburtsdatum() { 
		$this->fixture->setGeburtsdatum('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getGeburtsdatum()
		);
	}
	
	/**
	 * @test
	 */
	public function getTelephoneReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setTelephoneForStringSetsTelephone() { 
		$this->fixture->setTelephone('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getTelephone()
		);
	}
	
	/**
	 * @test
	 */
	public function getEmailReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setEmailForStringSetsEmail() { 
		$this->fixture->setEmail('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getEmail()
		);
	}
	
	/**
	 * @test
	 */
	public function getImageReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setImageForStringSetsImage() { 
		$this->fixture->setImage('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getImage()
		);
	}
	
	/**
	 * @test
	 */
	public function getHauptamtlichReturnsInitialValueForBoolean() { 
		$this->assertSame(
			TRUE,
			$this->fixture->getHauptamtlich()
		);
	}

	/**
	 * @test
	 */
	public function setHauptamtlichForBooleanSetsHauptamtlich() { 
		$this->fixture->setHauptamtlich(TRUE);

		$this->assertSame(
			TRUE,
			$this->fixture->getHauptamtlich()
		);
	}
	
	/**
	 * @test
	 */
	public function getInfoReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setInfoForStringSetsInfo() { 
		$this->fixture->setInfo('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getInfo()
		);
	}
	
	/**
	 * @test
	 */
	public function getOrtswerkReturnsInitialValueForTx_Ejwintern_Domain_Model_Ortswerk() { 
		$this->assertEquals(
			NULL,
			$this->fixture->getOrtswerk()
		);
	}

	/**
	 * @test
	 */
	public function setOrtswerkForTx_Ejwintern_Domain_Model_OrtswerkSetsOrtswerk() { 
		$dummyObject = new Tx_Ejwintern_Domain_Model_Ortswerk();
		$this->fixture->setOrtswerk($dummyObject);

		$this->assertSame(
			$dummyObject,
			$this->fixture->getOrtswerk()
		);
	}
	
	/**
	 * @test
	 */
	public function getFachgruppeReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Fachgruppe() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getFachgruppe()
		);
	}

	/**
	 * @test
	 */
	public function setFachgruppeForObjectStorageContainingTx_Ejwintern_Domain_Model_FachgruppeSetsFachgruppe() { 
		$fachgruppe = new Tx_Ejwintern_Domain_Model_Fachgruppe();
		$objectStorageHoldingExactlyOneFachgruppe = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneFachgruppe->attach($fachgruppe);
		$this->fixture->setFachgruppe($objectStorageHoldingExactlyOneFachgruppe);

		$this->assertSame(
			$objectStorageHoldingExactlyOneFachgruppe,
			$this->fixture->getFachgruppe()
		);
	}
	
	/**
	 * @test
	 */
	public function addFachgruppeToObjectStorageHoldingFachgruppe() {
		$fachgruppe = new Tx_Ejwintern_Domain_Model_Fachgruppe();
		$objectStorageHoldingExactlyOneFachgruppe = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneFachgruppe->attach($fachgruppe);
		$this->fixture->addFachgruppe($fachgruppe);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneFachgruppe,
			$this->fixture->getFachgruppe()
		);
	}

	/**
	 * @test
	 */
	public function removeFachgruppeFromObjectStorageHoldingFachgruppe() {
		$fachgruppe = new Tx_Ejwintern_Domain_Model_Fachgruppe();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($fachgruppe);
		$localObjectStorage->detach($fachgruppe);
		$this->fixture->addFachgruppe($fachgruppe);
		$this->fixture->removeFachgruppe($fachgruppe);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getFachgruppe()
		);
	}
	
	/**
	 * @test
	 */
	public function getGemeindeReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Gemeinde() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getGemeinde()
		);
	}

	/**
	 * @test
	 */
	public function setGemeindeForObjectStorageContainingTx_Ejwintern_Domain_Model_GemeindeSetsGemeinde() { 
		$gemeinde = new Tx_Ejwintern_Domain_Model_Gemeinde();
		$objectStorageHoldingExactlyOneGemeinde = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneGemeinde->attach($gemeinde);
		$this->fixture->setGemeinde($objectStorageHoldingExactlyOneGemeinde);

		$this->assertSame(
			$objectStorageHoldingExactlyOneGemeinde,
			$this->fixture->getGemeinde()
		);
	}
	
	/**
	 * @test
	 */
	public function addGemeindeToObjectStorageHoldingGemeinde() {
		$gemeinde = new Tx_Ejwintern_Domain_Model_Gemeinde();
		$objectStorageHoldingExactlyOneGemeinde = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneGemeinde->attach($gemeinde);
		$this->fixture->addGemeinde($gemeinde);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneGemeinde,
			$this->fixture->getGemeinde()
		);
	}

	/**
	 * @test
	 */
	public function removeGemeindeFromObjectStorageHoldingGemeinde() {
		$gemeinde = new Tx_Ejwintern_Domain_Model_Gemeinde();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($gemeinde);
		$localObjectStorage->detach($gemeinde);
		$this->fixture->addGemeinde($gemeinde);
		$this->fixture->removeGemeinde($gemeinde);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getGemeinde()
		);
	}
	
	/**
	 * @test
	 */
	public function getGruppeReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Gruppe() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getGruppe()
		);
	}

	/**
	 * @test
	 */
	public function setGruppeForObjectStorageContainingTx_Ejwintern_Domain_Model_GruppeSetsGruppe() { 
		$gruppe = new Tx_Ejwintern_Domain_Model_Gruppe();
		$objectStorageHoldingExactlyOneGruppe = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneGruppe->attach($gruppe);
		$this->fixture->setGruppe($objectStorageHoldingExactlyOneGruppe);

		$this->assertSame(
			$objectStorageHoldingExactlyOneGruppe,
			$this->fixture->getGruppe()
		);
	}
	
	/**
	 * @test
	 */
	public function addGruppeToObjectStorageHoldingGruppe() {
		$gruppe = new Tx_Ejwintern_Domain_Model_Gruppe();
		$objectStorageHoldingExactlyOneGruppe = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneGruppe->attach($gruppe);
		$this->fixture->addGruppe($gruppe);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneGruppe,
			$this->fixture->getGruppe()
		);
	}

	/**
	 * @test
	 */
	public function removeGruppeFromObjectStorageHoldingGruppe() {
		$gruppe = new Tx_Ejwintern_Domain_Model_Gruppe();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($gruppe);
		$localObjectStorage->detach($gruppe);
		$this->fixture->addGruppe($gruppe);
		$this->fixture->removeGruppe($gruppe);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getGruppe()
		);
	}
	
	/**
	 * @test
	 */
	public function getFreizeitReturnsInitialValueForObjectStorageContainingTx_Ejwintern_Domain_Model_Freizeit() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getFreizeit()
		);
	}

	/**
	 * @test
	 */
	public function setFreizeitForObjectStorageContainingTx_Ejwintern_Domain_Model_FreizeitSetsFreizeit() { 
		$freizeit = new Tx_Ejwintern_Domain_Model_Freizeit();
		$objectStorageHoldingExactlyOneFreizeit = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneFreizeit->attach($freizeit);
		$this->fixture->setFreizeit($objectStorageHoldingExactlyOneFreizeit);

		$this->assertSame(
			$objectStorageHoldingExactlyOneFreizeit,
			$this->fixture->getFreizeit()
		);
	}
	
	/**
	 * @test
	 */
	public function addFreizeitToObjectStorageHoldingFreizeit() {
		$freizeit = new Tx_Ejwintern_Domain_Model_Freizeit();
		$objectStorageHoldingExactlyOneFreizeit = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneFreizeit->attach($freizeit);
		$this->fixture->addFreizeit($freizeit);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneFreizeit,
			$this->fixture->getFreizeit()
		);
	}

	/**
	 * @test
	 */
	public function removeFreizeitFromObjectStorageHoldingFreizeit() {
		$freizeit = new Tx_Ejwintern_Domain_Model_Freizeit();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($freizeit);
		$localObjectStorage->detach($freizeit);
		$this->fixture->addFreizeit($freizeit);
		$this->fixture->removeFreizeit($freizeit);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getFreizeit()
		);
	}
	
}
?>