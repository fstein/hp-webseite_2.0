<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "ejwintern".
 *
 * Auto generated 19-07-2014 13:33
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'EJW Intern',
	'description' => 'Hauptamtliche, Gemeinden, Freizeiten- und Gruppenfinder.',
	'category' => 'plugin',
	'author' => 'Paul Rohrbeck',
	'author_email' => 'paul@paul-rohrbeck.de',
	'author_company' => 'paul-rohrbeck.de',
	'state' => 'beta',
	'uploadfolder' => 1,
	'createDirs' => 'uploads/tx_ejwintern/',
	'clearCacheOnLoad' => 0,
	'version' => '0.0.6',
	'constraints' => 
	array (
		'depends' => 
		array (
			'cms' => '',
			'extbase' => '',
			'fluid' => '',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
	'_md5_values_when_last_written' => 'a:103:{s:21:"ExtensionBuilder.json";s:4:"d5ec";s:16:"ext_autoload.php";s:4:"e122";s:12:"ext_icon.gif";s:4:"e922";s:17:"ext_localconf.php";s:4:"1e22";s:14:"ext_tables.php";s:4:"1179";s:14:"ext_tables.sql";s:4:"6eb8";s:41:"Classes/Controller/FreizeitController.php";s:4:"e6e2";s:41:"Classes/Controller/GemeindeController.php";s:4:"1230";s:39:"Classes/Controller/GruppeController.php";s:4:"37ad";s:42:"Classes/Controller/GruppeModController.php";s:4:"de79";s:41:"Classes/Controller/PersonenController.php";s:4:"c378";s:35:"Classes/Domain/Model/Fachgruppe.php";s:4:"b8a7";s:33:"Classes/Domain/Model/Freizeit.php";s:4:"3cbd";s:33:"Classes/Domain/Model/Gemeinde.php";s:4:"4a5d";s:31:"Classes/Domain/Model/Gruppe.php";s:4:"8b88";s:35:"Classes/Domain/Model/Kategorien.php";s:4:"c2e8";s:33:"Classes/Domain/Model/Leistung.php";s:4:"e4ea";s:33:"Classes/Domain/Model/Ortswerk.php";s:4:"9da0";s:33:"Classes/Domain/Model/Personen.php";s:4:"85c0";s:48:"Classes/Domain/Repository/FreizeitRepository.php";s:4:"8d47";s:48:"Classes/Domain/Repository/GemeindeRepository.php";s:4:"a62a";s:46:"Classes/Domain/Repository/GruppeRepository.php";s:4:"a50a";s:50:"Classes/Domain/Repository/KategorienRepository.php";s:4:"6fe8";s:48:"Classes/Domain/Repository/LeistungRepository.php";s:4:"0fa5";s:48:"Classes/Domain/Repository/PersonenRepository.php";s:4:"7bd4";s:43:"Classes/ViewHelpers/AdvselectViewHelper.php";s:4:"94b5";s:41:"Classes/ViewHelpers/ExplodeViewHelper.php";s:4:"3519";s:47:"Classes/ViewHelpers/FreizeitdatumViewHelper.php";s:4:"6495";s:43:"Classes/ViewHelpers/OpengraphViewHelper.php";s:4:"5170";s:41:"Classes/ViewHelpers/PaddingViewHelper.php";s:4:"48ea";s:42:"Classes/ViewHelpers/StrftimeViewHelper.php";s:4:"df2e";s:44:"Classes/ViewHelpers/StrreplaceViewHelper.php";s:4:"a8a5";s:39:"Classes/ViewHelpers/TitleViewHelper.php";s:4:"6750";s:44:"Configuration/ExtensionBuilder/settings.yaml";s:4:"8bfc";s:42:"Configuration/FlexForms/freizeitfinder.xml";s:4:"023f";s:32:"Configuration/TCA/Fachgruppe.php";s:4:"0a72";s:30:"Configuration/TCA/Freizeit.php";s:4:"4b7f";s:30:"Configuration/TCA/Gemeinde.php";s:4:"455b";s:28:"Configuration/TCA/Gruppe.php";s:4:"6bc4";s:32:"Configuration/TCA/Kategorien.php";s:4:"0dbb";s:30:"Configuration/TCA/Leistung.php";s:4:"5f40";s:30:"Configuration/TCA/Ortswerk.php";s:4:"e258";s:30:"Configuration/TCA/Personen.php";s:4:"3691";s:38:"Configuration/TypoScript/constants.txt";s:4:"360b";s:34:"Configuration/TypoScript/setup.txt";s:4:"e392";s:40:"Resources/Private/Language/locallang.xml";s:4:"a38b";s:81:"Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_fachgruppe.xml";s:4:"4f70";s:79:"Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_freizeit.xml";s:4:"f246";s:79:"Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_gemeinde.xml";s:4:"01fc";s:77:"Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_gruppe.xml";s:4:"9a84";s:81:"Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_kategorien.xml";s:4:"4452";s:79:"Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_leistung.xml";s:4:"f3fd";s:79:"Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_ortswerk.xml";s:4:"503e";s:79:"Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_personen.xml";s:4:"78e6";s:43:"Resources/Private/Language/locallang_db.xml";s:4:"325d";s:44:"Resources/Private/Language/locallang_mod.xml";s:4:"55bd";s:38:"Resources/Private/Layouts/Default.html";s:4:"222a";s:51:"Resources/Private/Partials/Freizeit/Properties.html";s:4:"1fc7";s:46:"Resources/Private/Templates/Freizeit/List.html";s:4:"deea";s:46:"Resources/Private/Templates/Freizeit/Show.html";s:4:"d300";s:46:"Resources/Private/Templates/Gemeinde/Show.html";s:4:"ee14";s:44:"Resources/Private/Templates/Gruppe/List.html";s:4:"268b";s:47:"Resources/Private/Templates/GruppeMod/List.html";s:4:"c689";s:46:"Resources/Private/Templates/Personen/List.html";s:4:"37b1";s:38:"Resources/Public/CSS/gruppenfinder.css";s:4:"c345";s:36:"Resources/Public/CSS/nivo-slider.css";s:4:"8a8f";s:35:"Resources/Public/Icons/relation.gif";s:4:"e615";s:63:"Resources/Public/Icons/tx_ejwintern_domain_model_fachgruppe.gif";s:4:"1103";s:61:"Resources/Public/Icons/tx_ejwintern_domain_model_freizeit.gif";s:4:"905a";s:61:"Resources/Public/Icons/tx_ejwintern_domain_model_gemeinde.gif";s:4:"1103";s:59:"Resources/Public/Icons/tx_ejwintern_domain_model_gruppe.gif";s:4:"905a";s:63:"Resources/Public/Icons/tx_ejwintern_domain_model_kategorien.gif";s:4:"1103";s:61:"Resources/Public/Icons/tx_ejwintern_domain_model_leistung.gif";s:4:"1103";s:61:"Resources/Public/Icons/tx_ejwintern_domain_model_ortswerk.gif";s:4:"1103";s:61:"Resources/Public/Icons/tx_ejwintern_domain_model_personen.gif";s:4:"905a";s:59:"Resources/Public/Icons/tx_ejwintern_domain_model_turnus.gif";s:4:"1103";s:41:"Resources/Public/Images/GooglePin-HMP.png";s:4:"d9c0";s:40:"Resources/Public/Images/GooglePin-HP.png";s:4:"8898";s:40:"Resources/Public/Images/GooglePin-MA.png";s:4:"8952";s:46:"Resources/Public/Images/GooglePin-standort.png";s:4:"eeea";s:41:"Resources/Public/Images/belegt-banner.png";s:4:"55c7";s:40:"Resources/Public/Images/close-bubble.png";s:4:"9adb";s:49:"Resources/Public/Images/onlineanmelden_belegt.png";s:4:"4d0c";s:42:"Resources/Public/JS/_test_gruppenfinder.js";s:4:"9a06";s:40:"Resources/Public/JS/freizeiten_detail.js";s:4:"7a33";s:38:"Resources/Public/JS/gemeinde_detail.js";s:4:"0ee9";s:36:"Resources/Public/JS/gruppenfinder.js";s:4:"1e0d";s:42:"Resources/Public/JS/infobubble-compiled.js";s:4:"b295";s:33:"Resources/Public/JS/infobubble.js";s:4:"a792";s:30:"Resources/Public/JS/oms.min.js";s:4:"6f97";s:48:"Tests/Unit/Controller/FreizeitControllerTest.php";s:4:"8e81";s:46:"Tests/Unit/Controller/GruppeControllerTest.php";s:4:"f954";s:48:"Tests/Unit/Controller/PersonenControllerTest.php";s:4:"bc6a";s:42:"Tests/Unit/Domain/Model/FachgruppeTest.php";s:4:"629e";s:40:"Tests/Unit/Domain/Model/FreizeitTest.php";s:4:"bf3a";s:40:"Tests/Unit/Domain/Model/GemeindeTest.php";s:4:"acc8";s:38:"Tests/Unit/Domain/Model/GruppeTest.php";s:4:"adf6";s:42:"Tests/Unit/Domain/Model/KategorienTest.php";s:4:"4299";s:40:"Tests/Unit/Domain/Model/LeistungTest.php";s:4:"f504";s:40:"Tests/Unit/Domain/Model/OrtswerkTest.php";s:4:"0917";s:40:"Tests/Unit/Domain/Model/PersonenTest.php";s:4:"aa0e";s:38:"Tests/Unit/Domain/Model/TurnusTest.php";s:4:"4dae";s:14:"doc/manual.sxw";s:4:"8d2d";}',
);

