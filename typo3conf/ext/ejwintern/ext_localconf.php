<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::configurePlugin(
	$_EXTKEY,
	'Freizeitfinder',
	array(
		'Freizeit' => 'list,show'		
	),
	// non-cacheable actions
	array()
);

Tx_Extbase_Utility_Extension::configurePlugin(
	$_EXTKEY,
	'Gruppenfinder',
	array(
		'Gruppe' => 'list'
	),
	// non-cacheable actions
	array()
);

Tx_Extbase_Utility_Extension::configurePlugin(
	$_EXTKEY,
	'Hauptamtliche',
	array(
		'Personen' => 'list'	
	),
	// non-cacheable actions
	array()
);

Tx_Extbase_Utility_Extension::configurePlugin(
	$_EXTKEY,
	'Einzelgemeinde',
	array(
		'Gemeinde' => 'show'	
	),
	// non-cacheable actions
	array()	
);

?>