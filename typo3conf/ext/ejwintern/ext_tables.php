<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Freizeitfinder',
	'Freizeitfinder'
);

$pluginSignature = str_replace('_','',$_EXTKEY) . '_' . freizeitfinder;
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
t3lib_extMgm::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/freizeitfinder.xml');


Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Gruppenfinder',
	'Gruppenfinder'
);

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Hauptamtliche',
	'Hauptamtliche'
);

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Einzelgemeinde',
	'Einzelgemeinde'
);

t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', '[EJW Intern] Basic');

t3lib_extMgm::addLLrefForTCAdescr('tx_ejwintern_domain_model_freizeit', 'EXT:ejwintern/Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_freizeit.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_ejwintern_domain_model_freizeit');
$TCA['tx_ejwintern_domain_model_freizeit'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_freizeit',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Freizeit.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_ejwintern_domain_model_freizeit.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_ejwintern_domain_model_fachgruppe', 'EXT:ejwintern/Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_fachgruppe.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_ejwintern_domain_model_fachgruppe');
$TCA['tx_ejwintern_domain_model_fachgruppe'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_fachgruppe',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Fachgruppe.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_ejwintern_domain_model_fachgruppe.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_ejwintern_domain_model_gemeinde', 'EXT:ejwintern/Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_gemeinde.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_ejwintern_domain_model_gemeinde');
$TCA['tx_ejwintern_domain_model_gemeinde'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gemeinde',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Gemeinde.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_ejwintern_domain_model_gemeinde.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_ejwintern_domain_model_leistung', 'EXT:ejwintern/Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_leistung.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_ejwintern_domain_model_leistung');
$TCA['tx_ejwintern_domain_model_leistung'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_leistung',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Leistung.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_ejwintern_domain_model_leistung.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_ejwintern_domain_model_gruppe', 'EXT:ejwintern/Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_gruppe.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_ejwintern_domain_model_gruppe');
$TCA['tx_ejwintern_domain_model_gruppe'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_gruppe',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Gruppe.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_ejwintern_domain_model_gruppe.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_ejwintern_domain_model_ortswerk', 'EXT:ejwintern/Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_ortswerk.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_ejwintern_domain_model_ortswerk');
$TCA['tx_ejwintern_domain_model_ortswerk'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_ortswerk',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Ortswerk.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_ejwintern_domain_model_ortswerk.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_ejwintern_domain_model_personen', 'EXT:ejwintern/Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_personen.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_ejwintern_domain_model_personen');
$TCA['tx_ejwintern_domain_model_personen'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_personen',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Personen.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_ejwintern_domain_model_personen.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_ejwintern_domain_model_kategorien', 'EXT:ejwintern/Resources/Private/Language/locallang_csh_tx_ejwintern_domain_model_kategorien.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_ejwintern_domain_model_kategorien');
$TCA['tx_ejwintern_domain_model_kategorien'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ejwintern/Resources/Private/Language/locallang_db.xml:tx_ejwintern_domain_model_kategorien',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'sortby' => 'sorting',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Kategorien.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_ejwintern_domain_model_kategorien.gif'
	),
);

?>