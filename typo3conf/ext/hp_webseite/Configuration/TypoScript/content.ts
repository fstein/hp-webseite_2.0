	



	// -----------------------------------------------------------------------------
// TCEFORM
// -----------------------------------------------------------------------------

TCEFORM {
	tt_content.section_frame {
     addItems.100 = Stammbox
     addItems.110 = tooltip
     addItems.200 = Teaser
     addItems.250 = imgShadow
	}
	tt_content {

		header_layout.removeItems = 1, 2

		# CType.removeItems = bullets, div, splash, search, script

		section_frame.removeItems = 1, 2, 3, 5, 6, 10, 11, 12, 20, 21
		section_frame.addItems {
			# 100 = LLL:fileadmin/templates/xml/locallang_backend.xml:blafasel
		}
	}
}

tt_content {

	// Remove useless class in article
	# stdWrap.innerWrap.cObject.default.15 >
	
	stdWrap.innerWrap.cObject = CASE
	stdWrap.innerWrap.cObject {
  		key.field = section_frame
  		100 = TEXT
  		100.value = <div class="Stammbox">|</div>
        110 = TEXT
        110.value = <div class="tooltip">|</div>
  		200 = TEXT
  		200.value = <div class="Teaser">|</div>
  		250 = TEXT
  		250.value = <div class="imgShadow">|</div>

	}
}