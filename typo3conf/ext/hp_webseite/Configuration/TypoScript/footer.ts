////////////////////////////////////////////////////////////////////////////////
// footer navigation
////////////////////////////////////////////////////////////////////////////////
footermenu1 = COA
footermenu1 {
10 = HMENU
10.special = list
10.special.value = 2, 7
  10 {
   # Erste Ebene als Textmenü
    1 = TMENU
    1 {
        # Ebene 1 als Liste
        NO.wrapItemAndSub = <ul>|</ul>
        # Klappe alles aus
        expAll = 1
        # Normalzustand
        NO = 1
        NO {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
            # Zusatzattribut für die Links
          ATagParams = class="first-level" style="font-weight:700;"
        }

        # Aktivzustand
        CUR.wrapItemAndSub = <ul>|</ul>
        CUR = 1
        CUR {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
          ATagParams = class="first-level" style="font-weight:700;"
        }
    }
    
        # Zweite Ebene als Textmenü
    2 = TMENU
    2 {
        # Ebene 1 als Liste
        # Normalzustand
        NO = 1
        NO {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
            # Zusatzattribut für die Links
        }

        # Aktivzustand
               CUR.wrapItemAndSub = <ul>|</ul>  
      CUR = 1
        CUR {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
        }
    }
  }
  20 = HTML
  20.value = <div class="clearAll"></div>
}

footermenu2 = COA
footermenu2 {
10 = HMENU
10.special = list
10.special.value = 8
  10 {
   # Erste Ebene als Textmenü
    1 = TMENU
    1 {
        # Ebene 1 als Liste
        NO.wrapItemAndSub = <ul>|</ul>
        # Klappe alles aus
        expAll = 1
        # Normalzustand
        NO = 1
        NO {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
            # Zusatzattribut für die Links
          ATagParams = class="first-level" style="font-weight:700;"
        }

        # Aktivzustand
        CUR.wrapItemAndSub = <ul>|</ul>
        CUR = 1
        CUR {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
          ATagParams = class="first-level" style="font-weight:700;"
        }
    }
    
        # Zweite Ebene als Textmenü
    2 = TMENU
    2 {
        # Ebene 1 als Liste
        # Normalzustand
        NO = 1
        NO {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
            # Zusatzattribut für die Links
        }

        # Aktivzustand
               CUR.wrapItemAndSub = <ul>|</ul>  
      CUR = 1
        CUR {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
        }
    }
  }
  20 = HTML
  20.value = <div class="clearAll"></div>
}

footermenu3 = COA
footermenu3 {
10 = HMENU
10.special = list
10.special.value = 9, 50
  10 {
   # Erste Ebene als Textmenü
    1 = TMENU
    1 {
        # Ebene 1 als Liste
        NO.wrapItemAndSub = <ul>|</ul>
        # Klappe alles aus
        expAll = 1
        # Normalzustand
        NO = 1
        NO {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
            # Zusatzattribut für die Links
          ATagParams = class="first-level" style="font-weight:700;"
        }

        # Aktivzustand
        CUR.wrapItemAndSub = <ul>|</ul>
        CUR = 1
        CUR {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
          ATagParams = class="first-level" style="font-weight:700;"
        }
    }
    
        # Zweite Ebene als Textmenü
    2 = TMENU
    2 {
        # Ebene 1 als Liste
        # Normalzustand
        NO = 1
        NO {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
            # Zusatzattribut für die Links
        }

        # Aktivzustand
               CUR.wrapItemAndSub = <ul>|</ul>  
      CUR = 1
        CUR {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
        }
    }
  }
  20 = HTML
  20.value = <div class="clearAll"></div>
}

footermenu4 = COA
footermenu4 {
10 = HMENU
10.special = list
10.special.value = 51
  10 {
   # Erste Ebene als Textmenü
    1 = TMENU
    1 {
        # Ebene 1 als Liste
        NO.wrapItemAndSub = <ul>|</ul>
        # Klappe alles aus
        expAll = 1
        # Normalzustand
        NO = 1
        NO {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
            # Zusatzattribut für die Links
          ATagParams = class="first-level" style="font-weight:700;"
        }

        # Aktivzustand
        CUR.wrapItemAndSub = <ul>|</ul>
        CUR = 1
        CUR {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
          ATagParams = class="first-level" style="font-weight:700;"
        }
    }
    
        # Zweite Ebene als Textmenü
    2 = TMENU
    2 {
        # Ebene 1 als Liste
        # Normalzustand
        NO = 1
        NO {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
            # Zusatzattribut für die Links
        }

        # Aktivzustand
               CUR.wrapItemAndSub = <ul>|</ul>  
      CUR = 1
        CUR {
            # alle Ebene 1 Links als Listenelement
            linkWrap = <li>|</li>
        }
    }
  }
  20 = HTML
  20.value = <div class="clearAll"></div>
}‚