////////////////////////////////////////////////////////////////////////////////
// navigation
////////////////////////////////////////////////////////////////////////////////

menu = COA
menu {
  10 = HMENU
  10 {
    ### Erste Ebene ###
    1 = TMENU
    1 {
      wrap = <ul id="nav-main">|</ul>
      expAll = 1
      NO.wrapItemAndSub = <li class="firstlevel">|</li>
      NO.ATagParams = class="nav-element"

      ACT < .NO
      ACT = 1
      ACT.ATagParams = class="active currentID"
      #ACT.allWrap = ||*|<span class="nav-main-trenner"><img width="1" height="46" src="fileadmin/templates/images/nav-trenner.png" alt="Trennlinie"/></span> |*|
    }

    ### Zweite Ebene ###
    2 = TMENU
    2 {
      wrap = <div class="subnav"><div class="subnav-container"><ul>|</ul></div></div>
      NO.allWrap = <li>|</li>

      ACT < .NO
      ACT = 2
      ACT.ATagParams = class="active-2"
    }
  }
}
/* ######OLD VERSION########
menu = HMENU
menu{
  #wrap = <ul>|</ul>
  entryLevel = 0
  1 = TMENU
  1{
    NO = 1
    NO.stdWrap2.wrap = <li>|</li> 
  
    CUR = 1
    CUR < .NO
    CUR.stdWrap2.wrap = <li class="active">|</li> 
  }
}
*/


////////////////////////////////////////////////////////////////////////////////
// breadcrumb-menu
////////////////////////////////////////////////////////////////////////////////
# Breadcrumb-Funktion (Pfadanzeige, rootline)
breadcrumb = COA
breadcrumb {
  
  
  10 = HTML
  10.value = <p>Du befindest Dich hier:&nbsp;
  
  20 = HMENU
  20 {
 special = rootline
 special.range = 1
    includeNotInMenu = 1
 1 = TMENU
 1 {
 NO = 1
 NO {
 allWrap = | &nbsp;»&nbsp;
 }
 #Achtung, alle Seiten in der Rootline sind aktiv. Daher erzeugen ACT und NO den selben Effekt
   
 CUR = 1
 CUR {
 doNotLinkIt = 1
 }
 
 }
}
30 = HTML
30.value = </p>
}

////////////////////////////////////////////////////////////////////////////////
// left menu
////////////////////////////////////////////////////////////////////////////////

leftmenu = HMENU
leftmenu.special = directory
leftmenu.special.value = 3
leftmenu {
 1 = TMENU
 1 {
 NO.allWrap = ||&nbsp;<span class="dot">·</span>&nbsp;
 ACT = 1
 ACT.allWrap = ||&nbsp;<span class="dot">·</span&nbsp;
 ACT.linkWrap = <span class="dot">·</span>
 }
}


/////////////////////////////////////////////
////NIVO Slider
/////////////////////////////////////////////
slideshow=IMAGE
slideshow{
 file =  "http://de.wallpapersus.com/wp-content/uploads/2012/05/Katzen-Tiere-Katzen-Hintergrund.jpg"
 altText= Bild: Freizeit Värnamo 2010
 titleText= Bild: Freizeit Värnamo 2010
 imageLinkWrap = 1
 imageLinkWrap {
   enable = 1
   typolink.parameter = 50
 }
}


subMenu = HMENU
subMenu{
  entryLevel = 1
  
  1 = TMENU
  1{
   NO = 1
   NO.ATagParams = class="list-group-item" 
    
   CUR = 1
   CUR < .NO
   CUR.ATagParams = class="list-group-item active" 
    
  }
}

submenusubpage = HMENU
submenusubpage.entryLevel = 1
submenusubpage.1 = TMENU
submenusubpage.1 {
 wrap = <div class="second-level-nav">|</div>
 #target = page
 NO.linkWrap =
 NO.ATagBeforeWrap = 1
 ACT < .NO
 ACT = 1
 ACT.linkWrap =
 ACT.ATagParams = class="active"
}

metaMenu = HMENU
metaMenu{
 special = directory
 special.value = 22
  wrap = <ul class="metamenu">|</ul>
  
  1 = TMENU
  1{
   NO = 1
   NO.stdWrap2.wrap = <li>|</li> 
    
   CUR = 1
   CUR < .NO
   #CUR.ATagParams = class="active" 
    
  }
}
temp.copyright = TEXT
temp.copyright {
   data = date:U
   strftime = %Y
   noTrimWrap = |© GJ ||
}

mainTemplate = TEMPLATE
mainTemplate{
  
 template = FILE
 template.file = EXT:hp_webseite/Resources/Private/HTML/HPStartseite.html
 workOnSubpart = BODY
 marks.HEADER-MENU < headerMenu
 marks.MENU < menu
 marks.SLIDESHOW < styles.content.getLeft
 marks.CONTENT < styles.content.get
 marks.NEWS < styles.content.getRight
 marks.NAV-FOOTER1 < footermenu1
 marks.NAV-FOOTER2 < footermenu2
 marks.NAV-FOOTER3 < footermenu3
 marks.NAV-FOOTER4 < footermenu4
 marks.FOOTER-SECOND-ROW < footerSecondRow
 marks.HEADER-MENU < leftmenu
 
}


subpageTemplate = TEMPLATE
subpageTemplate{
 template = FILE
 template.file = EXT:hp_webseite/Resources/Private/HTML/HPUnterseite.html
 workOnSubpart = BODY
 marks.HEADER-MENU < headerMenu
 marks.MENU < menu
 marks.LOCATION < breadcrumb
 marks.SECONDLEVELNAV < submenusubpage
 marks.CONTENT < styles.content.get
 marks.NAV-FOOTER1 < footermenu1
 marks.NAV-FOOTER2 < footermenu2
 marks.NAV-FOOTER3 < footermenu3
 marks.NAV-FOOTER4 < footermenu4
 marks.FOOTER-SECOND-ROW < footerSecondRow
 marks.HEADER-MENU < leftmenu
}




page = PAGE
page.10 < subpageTemplate

page.includeCSS{
    breadcrumb = EXT:hp_webseite/Resources/Public/CSS/breadcrumb.css
    calendar = EXT:hp_webseite/Resources/Public/CSS/calendar.css
    # file22 = EXT:hp_webseite/Resources/Public/CSS/chosen.css
    contentelements = EXT:hp_webseite/Resources/Public/CSS/contentelements.css
    dd = EXT:hp_webseite/Resources/Public/CSS/dd.css
    footer = EXT:hp_webseite/Resources/Public/CSS/footer.css
    forms = EXT:hp_webseite/Resources/Public/CSS/forms.css
    # file27 = EXT:hp_webseite/Resources/Public/CSS/forbildungen.css
    # file28 = EXT:hp_webseite/Resources/Public/CSS/freizeiten.css
    gruppen = EXT:hp_webseite/Resources/Public/CSS/gruppen.css
    hauptamtliche = EXT:hp_webseite/Resources/Public/CSS/hauptamtliche.css
    ie7 = EXT:hp_webseite/Resources/Public/CSS/ie7.css
    ie8 = EXT:hp_webseite/Resources/Public/CSS/ie8.css
    navigation = EXT:hp_webseite/Resources/Public/CSS/navigation.css
    navigation_ie = EXT:hp_webseite/Resources/Public/CSS/navigation_ie.css
    nivo-slider = EXT:hp_webseite/Resources/Public/CSS/nivo-slider.css
    search = EXT:hp_webseite/Resources/Public/CSS/search.css
    startseite = EXT:hp_webseite/Resources/Public/CSS/startseite.css
    styles = EXT:hp_webseite/Resources/Public/CSS/styles.css
    tt_news = EXT:hp_webseite/Resources/Public/CSS/tt_news.css
    typography = EXT:hp_webseite/Resources/Public/CSS/typography.css
	googlefont = http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Kreon:300,400,700
}#ElementinformationenAttributePositionAndereElternelementeKindelemente
 page.headerData{
 }

page.includeJS{
    jquery = EXT:hp_webseite/Resources/Public/JS/jquery.min.js
    curveycorners = EXT:hp_webseite/Resources/Public/JS/curveycorners.js
    footer = EXT:hp_webseite/Resources/Public/JS/footer.js
    jsdd = EXT:hp_webseite/Resources/Public/JS/jquery.dd.js
    #file64 = EXT:hp_webseite/Resources/Public/JS/jquery.isotope.min.js
    #file65 = EXT:hp_webseite/Resources/Public/JS/jquery.jscroll.js
    jscroll = http://www.ejw.de/fileadmin/templates/js/jquery.jscroll.js
    masonry = EXT:hp_webseite/Resources/Public/JS/jquery.masonry.min.js
    nivoSlider = EXT:hp_webseite/Resources/Public/JS/jquery.nivo.slider.pack.js
    calendarJS = EXT:hp_webseite/Resources/Public/JS/calender.js
    nav-main = EXT:hp_webseite/Resources/Public/JS/nav-main.js
    navigation = EXT:hp_webseite/Resources/Public/JS/navigation.js
    startseite = EXT:hp_webseite/Resources/Public/JS/startseite.js
	googlemaps = http://maps.google.com/maps/api/js?sensor=false
}


[globalVar = TSFE:id = 1]
page.10 >
page.10 < mainTemplate
}
page.includeJS{
 
}
[global]

