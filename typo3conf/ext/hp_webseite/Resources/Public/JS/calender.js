$(function() {
  $('.cal-more').each(function() {
      var self = $(this),
          nextdd = self.parent().next('dd');

      if(nextdd.is(':empty')) {
          self.hide();
      } else {
          self.click(function() {
              nextdd.slideToggle('slow');
              self.toggleClass('cal-more-open');
          });
      }
  });
});
