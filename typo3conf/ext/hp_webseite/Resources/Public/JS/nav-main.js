var computeTotalWidth = function(elements) {
  var totalWidth = 0;
  elements.each(function() {totalWidth += $(this).outerWidth();});
  return totalWidth;
}

$(document).ready(function () {
  var children = $('#nav-main > li > a');
  var totalChildWidth = computeTotalWidth(children);
  var totalSpacerWidth = computeTotalWidth($('#nav-main > li > span'));
  var totalWidth = $('#nav-main').width();
  var padding = Math.floor((totalWidth - totalChildWidth - totalSpacerWidth) / (children.length * 2));
  
  // now set the padding for all elements. Problem-> due to subpixel rendering inaccuracy
  // we may get an overflow or some white pixel flashes
  children.css('padding-right', padding);
  children.css('padding-left', padding);
  
  var width = 0;
  var widthpadding = 0;

  $("#nav-main a").hover(function() { //When hovering over nav-main
    var p = $(this);
    var position = p.position();
    //var marginalignright = $(window).width()-position.left-$(this).width()-widthpadding;
    //var marginalignright = $(window).width()-position.left-$(this).outerWidth();
    var marginalignright = $(window).width()-position.left-$(this).outerWidth()+parseInt(p.css('padding-right'), 10);
    var marginalignleft = position.left+parseInt(p.css('padding-left'), 10);
    
    //Following events are applied to the subnav itself (moving subnav up and down)
    $(this).parent().find("div.subnav").fadeIn('fast').show(); //Drop down the subnav on click 
    
    // Float Left, Float Right
    if(marginalignright>500) {
      $(this).parent().find("div.subnav div.subnav-container").css('margin-left', marginalignleft );
      $(this).parent().find("div.subnav div.subnav-container").css('text-align', "left" );
    } else {
      $(this).parent().find("div.subnav div.subnav-container").css('margin-right', marginalignright );
      $(this).parent().find("div.subnav div.subnav-container").css('text-align', "right" );
    }
    /*
    if($(this).hasClass('currentID') || $(this).closest('.currentID').length > 0) {
    } else {
      alert('ich bin hier:' + $(this).closest('.currentID').html());
      $("body").find(".currentID").removeClass('active');
    }
    */
    $(this).addClass('active');
    
    // wir müssen dem aktuell aktiven noch das "active"-flag stehlen
    if ($(this).closest('li.firstlevel').find('.currentID').length == 0)
      $('.currentID').removeClass('active');
    
    $(this).closest('li.firstlevel').hover(function() { }, function() {
      $(this).parent().find("div.subnav").fadeOut('fast'); //When the mouse hovers out of the subnav, move it back up 
      $(this).find("a").removeClass('active');
      $("body").find(".currentID").addClass('active');
    });
    /*
    $(this).parent().hover(
      function() { }, 
      );
    */
  });  
  /*
  $("div.subnav-container a").hover(function() {
    $(this).parents('li').find("#current").addClass('active');
    //$("body").find("#current").addClass('active');
  });*/
  //$("body").find("#current").addClass('active');
});

$(window).load(function () {
  // fit the first and last, so that we can use a maximum of space
  var children = $('#nav-main > li > a');
  var totalChildWidth = computeTotalWidth(children.slice(1, -1));
  var totalSpacerWidth = computeTotalWidth($('#nav-main > li > span'));
  var totalWidth = $('#nav-main').width();
  
  padding = (totalWidth - totalChildWidth - totalSpacerWidth - children.first().width() - children.last().width()) / 4;
  children.first().css('padding-right', padding);
  children.first().css('padding-left', padding);
  
  children.last().css('padding-right', padding);
  children.last().css('padding-left', padding);
  
  if (children.last().position().top > 0)
    children.last().css('padding-right', padding-1);
});
