<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "pt_extlist".
 *
 * Auto generated 19-07-2014 15:26
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'List Generator',
	'description' => 'List generator for TYPO3 based on Extbase. See http://extlist.punkt.de for further details.',
	'category' => 'plugin',
	'version' => '1.3.2',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'clearcacheonload' => 0,
	'author' => 'Daniel Lienert, Michael Knoll',
	'author_email' => 't3extensions@punkt.de',
	'author_company' => 'http://www.punkt.de',
	'constraints' => 
	array (
		'depends' => 
		array (
			'typo3' => '4.5.0-6.2.99',
			'extbase' => '1.4.0',
			'fluid' => '',
			'pt_extbase' => '',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
);

