<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 punkt.de GmbH - Karlsruhe, Germany - http://www.punkt.de
 *  Author: David Vogt
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package pt_nivoslider
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_PtNivoslider_Controller_SliderController extends Tx_Extbase_MVC_Controller_ActionController {

    /**
	 * effectRepository
	 *
	 * @var Tx_PtNivoslider_Domain_Repository_EffectRepository
	 */
	protected $effectRepository;

	/**
	 * injectEffectRepository
	 *
	 * @param Tx_PtNivoslider_Domain_Repository_EffectRepository $effectRepository
	 * @return void
	 */
	public function injectEffectRepository(Tx_PtNivoslider_Domain_Repository_EffectRepository $effectRepository) {
		$this->effectRepository = $effectRepository;
	}

    /**
	 * action index
	 *
	 * @return void
	 */
    public function indexAction () {
        if($this->settings['flexform']['imgsrcs']){

            //Prepare Plugin
            $pluginUid = ($this->configurationManager->getContentObject()->data['uid']);
            $slider = new Tx_PtNivoslider_Domain_Model_Slider;

            //set general parameter and resample images
            $slider = $this->setGeneralParameters($slider);
            $this->resampleImages($slider->getImgSrcs(), $slider->getWidth(), $slider->getHeight());

            //set style-independent settings
            $slider = $this->setStyleIndependentSettings($slider);

            //set style-dependent settings
            $slider = $this->setStyleDependantSettings($slider);

            //set advanced javascript functions
            $slider = $this->setAdvancedJavaScriptFunctions($slider);

            //render text on images
            if ($this->settings['flexform']['render'] && $this->settings['flexform']['rendertext']){
                $render = $this->renderTextOnImages();
            } else {
                $render = 0;
            }

            // set external control
            if ($this->settings['flexform']['external']){
                $string = $this->setExternalControl($pluginUid);
            } else {
                $string = '';
            }

            $this->view->assign('effects', $string);
            $this->view->assign('render', $render);
            $this->view->assign('slider', $slider);
            $this->view->assign('pluginUid', $pluginUid);
        }
    }

    protected function resampleImages ($srcs, $width, $height){
        foreach ($srcs as $src){

            //Bild einlesen
            $imgSrc = 'uploads/tx_ptnivoslider/' . $src;
            $img =  new Tx_PtNivoslider_Service_Image($imgSrc);
            $image = $img->getImageData();

            //neues Bild erzeugen
            $new_image = imagecreatetruecolor($width, $height);
            imagecopyresampled ($new_image ,$image, 0, 0, 0, 0, $width, $height, $img->getImageWidth(), $img->getImageHeight());

            //Bild speichern
            $save = preg_replace("|(.*)\..*$|", "\\1.jpg", $imgSrc);
            imageJPEG($new_image, $save, 100);
        }
    }

    protected function setGeneralParameters (Tx_PtNivoslider_Domain_Model_Slider $slider) {

        $slider->setImgSrcs(explode(',', $this->settings['flexform']['imgsrcs']));

        if($this->settings['flexform']['links']) {
            $slider->setLinks(explode("\n", $this->settings['flexform']['links']));
        }

        if($this->settings['flexform']['titletags']) {
            $slider->setTitleTags(explode("\n", $this->settings['flexform']['titletags']));
        }

        if($this->settings['flexform']['alttags']) {
            $slider->setAltTags(explode("\n", $this->settings['flexform']['alttags']));
        }

        if ($this->settings['flexform']['width']){
            $slider->setWidth($this->settings['flexform']['width']);
        } else {
            $srcs = $slider->getImgSrcs();
            $imgSrc = 'uploads/tx_ptnivoslider/' . $srcs[0];
            $img = new Tx_PtNivoslider_Service_Image($imgSrc);
            $slider->setWidth($img->getImageWidth());
        }

        if ($this->settings['flexform']['height']){
            $slider->setHeight($this->settings['flexform']['height']);
        } else {
            $srcs = $slider->getImgSrcs();
            $imgSrc = 'uploads/tx_ptnivoslider/' . $srcs[0];
            $img = new Tx_PtNivoslider_Service_Image($imgSrc);
            $slider->setHeight($img->getImageHeight());
        }

        $style = $this->settings['flexform']['style'];
        if ($style == "ownstyle"){
            if ($this->settings['flexform']['stylename']){
                $style = $this->settings['flexform']['stylename'];
            } else {
                $style = 'ptnivoslider';
            }
        }
        $slider->setStyle($style);


        return $slider;
    }

    protected function setStyleIndependentSettings (Tx_PtNivoslider_Domain_Model_Slider $slider) {

        $slider->setEffect($this->settings['flexform']['effect']);

        if ($this->settings['flexform']['boxcols']){
            $slider->setBoxCols($this->settings['flexform']['boxcols']);
        } else {
            $slider->setBoxCols(8);
        }

        if ($this->settings['flexform']['boxrows']){
            $slider->setBoxRows($this->settings['flexform']['boxrows']);
        } else {
            $slider->setBoxRows(4);
        }

        if ($this->settings['flexform']['animspeed']){
            $slider->setAnimSpeed($this->settings['flexform']['animspeed']);
        } else {
            $slider->setAnimSpeed(500);
        }

        if ($this->settings['flexform']['pausetime']){
            $animSpeed = $slider->getAnimSpeed();
            $pauseTime = 2*$animSpeed+$this->settings['flexform']['pausetime'];
            $slider->setPauseTime($pauseTime);
        } else {
            $animSpeed = $slider->getAnimSpeed();
            $pauseTime = 2*$animSpeed+1000;
            $slider->setPauseTime($pauseTime);
        }

        if ($this->settings['flexform']['slices']){
            $slider->setSlices($this->settings['flexform']['slices']);
        } else {
            $slider->setSlices(15);
        }

        if ($this->settings['flexform']['startslide']){
            $slider->setStartSlide($this->settings['flexform']['startslide']-1);
        } else {
            $slider->setStartSlide(0);
        }

        if ($this->settings['flexform']['captionopacity']){
            $slider->setCaptionOpacity($this->settings['flexform']['captionopacity']/100);
        } else {
            $slider->setCaptionOpacity(0.8);
        }

        if ($this->settings['flexform']['keyboardnav']){
            $slider->setKeyboardNav('true');
        } else {
            $slider->setKeyboardNav('false');
        }

        if ($this->settings['flexform']['pauseonhover']){
            $slider->setPauseOnHover('true');
        } else {
            $slider->setPauseOnHover('false');
        }

        if ($this->settings['flexform']['manualadvance']){
            $slider->setManualAdvance('true');
        } else {
            $slider->setManualAdvance('false');
        }

        if ($this->settings['flexform']['randomstart']){
            $slider->setRandomStart('true');
        } else {
            $slider->setRandomStart('false');
        }

        return $slider;
    }

    protected function setStyleDependantSettings (Tx_PtNivoslider_Domain_Model_Slider $slider) {

        if ($slider->getStyle() == "ptplain"){
            $slider->setDirectionNav('false');
            $slider->setDirectionNavHide('false');
            $slider->setControlNav('false');
            $slider->setPrevText('');
            $slider->setNextText('');
            $slider->setThumbNav('false');
            $slider->setPlaybutton('false');
        } elseif ($slider->getStyle() == "ptmedia"){
            $slider->setDirectionNav('true');
            $slider->setDirectionNavHide('false');
            $slider->setControlNav('false');
            $slider->setPrevText('Prev');
            $slider->setNextText('Next');
            $slider->setThumbNav('false');
            $slider->setPlaybutton('true');
        } elseif ($slider->getStyle() == "ptsimple"){
            $slider->setDirectionNav('true');
            $slider->setDirectionNavHide('true');
            $slider->setControlNav('false');
            $slider->setPrevText('');
            $slider->setNextText('');
            $slider->setThumbNav('false');
            $slider->setPlaybutton('false');
        } elseif ($slider->getStyle() == "ptthumbs"){
            $slider->setDirectionNav('false');
            $slider->setDirectionNavHide('false');
            $slider->setControlNav('true');
            $slider->setPrevText('');
            $slider->setNextText('');
            $slider->setThumbNav('true');
            $thumbCreator = new Tx_PtNivoslider_Service_ThumbCreator;
            if ($this->settings['flexform']['thumbsize']){
                $size = $this->settings['flexform']['thumbsize'];
            } else {
                $size = 100;
            }
            $thumbCreator->create($slider->getImgSrcs(), $size);
            $slider->setPlaybutton('false');
        } elseif ($slider->getStyle() == "ptextended"){
            $slider->setDirectionNav('true');
            $slider->setDirectionNavHide('true');
            $slider->setControlNav('true');
            $slider->setPrevText('Prev');
            $slider->setNextText('Next');
            $slider->setThumbNav('true');
            $thumbCreator = new Tx_PtNivoslider_Service_ThumbCreator;
            if ($this->settings['flexform']['thumbsize']){
                $size = $this->settings['flexform']['thumbsize'];
            } else {
                $size = 100;
            }
            $thumbCreator->create($slider->getImgSrcs(), $size);
            $slider->setPlaybutton('false');
        } else {

            if ($this->settings['flexform']['directionnav']){
                $slider->setDirectionNav('true');
            } else {
                $slider->setDirectionNav('false');
            }

            if ($this->settings['flexform']['directionnavhide']){
                $slider->setDirectionNavHide('true');
            } else {
                $slider->setDirectionNavHide('false');
            }

            if ($this->settings['flexform']['controlnav'] || $this->settings['flexform']['thumbnav']){
                $slider->setControlNav('true');
            } else {
                $slider->setControlNav('false');
            }

             if ($this->settings['flexform']['prevtext']){
                $slider->setPrevText($this->settings['flexform']['prevtext']);
            } else {
                $slider->setPrevText('Prev');
            }

            if ($this->settings['flexform']['nexttext']){
                $slider->setNextText($this->settings['flexform']['nexttext']);
            } else {
                $slider->setNextText('Next');
            }

            if ($this->settings['flexform']['thumbnav']){
                $slider->setThumbNav('true');
                $thumbCreator = new Tx_PtNivoslider_Service_ThumbCreator;
                if ($this->settings['flexform']['thumbsize']){
                    $size = $this->settings['flexform']['thumbsize'];
                } else {
                    $size = 100;
                }
                $thumbCreator->create($slider->getImgSrcs(), $size);
            } else {
                $slider->setThumbNav('false');
            }

            if ($this->settings['flexform']['playbutton']){
                $slider->setPlaybutton('true');
            } else {
                $slider->setPlaybutton('false');
            }
        }

        return $slider;
    }

    protected function setAdvancedJavaScriptFunctions (Tx_PtNivoslider_Domain_Model_Slider $slider){

        if ($this->settings['flexform']['beforechange']){
            $slider->setBeforeChange($this->settings['flexform']['beforechange']);
        } else {
            $slider->setBeforeChange('function(){}');
        }

        if ($this->settings['flexform']['afterchange']){
            $slider->setAfterChange($this->settings['flexform']['afterchange']);
        } else {
            $slider->setAfterChange('function(){}');
        }

        if ($this->settings['flexform']['slideshowend']){
            $slider->setSlideshowEnd($this->settings['flexform']['slideshowend']);
        } else {
            $slider->setSlideshowEnd('function(){}');
        }

        if ($this->settings['flexform']['lastslide']){
            $slider->setLastSlide($this->settings['flexform']['lastslide']);
        } else {
            $slider->setLastSlide('function(){}');
        }

        if ($this->settings['flexform']['afterload']){
            $slider->setAfterLoad($this->settings['flexform']['afterload']);
        } else {
            $slider->setAfterLoad('function(){}');
        }

        return $slider;
    }

    protected function setExternalControl ($pluginUid) {

        $effects = $this->effectRepository->findAll();
        $string = '$(window).load(function() {$(document).ready(function() {';

        foreach($effects as $effect){
            $substring = "$('#" . $effect->getId() . "').on('" . $effect->getMethod() . "', function() {";
            $functions = explode(',', $effect->getFunctions());
            $clickDuringAnimation = $effect->getClickDuringAnimation();
            if ($clickDuringAnimation){
                $clickDuringAnimationForJs = 'true';
            } else {
                $clickDuringAnimationForJs = 'false';
            }
            $stopAnimation = $effect->getStopAnimation();
            if ($stopAnimation) {
                $stopAnimationForJs = 'true';
            } else {
                $stopAnimationForJs = 'false';
            }
            foreach ($functions as $function){
                if ($function == 'jumpToImage' || $function == 'animateImage') {
                    $imageNumber = $effect->getImage();
                    $substring = $substring . "$('#slider-" . $pluginUid . "').data('nivoslider')." . $function . "(" . $clickDuringAnimation . $stopAnimation . $imageNumber . "); \n";
                } elseif ($function == 'setSpeed') {
                    $newAnimSpeed = $effect->getAbsoluteSpeed();
                    $newIdleTime = $effect->getAbsoluteIdleTime();
                    $substring = $substring . "$('#slider-" . $pluginUid . "').data('nivoslider')." . $function . "(" . $newAnimSpeed . "," . $newIdleTime . "); \n";
                } elseif ($function == 'changeSpeed') {
                    $relAnimSpeed = $effect->getRelativeSpeed();
                    $relIdleTime = $effect->getRelativeIdleTime();
                    $substring = $substring . "$('#slider-" . $pluginUid . "').data('nivoslider')." . $function . "(" . $relAnimSpeed . "," . $relIdleTime .  "); \n";
                } elseif ($function == "start" || $function == 'stop' || $function == 'changeDirection') {
                    $substring = $substring . "$('#slider-" . $pluginUid . "').data('nivoslider')." . $function . "(); \n";
                } else {
                    $substring = $substring . "$('#slider-" . $pluginUid . "').data('nivoslider')." . $function . "(" . $clickDuringAnimationForJs . "," . $stopAnimationForJs . "); \n";
                }

            }
            $substring = $substring . '});';
            $string = $string . $substring;
        }

        $string = $string . '});});';

        return $string;
    }

    protected function renderTextOnImages(){
        $render = 1;
        $renderText = new Tx_PtNivoslider_Service_RenderText();
        $renderObject = new Tx_PtNivoslider_Service_RenderObject();

        if ($this->settings['flexform']['position-x']){
            $renderObject->setX($this->settings['flexform']['position-x']);
        } else {
            $renderObject->setX(0);
        }

        if ($this->settings['flexform']['position-y']){
            $renderObject->setY($this->settings['flexform']['position-y']);
        } else {
            $renderObject->setY(0);
        }

        if ($this->settings['flexform']['font']){
            $renderObject->setFont($this->settings['flexform']['font']);
        } else {
            echo 'Die von Ihnen gewählte Schriftart wurde nicht gefunden';
            exit;
        }

        $renderObject->setTexts(explode(";;;\n", $this->settings['flexform']['rendertext']));

        if ($this->settings['flexform']['fontsize']){
            $renderObject->setFontsize($this->settings['flexform']['fontsize']);
        } else {
            $renderObject->setFontsize(20);
        }

        if ($this->settings['flexform']['hexcolor']){
            $renderObject->setRed(hexdec(substr(trim($this->settings['flexform']['hexcolor'], '#'),0,2)));
            $renderObject->setGreen(hexdec(substr(trim($this->settings['flexform']['hexcolor'], '#'),2,2)));
            $renderObject->setBlue(hexdec(substr(trim($this->settings['flexform']['hexcolor'], '#'),4,2)));
        } else {
            $renderObject->setBlue(0);
            $renderObject->setRed(0);
            $renderObject->setGreen(0);
        }

        if ($this->settings['flexform']['renderbox']){
            $renderObject->setRenderBox(1);
            if ($this->settings['flexform']['padding']) {
                $renderObject->setPadding($this->settings['flexform']['padding']);
            } else {
                $renderObject->setPadding(10);
            }
            if ($this->settings['flexform']['boxhexcolor']){
                $renderObject->setRedBox(hexdec(substr(trim($this->settings['flexform']['boxhexcolor'], '#'),0,2)));
                $renderObject->setGreenBox(hexdec(substr(trim($this->settings['flexform']['boxhexcolor'], '#'),2,2)));
                $renderObject->setBlueBox(hexdec(substr(trim($this->settings['flexform']['boxhexcolor'], '#'),4,2)));
            } else {
                $renderObject->setBlueBox(255);
                $renderObject->setRedBox(255);
                $renderObject->setGreenBox(255);
            }
        } else {
            $renderObject->setRenderBox(0);
        }

        $renderObject->setImgSrcs(explode(',', $this->settings['flexform']['imgsrcs']));

        $renderText->render($renderObject);

        return $render;
    }
}

?>
