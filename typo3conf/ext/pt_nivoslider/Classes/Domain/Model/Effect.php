<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 punkt.de GmbH - Karlsruhe, Germany - http://www.punkt.de
 *  Author: David Vogt
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package pt_nivoslider
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_PtNivoslider_Domain_Model_Effect extends Tx_Extbase_DomainObject_AbstractValueObject {

    /**
	 * id
	 *
	 * @var string
     *
	 */
	protected $id;

    /**
	 * title
	 *
	 * @var string
     *
	 */
	protected $title;

    /**
	 * method
	 *
	 * @var string
     *
	 */
	protected $method;

    /**
	 * image
	 *
	 * @var integer
     *
	 */
	protected $image;

    /**
	 * functions
	 *
	 * @var string
     *
	 */
	protected $functions;

    /**
     * clickDuringAnimations
     *
     * @var boolean
     */
    protected $clickDuringAnimation;

    /**
     * stopAnimation
     *
     * @var boolean
     */
    protected $stopAnimation;

    /**
	 * absoluteSpeed
	 *
	 * @var integer
     *
	 */
	protected $absoluteSpeed;

    /**
	 * relativeSpeed
	 *
	 * @var integer
     *
	 */
	protected $relativeSpeed;

    /**
     * absoluteIdleTime
     *
     * @var integer
     */
    protected $absoluteIdleTime;

    /**
     * relativeIdleTime
     *
     * @var integer
     */
    protected $relativeIdleTime;

    /**
	 * Returns the id
	 *
	 * @return string $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Sets the id
	 *
	 * @param string $id
	 * @return void
	 */
	public function setId($id) {
		$this->id = $id;
	}

    /**
	 * Returns the method
	 *
	 * @return string $method
	 */
	public function getMethod() {
		return $this->method;
	}

	/**
	 * Sets the method
	 *
	 * @param string $method
	 * @return void
	 */
	public function setMethod($method) {
		$this->method = $method;
	}

    /**
	 * Returns the functions
	 *
	 * @return string $functions
	 */
	public function getFunctions() {
		return $this->functions;
	}

	/**
	 * Sets the functions
	 *
	 * @param string $functions
	 * @return void
	 */
	public function setFunctions($functions) {
		$this->functions = $functions;
	}

    /**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

    /**
   	 * Sets the image number
   	 *
   	 * @param integer $image
   	 * @return void
   	 */
   	public function setImage($image) {
   		$this->image = $image;
   	}

    /**
	 * Returns the image number
	 *
	 * @return integer $image
	 */
	public function getImage() {
		return $this->image;
	}

    /**
     * sets whether you can click during a running animation or not
     *
     * @param boolean $clickDuringAnimation
     * @return void
     */
    public function setClickDuringAnimation($clickDuringAnimation) {
        $this->clickDuringAnimation = $clickDuringAnimation;
    }

    /**
     * returns whether you can click during a running animation or not
     *
     * @return boolean
     */
    public function getClickDuringAnimation() {
        return $this->clickDuringAnimation;
    }

    /**
     * Sets whether animations stops or finishes if a click occurs during its run time
     *
     * @param boolean $stopAnimation
     * @return void
     */
    public function setStopAnimation($stopAnimation) {
        $this->stopAnimation = $stopAnimation;
    }

    /**
     * Returns whether animations stops or finishes if a click occurs during its run time
     *
     * @return boolean
     */
    public function getStopAnimation() {
        return $this->stopAnimation;
    }

	/**
	 * Sets the absolute length of the animation in ms
	 *
	 * @param integer $absoluteSpeed
	 * @return void
	 */
	public function setAbsoluteSpeed($absoluteSpeed) {
		$this->absoluteSpeed = $absoluteSpeed;
	}

    /**
	 * Returns the absolute length of the animation in ms
	 *
	 * @return integer $absoluteSpeed
	 */
	public function getAbsoluteSpeed() {
		return $this->absoluteSpeed;
	}

	/**
	 * Sets the additional animation speed
	 *
	 * @param integer $relativeSpeed
	 * @return void
	 */
	public function setRelativeSpeed($relativeSpeed) {
		$this->relativeSpeed = $relativeSpeed;
	}

    /**
	 * Returns the additional animation speed
	 *
	 * @return integer $relativeSpeed
	 */
	public function getRelativeSpeed() {
		return $this->relativeSpeed;
	}

    /**
   	 * Sets the idle time between animations
   	 *
   	 * @param integer $absoluteIdleTime
   	 * @return void
   	 */
   	public function setAbsoluteIdleTime($absoluteIdleTime) {
   		$this->absoluteIdleTime = $absoluteIdleTime;
   	}

    /**
   	 * Returns the idle time between animations
   	 *
   	 * @return integer $absoluteIdleTime
   	 */
   	public function getAbsoluteIdleTime() {
   		return $this->absoluteIdleTime;
   	}

    /**
   	 * Sets the additional idle time between animations
   	 *
   	 * @param integer $relativeIdleTime
   	 * @return void
   	 */
   	public function setRelativeIdleTime($relativeIdleTime) {
   		$this->relativeIdleTime = $relativeIdleTime;
   	}

    /**
   	 * Returns the additional idle time between animations
   	 *
   	 * @return integer $relativeIdleTime
   	 */
   	public function getRelativeIdleTime() {
   		return $this->relativeIdleTime;
   	}

}

?>