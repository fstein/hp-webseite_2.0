<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 punkt.de GmbH - Karlsruhe, Germany - http://www.punkt.de
 *  Author: David Vogt
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package pt_nivoslider
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_PtNivoslider_Domain_Model_Slider extends Tx_Extbase_DomainObject_AbstractValueObject {

    /**
	 * imgSrcs
	 *
	 * @var array
     *
	 */
	protected $imgSrcs;

    /**
	 * links
	 *
	 * @var array
     *
	 */
	protected $links;

    /**
	 * titleTags
	 *
	 * @var array
     *
	 */
	protected $titleTags;

    /**
	 * altTags
	 *
	 * @var array
     *
	 */
	protected $altTags;

    /**
	 * effect
	 *
	 * @var string
     *
	 */
	protected $effect;

    /**
	 * controlNavThumbsSearch
	 *
	 * @var string
     *
	 */
	protected $controlNavThumbsSearch;

    /**
	 * controlNavThumbsReplace
	 *
	 * @var string
     *
	 */
	protected $controlNavThumbsReplace;

    /**
	 * prevText
	 *
	 * @var string
     *
	 */
	protected $prevText;

    /**
	 * nextText
	 *
	 * @var string
     *
	 */
	protected $nextText;

    /**
	 * beforeChange
	 *
	 * @var string
     *
	 */
	protected $beforeChange;

    /**
	 * afterChange
	 *
	 * @var string
     *
	 */
	protected $afterChange;

    /**
	 * slideshowEnd
	 *
	 * @var string
     *
	 */
	protected $slideshowEnd;

    /**
	 * lastSlide
	 *
	 * @var string
     *
	 */
	protected $lastSlide;

    /**
	 * afterLoad
	 *
	 * @var string
     *
	 */
	protected $afterLoad;

    /**
	 * height
	 *
	 * @var int
     *
	 */
	protected $height;

    /**
	 * width
	 *
	 * @var int
     *
	 */
	protected $width;

    /**
	 * slices
	 *
	 * @var int
     *
	 */
	protected $slices;

    /**
	 * boxCols
	 *
	 * @var int
     *
	 */
	protected $boxCols;

    /**
	 * boxRows
	 *
	 * @var int
     *
	 */
	protected $boxRows;

    /**
	 * animSpeed
	 *
	 * @var int
     *
	 */
	protected $animSpeed;

    /**
	 * pauseTime
	 *
	 * @var int
     *
	 */
	protected $pauseTime;

    /**
	 * startSlide
	 *
	 * @var int
     *
	 */
	protected $startSlide;


    /**
	 * captionOpacity
	 *
	 * @var float
     *
	 */
	protected $captionOpacity;

    /**
	 * randomStart
	 *
	 * @var boolean
     *
	 */
	protected $randomStart;

    /**
	 * directionNav
	 *
	 * @var boolean
     *
	 */
	protected $directionNav;

    /**
	 * directionNavHide
	 *
	 * @var boolean
     *
	 */
	protected $directionNavHide;

    /**
	 * controlNav
	 *
	 * @var boolean
     *
	 */
	protected $controlNav;

    /**
	 * controlNavThumbs
	 *
	 * @var boolean
     *
	 */
	protected $controlNavThumbs;

    /**
	 * controlNavThumbsFromRel
	 *
	 * @var boolean
     *
	 */
	protected $controlNavThumbsFromRel;

    /**
	 * pauseOnHover
	 *
	 * @var boolean
     *
	 */
	protected $pauseOnHover;

    /**
	 * manualAdvance
	 *
	 * @var boolean
     *
	 */
	protected $manualAdvance;

    /**
	 * keyboardNav
	 *
	 * @var boolean
     *
	 */
	protected $keyboardNav;

    /**
	 * thumbNav
	 *
	 * @var boolean
     *
	 */
	protected $thumbNav;

    /**
	 * style
	 *
	 * @var string
     *
	 */
	protected $style;

    /**
	 * style
	 *
	 * @var string
     *
	 */
	protected $playbutton;


    /**
	 * Returns the playbutton
	 *
	 * @return string $playbutton
	 */
	public function getPlaybutton() {
		return $this->playbutton;
	}

	/**
	 * Sets the playbutton
	 *
	 * @param string $playbutton
	 * @return void
	 */
	public function setPlaybutton($playbutton) {
		$this->playbutton = $playbutton;
	}

    /**
	 * Returns the style
	 *
	 * @return string $style
	 */
	public function getStyle() {
		return $this->style;
	}

	/**
	 * Sets the style
	 *
	 * @param string $style
	 * @return void
	 */
	public function setStyle($style) {
		$this->style = $style;
	}



    /**
	 * Returns the thumbNav
	 *
	 * @return boolean $thumbNav
	 */
	public function getThumbNav() {
		return $this->thumbNav;
	}

	/**
	 * Sets the thumbNav
	 *
	 * @param boolean $thumbNav
	 * @return void
	 */
	public function setThumbNav($thumbNav) {
		$this->thumbNav = $thumbNav;
	}


    /**
	 * Returns the keyboardNav
	 *
	 * @return boolean $keyboardNav
	 */
	public function getKeyboardNav() {
		return $this->keyboardNav;
	}

	/**
	 * Sets the keyboardNav
	 *
	 * @param boolean $keyboardNav
	 * @return void
	 */
	public function setKeyboardNav($keyboardNav) {
		$this->keyboardNav = $keyboardNav;
	}


    /**
	 * Returns the randomStart
	 *
	 * @return boolean $randomStart
	 */
	public function getRandomStart() {
		return $this->randomStart;
	}

	/**
	 * Sets the randomStart
	 *
	 * @param boolean $randomStart
	 * @return void
	 */
	public function setRandomStart($randomStart) {
		$this->randomStart = $randomStart;
	}

    /**
	 * Returns the directionNav
	 *
	 * @return boolean $directionNav
	 */
	public function getDirectionNav() {
		return $this->directionNav;
	}

	/**
	 * Sets the directionNav
	 *
	 * @param boolean $directionNav
	 * @return void
	 */
	public function setDirectionNav($directionNav) {
		$this->directionNav = $directionNav;
	}

    /**
	 * Returns the directionNavHide
	 *
	 * @return boolean $directionNavHide
	 */
	public function getDirectionNavHide() {
		return $this->directionNavHide;
	}

	/**
	 * Sets the directionNavHide
	 *
	 * @param boolean $directionNavHide
	 * @return void
	 */
	public function setDirectionNavHide($directionNavHide) {
		$this->directionNavHide = $directionNavHide;
	}

    /**
	 * Returns the controlNav
	 *
	 * @return boolean $controlNav
	 */
	public function getControlNav() {
		return $this->controlNav;
	}

	/**
	 * Sets the controlNav
	 *
	 * @param boolean $controlNav
	 * @return void
	 */
	public function setControlNav($controlNav) {
		$this->controlNav = $controlNav;
	}

    /**
	 * Returns the controlNavThumbs
	 *
	 * @return boolean $controlNavThumbs
	 */
	public function getControlNavThumbs() {
		return $this->controlNavThumbs;
	}

	/**
	 * Sets the controlNavThumbs
	 *
	 * @param boolean $controlNavThumbs
	 * @return void
	 */
	public function setControlNavThumbs($controlNavThumbs) {
		$this->controlNavThumbs = $controlNavThumbs;
	}

    /**
	 * Returns the controlNavThumbsFromRel
	 *
	 * @return boolean $controlNavThumbsFromRel
	 */
	public function getControlNavThumbsFromRel() {
		return $this->controlNavThumbsFromRel;
	}

	/**
	 * Sets the controlNavThumbsFromRel
	 *
	 * @param boolean $controlNavThumbsFromRel
	 * @return void
	 */
	public function setControlNavThumbsFromRel($controlNavThumbsFromRel) {
		$this->controlNavThumbsFromRel = $controlNavThumbsFromRel;
	}

    /**
	 * Returns the pauseOnHover
	 *
	 * @return boolean $pauseOnHover
	 */
	public function getPauseOnHover() {
		return $this->pauseOnHover;
	}

	/**
	 * Sets the pauseOnHover
	 *
	 * @param boolean $pauseOnHover
	 * @return void
	 */
	public function setPauseOnHover($pauseOnHover) {
		$this->pauseOnHover = $pauseOnHover;
	}

    /**
	 * Returns the manualAdvance
	 *
	 * @return boolean $manualAdvance
	 */
	public function getManualAdvance() {
		return $this->manualAdvance;
	}

	/**
	 * Sets the manualAdvance
	 *
	 * @param boolean $manualAdvance
	 * @return void
	 */
	public function setManualAdvance($manualAdvance) {
		$this->manualAdvance = $manualAdvance;
	}

    /**
	 * Returns the captionOpacity
	 *
	 * @return float $captionOpacity
	 */
	public function getCaptionOpacity() {
		return $this->captionOpacity;
	}

	/**
	 * Sets the captionOpacity
	 *
	 * @param float $captionOpacity
	 * @return void
	 */
	public function setCaptionOpacity($captionOpacity) {
		$this->captionOpacity = $captionOpacity;
	}

    /**
	 * Returns the slices
	 *
	 * @return int $slices
	 */
	public function getSlices() {
		return $this->slices;
	}

	/**
	 * Sets the slices
	 *
	 * @param int $slices
	 * @return void
	 */
	public function setSlices($slices) {
		$this->slices = $slices;
	}

    /**
	 * Returns the boxCols
	 *
	 * @return int $boxCols
	 */
	public function getBoxCols() {
		return $this->boxCols;
	}

	/**
	 * Sets the boxCols
	 *
	 * @param int $boxCols
	 * @return void
	 */
	public function setBoxCols($boxCols) {
		$this->boxCols = $boxCols;
	}

    /**
	 * Returns the boxRows
	 *
	 * @return int $boxRows
	 */
	public function getBoxRows() {
		return $this->boxRows;
	}

	/**
	 * Sets the boxRows
	 *
	 * @param int $boxRows
	 * @return void
	 */
	public function setBoxRows($boxRows) {
		$this->boxRows = $boxRows;
	}

    /**
	 * Returns the animSpeed
	 *
	 * @return int $animSpeed
	 */
	public function getAnimSpeed() {
		return $this->animSpeed;
	}

	/**
	 * Sets the animSpeed
	 *
	 * @param int $animSpeed
	 * @return void
	 */
	public function setAnimSpeed($animSpeed) {
		$this->animSpeed = $animSpeed;
	}

    /**
	 * Returns the pauseTime
	 *
	 * @return int $pauseTime
	 */
	public function getPauseTime() {
		return $this->pauseTime;
	}

	/**
	 * Sets the pauseTime
	 *
	 * @param int $pauseTime
	 * @return void
	 */
	public function setPauseTime($pauseTime) {
		$this->pauseTime = $pauseTime;
	}

    /**
	 * Returns the startSlide
	 *
	 * @return int $startSlide
	 */
	public function getStartSlide() {
		return $this->startSlide;
	}

	/**
	 * Sets the startSlide
	 *
	 * @param int $startSlide
	 * @return void
	 */
	public function setStartSlide($startSlide) {
		$this->startSlide = $startSlide;
	}

     /**
	 * Returns the effect
	 *
	 * @return string $effect
	 */
	public function getEffect() {
		return $this->effect;
	}

	/**
	 * Sets the effect
	 *
	 * @param string $effect
	 * @return void
	 */
	public function setEffect($effect) {
		$this->effect = $effect;
	}

    /**
	 * Returns the controlNavThumbsSearch
	 *
	 * @return string $controlNavThumbsSearch
	 */
	public function getControlNavThumbsSearch() {
		return $this->controlNavThumbsSearch;
	}

	/**
	 * Sets the controlNavThumbsSearch
	 *
	 * @param string $controlNavThumbsSearch
	 * @return void
	 */
	public function setControlNavThumbsSearch($controlNavThumbsSearch) {
		$this->controlNavThumbsSearch = $controlNavThumbsSearch;
	}

    /**
	 * Returns the controlNavThumbsReplace
	 *
	 * @return string $controlNavThumbsReplace
	 */
	public function getControlNavThumbsReplace() {
		return $this->controlNavThumbsReplace;
	}

	/**
	 * Sets the controlNavThumbsReplace
	 *
	 * @param string $controlNavThumbsReplace
	 * @return void
	 */
	public function setControlNavThumbsReplace($controlNavThumbsReplace) {
		$this->controlNavThumbsReplace = $controlNavThumbsReplace;
	}

    /**
	 * Returns the prevText
	 *
	 * @return string $prevText
	 */
	public function getPrevText() {
		return $this->prevText;
	}

	/**
	 * Sets the prevText
	 *
	 * @param string $prevText
	 * @return void
	 */
	public function setPrevText($prevText) {
		$this->prevText = $prevText;
	}

    /**
	 * Returns the nextText
	 *
	 * @return string $nextText
	 */
	public function getNextText() {
		return $this->nextText;
	}

	/**
	 * Sets the nextText
	 *
	 * @param string $nextText
	 * @return void
	 */
	public function setNextText($nextText) {
		$this->nextText = $nextText;
	}

    /**
	 * Returns the beforeChange
	 *
	 * @return string $beforeChange
	 */
	public function getBeforeChange() {
		return $this->beforeChange;
	}

	/**
	 * Sets the beforeChange
	 *
	 * @param string $beforeChange
	 * @return void
	 */
	public function setBeforeChange($beforeChange) {
		$this->beforeChange = $beforeChange;
	}

    /**
	 * Returns the afterChange
	 *
	 * @return string $afterChange
	 */
	public function getAfterChange() {
		return $this->afterChange;
	}

	/**
	 * Sets the afterChange
	 *
	 * @param string $afterChange
	 * @return void
	 */
	public function setAfterChange($afterChange) {
		$this->afterChange = $afterChange;
	}

    /**
	 * Returns the slideshowEnd
	 *
	 * @return string $slideshowEnd
	 */
	public function getSlideshowEnd() {
		return $this->slideshowEnd;
	}

	/**
	 * Sets the slideshowEnd
	 *
	 * @param string $slideshowEnd
	 * @return void
	 */
	public function setSlideshowEnd($slideshowEnd) {
		$this->slideshowEnd = $slideshowEnd;
	}

    /**
	 * Returns the lastSlide
	 *
	 * @return string $lastSlide
	 */
	public function getLastSlide() {
		return $this->lastSlide;
	}

	/**
	 * Sets the lastSlide
	 *
	 * @param string $lastSlide
	 * @return void
	 */
	public function setLastSlide($lastSlide) {
		$this->lastSlide = $lastSlide;
	}

    /**
	 * Returns the afterLoad
	 *
	 * @return string $afterLoad
	 */
	public function getAfterLoad() {
		return $this->afterLoad;
	}

	/**
	 * Sets the afterLoad
	 *
	 * @param string $afterLoad
	 * @return void
	 */
	public function setAfterLoad($afterLoad) {
		$this->afterLoad = $afterLoad;
	}

    /**
	 * Returns the imgSrcs
	 *
	 * @return array $imgSrcs
	 */
	public function getImgSrcs() {
		return $this->imgSrcs;
	}

	/**
	 * Sets the imgSrcs
	 *
	 * @param array $imgSrcs
	 * @return void
	 */
	public function setImgSrcs($imgSrcs) {
		$this->imgSrcs = $imgSrcs;
	}

     /**
	 * Returns the links
	 *
	 * @return array $links
	 */
	public function getLinks() {
		return $this->links;
	}

	/**
	 * Sets the links
	 *
	 * @param array $links
	 * @return void
	 */
	public function setLinks($links) {
		$this->links = $links;
	}

     /**
	 * Returns the titleTags
	 *
	 * @return array $titleTags
	 */
	public function getTitleTags() {
		return $this->titleTags;
	}

	/**
	 * Sets the titleTags
	 *
	 * @param array $titleTags
	 * @return void
	 */
	public function setTitleTags($titleTags) {
		$this->titleTags = $titleTags;
	}

    /**
	 * Returns the altTags
	 *
	 * @return array $altTags
	 */
	public function getAltTags() {
		return $this->altTags;
	}

	/**
	 * Sets the altTags
	 *
	 * @param array $altTags
	 * @return void
	 */
	public function setAltTags($altTags) {
		$this->altTags = $altTags;
	}

    /**
     * Sets height
     *
     * @param int $height
     */
    public function setHeight($height){
        $this->height = $height;
    }

    /**
     * Returns height
     *
     * @return int
     */
    public function getHeight() {
        return $this->height;
    }

    /**
     * Sets width
     *
     * @param int $width
     */
    public function setWidth($width) {
        $this->width = $width;
    }

    /**
     * Returns width
     *
     * @return int
     */
    public function getWidth() {
        return $this->width;
    }

}

?>