<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 punkt.de GmbH - Karlsruhe, Germany - http://www.punkt.de
 *  Author: David Vogt
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package pt_nivoslider
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_PtNivoslider_Service_Image {

    protected $image;
    protected $imageok;
    protected $thumb;

    public function __construct($imgSrc)
    {
        //detect image format
        $this->image["format"] = preg_replace("/.*\.(.*)$/", "\\1", $imgSrc);
        $this->image["format"] = strtoupper($this->image["format"]);

        // convert image into usable format.
        if ( $this->image["format"] == "JPG" || $this->image["format"] == "JPEG" ) {
            //JPEG
            $this->image["format"] = "JPEG";
            $this->image["data"] = imagecreatefromjpeg($imgSrc);
        } elseif( $this->image["format"] == "PNG" ){
            //PNG
            $this->image["format"] = "PNG";
            $this->image["data"] = imagecreatefrompng($imgSrc);
        } elseif( $this->image["format"] == "GIF" ){
            //GIF
            $this->image["format"] = "GIF";
            $this->image["data"] = imagecreatefromgif($imgSrc);
        } elseif ( $this->image["format"] == "WBMP" ){
            //WBMP
            $this->image["format"] = "WBMP";
            $this->image["data"] = imagecreatefromwbmp($imgSrc);
        } else {
            //DEFAULT
            return false;
        }

        // Image is ok
        $this->imageok = true;

        // Work out image size
        $this->image["width"] = imagesx($this->image["data"]);
        $this->image["height"] = imagesy($this->image["data"]);
    }

    public function setImageWidth ($value){
        $this->image["width"]=$value;
    }

    public function getImageWidth (){
        return $this->image["width"];
    }

    public function setImageHeight ($value){
        $this->image["height"]=$value;
    }

    public function getImageHeight (){
        return $this->image["height"];
    }

    public function setImageData ($value){
        $this->image["data"]=$value;
    }

    public function getImageData (){
        return $this->image["data"];
    }

    public function setThumbWidth ($value){
        $this->thumb["width"]=$value;
    }

    public function getThumbWidth (){
        return $this->thumb["width"];
    }

    public function setThumbHeight ($value){
        $this->thumb["height"]=$value;
    }

    public function getThumbHeight (){
        return $this->thumb["height"];
    }

    public function setThumbData ($value){
        $this->thumb["data"]=$value;
    }

    public function getThumbData (){
        return $this->thumb["data"];
    }

    public function getImageok(){
        return $this->imageok;
    }


}

?>