<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 punkt.de GmbH - Karlsruhe, Germany - http://www.punkt.de
 *  Author: David Vogt
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package pt_nivoslider
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_PtNivoslider_Service_RenderObject {

    /**
	 * texts
	 *
	 * @var array
     *
	 */
	protected $texts;

    /**
	 * imgSrcs
	 *
	 * @var array
     *
	 */
	protected $imgSrcs;

    /**
	 * font
	 *
	 * @var string
     *
	 */
	protected $font;

    /**
	 * fontsize
	 *
	 * @var int
     *
	 */
	protected $fontsize;

    /**
	 * x
	 *
	 * @var int
     *
	 */
	protected $x;

    /**
	 * y
	 *
	 * @var int
     *
	 */
	protected $y;

    /**
	 * useRgbColor
	 *
	 * @var boolean
     *
	 */
	protected $renderBox;

    /**
	 * red
	 *
	 * @var int
     *
	 */
	protected $red;

    /**
	 * green
	 *
	 * @var int
     *
	 */
	protected $green;

    /**
	 * blue
	 *
	 * @var int
     *
	 */
	protected $blue;

    /**
	 * redBox
	 *
	 * @var int
     *
	 */
	protected $redBox;

    /**
	 * greenBox
	 *
	 * @var int
     *
	 */
	protected $greenBox;

    /**
	 * blueBox
	 *
	 * @var int
     *
	 */
	protected $blueBox;

    /**
	 * padding
	 *
	 * @var int
     *
	 */
	protected $padding;

    /**
	 * Returns the padding
	 *
	 * @return int $padding
	 */
	public function getPadding() {
		return $this->padding;
	}

	/**
	 * Sets the padding
	 *
	 * @param int $padding
	 * @return void
	 */
	public function setPadding($padding) {
		$this->padding = $padding;
	}

    /**
	 * Returns the redBox
	 *
	 * @return int $redBox
	 */
	public function getRedBox() {
		return $this->redBox;
	}

	/**
	 * Sets the redBox
	 *
	 * @param int $redBox
	 * @return void
	 */
	public function setRedBox($redBox) {
		$this->redBox = $redBox;
	}

    /**
	 * Returns the greenBox
	 *
	 * @return int $greenBox
	 */
	public function getGreenBox() {
		return $this->greenBox;
	}

	/**
	 * Sets the greenBox
	 *
	 * @param int $greenBox
	 * @return void
	 */
	public function setGreenBox($greenBox) {
		$this->greenBox = $greenBox;
	}

    /**
	 * Returns the blueBox
	 *
	 * @return int $blueBox
	 */
	public function getBlueBox() {
		return $this->blueBox;
	}

	/**
	 * Sets the blueBox
	 *
	 * @param int $blueBox
	 * @return void
	 */
	public function setBlueBox($blueBox) {
		$this->blueBox = $blueBox;
	}

    /**
	 * Returns the red
	 *
	 * @return int $red
	 */
	public function getRed() {
		return $this->red;
	}

	/**
	 * Sets the red
	 *
	 * @param int $red
	 * @return void
	 */
	public function setRed($red) {
		$this->red = $red;
	}

    /**
	 * Returns the green
	 *
	 * @return int $green
	 */
	public function getGreen() {
		return $this->green;
	}

	/**
	 * Sets the green
	 *
	 * @param int $green
	 * @return void
	 */
	public function setGreen($green) {
		$this->green = $green;
	}

    /**
	 * Returns the blue
	 *
	 * @return int $blue
	 */
	public function getBlue() {
		return $this->blue;
	}

	/**
	 * Sets the blue
	 *
	 * @param int $blue
	 * @return void
	 */
	public function setBlue($blue) {
		$this->blue = $blue;
	}

    /**
	 * Returns the renderBox
	 *
	 * @return boolean $renderBox
	 */
	public function getRenderBox() {
		return $this->renderBox;
	}

	/**
	 * Sets the renderBox
	 *
	 * @param boolean $renderBox
	 * @return void
	 */
	public function setRenderBox($renderBox) {
		$this->renderBox = $renderBox;
	}

    /**
	 * Returns the font
	 *
	 * @return string $font
	 */
	public function getFont() {
		return $this->font;
	}

	/**
	 * Sets the font
	 *
	 * @param string $font
	 * @return void
	 */
	public function setFont($font) {
		$this->font = $font;
	}

    /**
	 * Returns the fontsize
	 *
	 * @return int $fontsize
	 */
	public function getFontsize() {
		return $this->fontsize;
	}

	/**
	 * Sets the fontsize
	 *
	 * @param int $fontsize
	 * @return void
	 */
	public function setFontsize($fontsize) {
		$this->fontsize = $fontsize;
	}

    /**
	 * Returns the imgSrcs
	 *
	 * @return array $imgSrcs
	 */
	public function getImgSrcs() {
		return $this->imgSrcs;
	}

	/**
	 * Sets the imgSrcs
	 *
	 * @param array $imgSrcs
	 * @return void
	 */
	public function setImgSrcs($imgSrcs) {
		$this->imgSrcs = $imgSrcs;
	}

    /**
	 * Returns the texts
	 *
	 * @return array $texts
	 */
	public function getTexts() {
		return $this->texts;
	}

	/**
	 * Sets the texts
	 *
	 * @param array $texts
	 * @return void
	 */
	public function setTexts($texts) {
		$this->texts = $texts;
	}

    /**
	 * Returns the x
	 *
	 * @return int $x
	 */
	public function getX() {
		return $this->x;
	}

	/**
	 * Sets the x
	 *
	 * @param int $x
	 * @return void
	 */
	public function setX($x) {
		$this->x = $x;
	}

    /**
	 * Returns the y
	 *
	 * @return int $y
	 */
	public function getY() {
		return $this->y;
	}

	/**
	 * Sets the y
	 *
	 * @param int $y
	 * @return void
	 */
	public function setY($y) {
		$this->y = $y;
	}


}

?>