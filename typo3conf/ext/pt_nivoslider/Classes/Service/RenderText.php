<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 punkt.de GmbH - Karlsruhe, Germany - http://www.punkt.de
 *  Author: David Vogt
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package pt_nivoslider
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_PtNivoslider_Service_RenderText {

    public function render (Tx_PtNivoslider_Service_RenderObject $renderObject) {

        foreach ($renderObject->getImgSrcs() as $key => $src ){

            //Bild einlesen
            $imgSrc = 'uploads/tx_ptnivoslider/' . $src;
            $img =  new Tx_PtNivoslider_Service_Image($imgSrc);
            $image = $img->getImageData();

            //Farbe definieren
            $color = ImageColorAllocate($image, $renderObject->getRed(), $renderObject->getGreen(), $renderObject->getBlue());
            $ttf = 'uploads/tx_ptnivoslider/' . $renderObject->getFont();
            $texts = $renderObject->getTexts();
            $textArray = explode("\n", $texts[$key]);

            foreach ($textArray as $key => $text){

                if ($renderObject->getRenderBox()){

                    //Kasten rendern
                    $corners = imagettfbbox($renderObject->getFontsize(), 0, $ttf , $text);
                    $boxSizeX = abs($corners[0]-$corners[2]);
                    $boxSizeY = $renderObject->getFontsize();
                    $boxPadding = $renderObject->getPadding();
                    $boxColor = ImageColorAllocate($image, $renderObject->getRedBox(), $renderObject->getGreenBox(), $renderObject->getBlueBox());
                    imagefilledrectangle($image, $renderObject->getX()-$boxPadding, ($renderObject->getY()+($key*($renderObject->getFontsize()+$renderObject->getPadding())))+$boxPadding, $renderObject->getX()+$boxSizeX+$boxPadding, ($renderObject->getY()+($key*($renderObject->getFontsize()+$renderObject->getPadding())))-$boxSizeY-$boxPadding, $boxColor);
                }
            }
            
            $cornersMore = imagettfbbox(20, 0, $ttf , "mehr");
            $boxSizeXMore = abs($cornersMore[0]-$cornersMore[2]);
            $count= count($textArray);
            $moreY = ($renderObject->getY()+($count*($renderObject->getFontsize()+$renderObject->getPadding())))-$boxSizeY-$boxPadding+20;
	    imagefilledrectangle($image, $renderObject->getX()-$boxPadding, $moreY, $renderObject->getX()+$boxSizeXMore+$boxPadding, $moreY+(2*$boxPadding)+20, $boxColor);
	    $moreX = $renderObject->getX();

            foreach ($textArray as $key => $text){

                //Text draufrendern
                imagettftext($image, $renderObject->getFontsize(), 0, $renderObject->getX(), $renderObject->getY()+($key*($renderObject->getFontsize()+$renderObject->getPadding())), $color, $ttf, $text);

            }
            
            $color = ImageColorAllocate($image, 255, 255, 255);
            imagettftext($image, 20, 0, $moreX, $moreY+$boxPadding+20, $color, $ttf, "mehr");

            //Bild speichern
            $save = preg_replace("|(.*)\..*$|", "\\1_render.jpg", $imgSrc);
            imageJPEG($image, $save, 100);

        }
    }

}

?>