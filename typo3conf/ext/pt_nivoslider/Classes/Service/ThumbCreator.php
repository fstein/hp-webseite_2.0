<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 punkt.de GmbH - Karlsruhe, Germany - http://www.punkt.de
 *  Author: David Vogt
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package pt_nivoslider
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_PtNivoslider_Service_ThumbCreator {

    public function create (array $imgSrcs, $size = 100){

        foreach ($imgSrcs as $imgSrc) {

            $imgSrc = 'uploads/tx_ptnivoslider/' . $imgSrc;

            //create new Image
            $image = new Tx_PtNivoslider_Service_Image($imgSrc);

            //set thumb size
            $image = $this->setThumbSize($image, $size);

            //create thumb
            $thumb = ImageCreateTrueColor($image->getThumbWidth(), $image->getThumbHeight());
            imagecopyresampled($thumb, $image->getImageData(), 0, 0, 0, 0, $image->getThumbWidth(), $image->getThumbHeight(), $image->getImageWidth(), $image->getImageHeight());
            $image->setThumbData($thumb);

            //save thumb
            $save = preg_replace("|(.*)\..*$|", "\\1_thumb.jpg", $imgSrc);
            imageJPEG($image->getThumbData(), $save, 75);

        }

    }

    protected function setThumbSize (Tx_PtNivoslider_Service_Image $image, $size){

        $originalWidth = $image->getImageWidth();
        $originalHeight = $image->getImageHeight();

        if ($originalWidth == $originalHeight){
            $image->setThumbWidth($size);
            $image->setThumbHeight($size);
        } elseif ($originalWidth > $originalHeight){
            $image->setThumbWidth($size);
            $image->setThumbHeight(($originalHeight*$size)/$originalWidth);
        } elseif ($originalWidth < $originalHeight){
            $image->setThumbHeight($size);
            $image->setThumbWidth(($originalWidth*$size)/$originalHeight);
        } else {
            return 'error';
        }

        return $image;
    }

}

?>