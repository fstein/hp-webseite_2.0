<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 punkt.de GmbH - Karlsruhe, Germany - http://www.punkt.de
 *  Author: David Vogt
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package pt_nivoslider
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_PtNivoslider_ViewHelpers_TitleViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {

   /**
    * @param int $imgnumber
    * @param string $titletags
    * @param string $kind
    * @return string
    */
    public function render($imgnumber, $titletags, $kind) {

        if ($kind == 'div'){
            if($titletags[$imgnumber]){
                $string = '<div id="htmlcaption' . $imgnumber . '" class="nivo-html-caption">' . $titletags[$imgnumber] . '</div>';
            } else {
                $string = '';
            }
        }

        if ($kind == 'attribute'){
            if($titletags[$imgnumber]){
                $string = '#htmlcaption' . $imgnumber;
            } else {
                $string = '';
            }
        }

        return $string;
    }

}

?>