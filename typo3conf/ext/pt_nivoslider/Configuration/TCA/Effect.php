<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_ptnivoslider_domain_model_effect'] = array(
	'ctrl' => $TCA['tx_ptnivoslider_domain_model_effect']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, id, method, image, functions, click_during_animation, stop_animation, absolute_speed, relative_speed, absolute_idle_time, relative_idle_time',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, id, method, image, functions, click_during_animation, stop_animation, absolute_speed, relative_speed, absolute_idle_time, relative_idle_time, --div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_ptnivoslider_domain_model_effect',
				'foreign_table_where' => 'AND tx_ptnivoslider_domain_model_effect.pid=###CURRENT_PID### AND tx_ptnivoslider_domain_model_effect.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
        'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'id' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.id',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'method' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.method',
			'config' => array(
				'type' => 'select',
				'items' => array(
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.method.click', 'click'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.method.mouseenter', 'mouseenter'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.method.mouseleave', 'mouseleave')
                ),
				'minitems' => 1,
				'maxitems' => 1,
                'eval' => 'required'
			),
		),
        'functions' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions',
			'config' => array(
				'type' => 'select',
                'size' => 10,
				'items' => array(
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.start', 'start'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.stop', 'stop'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.jumpToPrevious', 'jumpToPrevious'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.animatePrevious', 'animatePrevious'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.jumpToNext', 'jumpToNext'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.animateNext', 'animateNext'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.jumpToFirst', 'jumpToFirst'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.animateFirst', 'animateFirst'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.jumpToLast', 'jumpToLast'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.animateLast', 'animateLast'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.jumpToImage', 'jumpToImage'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.animateImage', 'animateImage'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.changeDirection', 'changeDirection'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.setSpeed', 'setSpeed'),
                    array('LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.functions.changeSpeed', 'changeSpeed'),
                ),
				'minitems' => 1,
				'maxitems' => 10,
                'eval' => 'required'
			),
		),
        'image' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.imageNumber',
            'config'=> array(
                'type' => 'input',
            	'size' => 2
            ),
        ),
        'click_during_animation' => array(
          'exclude' => 1,
           'label' => 'LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.clickDuringAnimation',
           'config' => array(
                'type' => 'check',
                'default' => 0,
           )
        ),
        'stop_animation' => array(
          'exclude' => 1,
           'label' => 'LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.stopAnimation',
           'config' => array(
                'type' => 'check',
                'default' => 0,
           )
        ),
        'absolute_speed' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.absoluteSpeed',
            'config'=> array(
                'type' => 'input',
            	'size' => '10',
                'eval' => ', trim, int'
            ),
        ),
        'relative_speed' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.relativeSpeed',
            'config'=> array(
                'type' => 'input',
            	'size' => '10',
                'eval' => 'trim, int'
            ),
        ),
        'absolute_idle_time' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.absoluteIdleTime',
            'config'=> array(
                'type' => 'input',
            	'size' => '10',
                'eval' => 'trim, int'
            ),
        ),
        'relative_idle_time' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect.relativeIdleTime',
            'config'=> array(
                'type' => 'input',
            	'size' => '10',
                'eval' => 'trim, int'
            ),
        ),
	),
);
?>