<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_PtNivoslider_Domain_Model_Slider.
 *
 * @package pt_booklib
 * @subpackage Tests\Domain\Model
 */
class Tx_PtNivoslider_Domain_Model_SliderTest extends Tx_Extbase_Tests_Unit_BaseTestCase {

	protected $proxyClass;

	protected $proxy;

	public function SetUp() {
		$this->proxyClass = $this->buildAccessibleProxy('Tx_PtNivoslider_Domain_Model_Slider');
		$this->proxy = new $this->proxyClass();
	}

	public function testSetImgSrcsSetsPropertyImgSrcs() {
		$input = $expected = array('imgSrc 1', 'imgSrc 2');
		$this->proxy->setImgSrcs($input);
		$actual = $this->proxy->_get('imgSrcs');
		$this->assertEquals($expected, $actual);
	}


	public function testGetImgSrcsGetsPropertyImgScrs() {
		$input = $expected = array('imgSrc 1', 'imgSrc 2');
		$this->proxy->_set('imgSrcs', $input);
		$actual = $this->proxy->getImgSrcs();
		$this->assertEquals($expected, $actual);
	}


	public function testSetLinksSetsPropertyLinks() {
		$input = $expected = array('link 1', 'link 2');
		$this->proxy->setLinks($input);
		$actual = $this->proxy->_get('links');
		$this->assertEquals($expected, $actual);
	}

	public function testGetLinksGetsPropertyLinks() {
		$input = $expected = array('link 1', 'link 2');
		$this->proxy->_set('links', $input);
		$actual = $this->proxy->getLinks();
		$this->assertEquals($expected, $actual);
	}


	public function testSetTitleTagsSetsPropertyTitleTags() {
		$input = $expected = array('title tag 1', 'title tag 2');
		$this->proxy->setTitleTags($input);
		$actual = $this->proxy->_get('titleTags');
		$this->assertEquals($expected, $actual);
	}

	public function testGetTitleTagsGetsPropertyTitleTags() {
		$input = $expected = array('title tag 1', 'title tag 2');
		$this->proxy->_set('titleTags', $input);
		$actual = $this->proxy->getTitleTags();
		$this->assertEquals($expected, $actual);
	}


	public function testSetAltTagsSetsPropertyAltTags() {
		$input = $expected = array('alt tag 1', 'alt tag 2');
		$this->proxy->setAltTags($input);
		$actual = $this->proxy->_get('altTags');
		$this->assertEquals($expected, $actual);
	}


	public function testGetAltTagsGetsPropertyAltTags() {
		$input = $expected = array('alt tag 1', 'alt tag 2');
		$this->proxy->_set('altTags', $input);
		$actual = $this->proxy->getAltTags();
		$this->assertEquals($expected, $actual);
	}


    public function testSetEffectSetsPropertyEffect() {
		$input = $expected = "effect";
		$this->proxy->setEffect($input);
		$actual = $this->proxy->_get('effect');
		$this->assertEquals($expected, $actual);
    }


	public function testGetEffectGetsPropertyEffect() {
		$input = $expected = "effect";
		$this->proxy->_set('effect', $input);
		$actual = $this->proxy->getEffect();
		$this->assertEquals($expected, $actual);
	}


	public function testSetControlNavThumbsSearchSetsPropertyControlNavThumbsSearch() {
		$input = $expected = "search";
		$this->proxy->setControlNavThumbsSearch($input);
		$actual = $this->proxy->_get('controlNavThumbsSearch');
		$this->assertEquals($expected, $actual);
	}


	public function testGetControlNavThumbsSearchGetsPropertyControlNavThumbsSearch() {
		$input = $expected = "search";
		$this->proxy->_set('controlNavThumbsSearch', $input);
		$actual = $this->proxy->getControlNavThumbsSearch();
		$this->assertEquals($expected, $actual);
	}


	public function testSetControlNavThumbsReplaceSetsPropertyControlNavThumbsReplace() {
		$input = $expected = "replace";
		$this->proxy->setControlNavThumbsReplace($input);
		$actual = $this->proxy->_get('controlNavThumbsReplace');
		$this->assertEquals($expected, $actual);
	}


	public function testGetControlNavThumbsReplaceGetsPropertyControlNavThumbsReplace() {
		$input = $expected = "replace";
		$this->proxy->_set('controlNavThumbsReplace', $input);
		$actual = $this->proxy->getControlNavThumbsReplace();
		$this->assertEquals($expected, $actual);
	}


	public function testSetPrevTextSetsPropertyPrevText() {
		$input = $expected = "previous";
		$this->proxy->setPrevText($input);
		$actual = $this->proxy->_get('prevText');
		$this->assertEquals($expected, $actual);
	}


	public function testGetPrevTextGetsPropertyPrevText() {
		$input = $expected = "previous";
		$this->proxy->_set('prevText', $input);
		$actual = $this->proxy->getPrevText();
		$this->assertEquals($expected, $actual);
	}


	public function testSetNextTextSetsPropertyNextText() {
		$input = $expected = "next";
		$this->proxy->setNextText($input);
		$actual = $this->proxy->_get('nextText');
		$this->assertEquals($expected, $actual);
	}


	public function testGetNextTextGetsPropertyNextText() {
		$input = $expected = "next";
		$this->proxy->_set('nextText', $input);
		$actual = $this->proxy->getNextText();
		$this->assertEquals($expected, $actual);
	}


	public function testSetBeforeChangeSetsPropertyBeforeChange() {
		$input = $expected = "before change";
		$this->proxy->setBeforeChange($input);
		$actual = $this->proxy->_get('beforeChange');
		$this->assertEquals($expected, $actual);
	}


	public function testGetBeforeChangeGetsPropertyBeforeChange() {
		$input = $expected = "before change";
		$this->proxy->_set('beforeChange', $input);
		$actual = $this->proxy->getBeforeChange();
		$this->assertEquals($expected, $actual);
	}


	public function testSetAfterChangeSetsPropertyAfterChange() {
		$input = $expected = "after change";
		$this->proxy->setAfterChange($input);
		$actual = $this->proxy->_get('afterChange');
		$this->assertEquals($expected, $actual);
	}


	public function testGetAfterChangeGetsPropertyAfterChange() {
		$input = $expected = "after change";
		$this->proxy->_set('afterChange', $input);
		$actual = $this->proxy->getAfterChange();
		$this->assertEquals($expected, $actual);
	}


	public function testSetSlideshowEndSetsPropertySlideshowEnd() {
		$input = $expected = "end";
		$this->proxy->setSlideshowEnd($input);
		$actual = $this->proxy->_get('slideshowEnd');
		$this->assertEquals($expected, $actual);
	}


	public function testGetSlideshowEndGetsPropertySlideshowEnd() {
		$input = $expected = "end";
		$this->proxy->_set('slideshowEnd', $input);
		$actual = $this->proxy->getSlideshowEnd();
		$this->assertEquals($expected, $actual);
	}


	public function testSetLastSlideSetsPropertyLastSlide() {
		$input = $expected = "last slide";
		$this->proxy->setLastSlide($input);
		$actual = $this->proxy->_get('lastSlide');
		$this->assertEquals($expected, $actual);
	}


	public function testGetLastSlideGetsPropertyLastSlide() {
		$input = $expected = "last slide";
		$this->proxy->_set('lastSlide', $input);
		$actual = $this->proxy->getLastSlide();
		$this->assertEquals($expected, $actual);
	}


	public function testSetAfterLoadSetsPropertyAfterLoad() {
		$input = $expected = "after load";
		$this->proxy->setAfterLoad($input);
		$actual = $this->proxy->_get('afterLoad');
		$this->assertEquals($expected, $actual);
	}


	public function testGetAfterLoadGetsPropertyAfterLoad() {
		$input = $expected = "after load";
		$this->proxy->_set('afterLoad', $input);
		$actual = $this->proxy->getAfterLoad();
		$this->assertEquals($expected, $actual);
	}


    public function testSetHeightSetsPropertyHeight(){
		$input = $expected = 200;
		$this->proxy->setHeight($input);
		$actual = $this->proxy->_get('height');
		$this->assertEquals($expected, $actual);
    }


    public function testGetHeightGetsPropertyHeight() {
		$input = $expected = 200;
		$this->proxy->_set('height', $input);
		$actual = $this->proxy->getHeight();
		$this->assertEquals($expected, $actual);
    }


    public function testSetWidthSetsPropertyWidth() {
		$input = $expected = 400;
		$this->proxy->setWidth($input);
		$actual = $this->proxy->_get('width');
		$this->assertEquals($expected, $actual);
    }


    public function testGetWidthGetsPropertyWidth() {
		$input = $expected = 400;
		$this->proxy->_set('width', $input);
		$actual = $this->proxy->getWidth();
		$this->assertEquals($expected, $actual);
    }


	public function testSetSlicesSetsPropertySlices() {
		$input = $expected = 5;
		$this->proxy->setSlices($input);
		$actual = $this->proxy->_get('slices');
		$this->assertEquals($expected, $actual);
	}


	public function testGetSlicesGetsPropertySlices() {
		$input = $expected = 5;
		$this->proxy->_set('slices', $input);
		$actual = $this->proxy->getSlices();
		$this->assertEquals($expected, $actual);
	}


	public function testSetBoxColsSetsPropertyBoxCols() {
		$input = $expected = 3;
		$this->proxy->setBoxCols($input);
		$actual = $this->proxy->_get('boxCols');
		$this->assertEquals($expected, $actual);
	}

    
	public function testGetBoxColsGetsPropertyBoxCols() {
		$input = $expected = 3;
		$this->proxy->_set('boxCols', $input);
		$actual = $this->proxy->getBoxCols();
		$this->assertEquals($expected, $actual);
	}


	public function testSetBoxRowsSetsPropertyBoxRows() {
		$input = $expected = 7;
		$this->proxy->setBoxRows($input);
		$actual = $this->proxy->_get('boxRows');
		$this->assertEquals($expected, $actual);
	}


	public function testGetBoxRowsGetsPropertyBoxRows() {
		$input = $expected = 7;
		$this->proxy->_set('boxRows', $input);
		$actual = $this->proxy->getBoxRows();
		$this->assertEquals($expected, $actual);
	}


	public function testSetAnimSpeedSetsPropertyAnimSpeed() {
		$input = $expected = 1000;
		$this->proxy->setAnimSpeed($input);
		$actual = $this->proxy->_get('animSpeed');
		$this->assertEquals($expected, $actual);
	}


	public function testGetAnimSpeedGetsPropertyAnimSpeed() {
		$input = $expected = 1000;
		$this->proxy->_set('animSpeed', $input);
		$actual = $this->proxy->getAnimSpeed();
		$this->assertEquals($expected, $actual);
	}


	public function testSetPauseTimeSetsPropertyPauseTime() {
		$input = $expected = 200;
		$this->proxy->setPauseTime($input);
		$actual = $this->proxy->_get('pauseTime');
		$this->assertEquals($expected, $actual);
	}


	public function testGetPauseTimeGetsPropertyPauseTime() {
		$input = $expected = 200;
		$this->proxy->_set('pauseTime', $input);
		$actual = $this->proxy->getPauseTime();
		$this->assertEquals($expected, $actual);
	}


	public function testSetStartSlideSetsPropertyStartSlide() {
		$input = $expected = 1;
		$this->proxy->setStartSlide($input);
		$actual = $this->proxy->_get('startSlide');
		$this->assertEquals($expected, $actual);
	}


	public function testGetStartSlideGetsPropertyStartSlide() {
		$input = $expected = 1;
		$this->proxy->_set('startSlide', $input);
		$actual = $this->proxy->getStartSlide();
		$this->assertEquals($expected, $actual);
	}


	public function testSetCaptionOpacitySetsPropertyCaptionOpacity() {
		$input = $expected = 83.5234;
		$this->proxy->setCaptionOpacity($input);
		$actual = $this->proxy->_get('captionOpacity');
		$this->assertEquals($expected, $actual);
	}


	public function testGetCaptionOpacityGetsPropertyCaptionOpacity() {
		$input = $expected = 74.678;
		$this->proxy->_set('captionOpacity', $input);
		$actual = $this->proxy->getCaptionOpacity();
		$this->assertEquals($expected, $actual);
	}


	public function testSetRandomStartSetsPropertyRandomStart() {
		$input = $expected = TRUE;
		$this->proxy->setRandomStart($input);
		$actual = $this->proxy->_get('randomStart');
		$this->assertEquals($expected, $actual);
	}


	public function testGetRandomStartGetsPropertyRandomStart() {
		$input = $expected = FALSE;
		$this->proxy->_set('randomStart', $input);
		$actual = $this->proxy->getRandomStart();
		$this->assertEquals($expected, $actual);
	}


	public function testSetDirectionNavSetsPropertyDirectionNav() {
		$input = $expected = TRUE;
		$this->proxy->setDirectionNav($input);
		$actual = $this->proxy->_get('directionNav');
		$this->assertEquals($expected, $actual);
	}


	public function testGetDirectionNavGetsPropertyDirectionNav() {
		$input = $expected = FALSE;
		$this->proxy->_set('directionNav', $input);
		$actual = $this->proxy->getDirectionNav();
		$this->assertEquals($expected, $actual);
	}


	public function testSetDirectionNavHideSetsPropertyDirectionNavHide() {
		$input = $expected = TRUE;
		$this->proxy->setDirectionNavHide($input);
		$actual = $this->proxy->_get('directionNavHide');
		$this->assertEquals($expected, $actual);
	}


	public function testGetDirectionNavHideGetsPropertyDirectionNavHide() {
		$input = $expected = FALSE;
		$this->proxy->_set('directionNavHide', $input);
		$actual = $this->proxy->getDirectionNavHide();
		$this->assertEquals($expected, $actual);
	}


	public function testSetControlNavSetsPropertyControlNav() {
		$input = $expected = TRUE;
		$this->proxy->setControlNav($input);
		$actual = $this->proxy->_get('controlNav');
		$this->assertEquals($expected, $actual);
	}


	public function testGetControlNavGetsPropertyControlNav() {
		$input = $expected = FALSE;
		$this->proxy->_set('controlNav', $input);
		$actual = $this->proxy->getControlNav();
		$this->assertEquals($expected, $actual);
	}


	public function testSetControlNavThumbsSetsPropertyControlNavThumbs() {
		$input = $expected = TRUE;
		$this->proxy->setControlNavThumbs($input);
		$actual = $this->proxy->_get('controlNavThumbs');
		$this->assertEquals($expected, $actual);
    }


	public function testGetControlNavThumbsGetsPropertyControlNavThumbs() {
        $input = $expected = FALSE;
		$this->proxy->_set('controlNavThumbs', $input);
		$actual = $this->proxy->getControlNavThumbs();
		$this->assertEquals($expected, $actual);
	}


	public function testSetControlNavThumbsFromRelSetsPropertyControlNavThumbsFromRel() {
		$input = $expected = TRUE;
		$this->proxy->setControlNavThumbsFromRel($input);
		$actual = $this->proxy->_get('controlNavThumbsFromRel');
		$this->assertEquals($expected, $actual);
	}


	public function testGetControlNavThumbsFromRelGetsPropertyControlNavThumbsFromRel() {
        $input = $expected = FALSE;
		$this->proxy->_set('controlNavThumbsFromRel', $input);
		$actual = $this->proxy->getControlNavThumbsFromRel();
		$this->assertEquals($expected, $actual);
	}


	public function testSetPauseOnHoverSetsPropertyPauseOnHover() {
		$input = $expected = TRUE;
		$this->proxy->setPauseOnHover($input);
		$actual = $this->proxy->_get('pauseOnHover');
		$this->assertEquals($expected, $actual);
	}


	public function testGetPauseOnHoverGetsPropertyPauseOnHover() {
        $input = $expected = FALSE;
		$this->proxy->_set('pauseOnHover', $input);
		$actual = $this->proxy->getPauseOnHover();
		$this->assertEquals($expected, $actual);
	}


	public function testSetManualAdvanceSetsPropertyManualAdvance() {
		$input = $expected = TRUE;
		$this->proxy->setManualAdvance($input);
		$actual = $this->proxy->_get('manualAdvance');
		$this->assertEquals($expected, $actual);
	}


	public function testGetManualAdvanceGetsPropertyManualAdvance() {
        $input = $expected = FALSE;
		$this->proxy->_set('manualAdvance', $input);
		$actual = $this->proxy->getManualAdvance();
		$this->assertEquals($expected, $actual);
	}


	public function testSetKeyboardNavSetsPropertyKeyboardNav() {
		$input = $expected = TRUE;
		$this->proxy->setKeyboardNav($input);
		$actual = $this->proxy->_get('keyboardNav');
		$this->assertEquals($expected, $actual);
	}


	public function testGetKeyboardNavGetsPropertyKeyboardNav() {
        $input = $expected = FALSE;
		$this->proxy->_set('keyboardNav', $input);
		$actual = $this->proxy->getKeyboardNav();
		$this->assertEquals($expected, $actual);
	}

}
?>

