<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "pt_nivoslider".
 *
 * Auto generated 16-05-2014 21:47
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'pt_nivoslider',
	'description' => 'An extension to comfortably use nivoslider in TYPO3',
	'category' => 'plugin',
	'author' => 'David Vogt',
	'author_email' => '',
	'author_company' => '',
	'dependencies' => 'cms,extbase,fluid',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => 1,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '0.0.0',
	'constraints' => 
	array (
		'depends' => 
		array (
			'cms' => '',
			'extbase' => '',
			'fluid' => '',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
	'suggests' => 
	array (
	),
	'conflicts' => '',
	'_md5_values_when_last_written' => 'a:43:{s:12:"ext_icon.gif";s:4:"5837";s:17:"ext_localconf.php";s:4:"710a";s:14:"ext_tables.php";s:4:"11eb";s:14:"ext_tables.sql";s:4:"3ce2";s:39:"Classes/Controller/SliderController.php";s:4:"031c";s:31:"Classes/Domain/Model/Effect.php";s:4:"c9ee";s:31:"Classes/Domain/Model/Slider.php";s:4:"cbe3";s:46:"Classes/Domain/Repository/EffectRepository.php";s:4:"b109";s:25:"Classes/Service/Image.php";s:4:"d6c9";s:32:"Classes/Service/RenderObject.php";s:4:"cf99";s:30:"Classes/Service/RenderText.php";s:4:"169f";s:32:"Classes/Service/ThumbCreator.php";s:4:"eed6";s:37:"Classes/ViewHelpers/AltViewHelper.php";s:4:"63e9";s:41:"Classes/ViewHelpers/LinktagViewHelper.php";s:4:"e945";s:37:"Classes/ViewHelpers/RelViewHelper.php";s:4:"ab6c";s:37:"Classes/ViewHelpers/SrcViewHelper.php";s:4:"bd05";s:40:"Classes/ViewHelpers/StringViewHelper.php";s:4:"9a87";s:39:"Classes/ViewHelpers/TitleViewHelper.php";s:4:"36f0";s:36:"Configuration/FlexForms/Flexform.xml";s:4:"b1ac";s:28:"Configuration/TCA/Effect.php";s:4:"370a";s:38:"Configuration/TypoScript/constants.txt";s:4:"09bb";s:34:"Configuration/TypoScript/setup.txt";s:4:"0110";s:33:"Resources/nivo.slider.license.txt";s:4:"221d";s:40:"Resources/Private/Language/locallang.xml";s:4:"77fe";s:80:"Resources/Private/Language/locallang_csh_tx_ptnivoslider_domain_model_effect.xml";s:4:"a086";s:43:"Resources/Private/Language/locallang_db.xml";s:4:"fb95";s:45:"Resources/Private/Templates/Slider/index.html";s:4:"5ce5";s:36:"Resources/Public/CSS/nivo-slider.css";s:4:"61c7";s:38:"Resources/Public/CSS/pt-nivoslider.css";s:4:"dd14";s:46:"Resources/Public/Images/Doppelpfeile-Links.png";s:4:"7010";s:47:"Resources/Public/Images/Doppelpfeile-Rechts.png";s:4:"8952";s:40:"Resources/Public/Images/Pfeil-Rechts.png";s:4:"d817";s:39:"Resources/Public/Images/Pfeil-links.png";s:4:"f313";s:33:"Resources/Public/Images/pause.png";s:4:"2d58";s:32:"Resources/Public/Images/play.png";s:4:"6629";s:39:"Resources/Public/Images/rueckspulen.png";s:4:"e5ff";s:37:"Resources/Public/Images/vorspulen.png";s:4:"df86";s:39:"Resources/Public/JS/jquery-1.7.1.min.js";s:4:"ddb8";s:48:"Resources/Public/JS/jquery.nivo.slider.herbie.js";s:4:"0ec2";s:41:"Resources/Public/JS/jquery.nivo.slider.js";s:4:"0c5c";s:46:"Resources/Public/JS/jquery.nivo.slider.pack.js";s:4:"829e";s:32:"Resources/Public/JS/pt-thumbs.js";s:4:"d3dd";s:38:"Tests/Unit/Domain/Model/SliderTest.php";s:4:"712a";}',
);

?>