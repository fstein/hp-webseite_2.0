<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::configurePlugin(
	$_EXTKEY,
	'PtNivoslider',
	array(
		'Slider' => 'index'
	),
	// non-cacheable actions
	array(
		'Slider' => ''
	)
);

?>