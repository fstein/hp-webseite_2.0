<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'PtNivoslider',
	'pt_nivoslider'
);


/**
* Register plugin as page content
*/
$extensionName = t3lib_div::underscoredToUpperCamelCase($_EXTKEY);
$pluginSignature = strtolower($extensionName) . '_' . strtolower('PtNivoslider');
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature]='layout,select_key,pages';


/**
* Register flexform
*/
t3lib_extMgm::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/Flexform.xml');




t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'pt_nivoslider');


t3lib_extMgm::addLLrefForTCAdescr('tx_ptnivoslider_domain_model_effect', 'EXT:pt_nivoslider/Resources/Private/Language/locallang_csh_tx_ptnivoslider_domain_model_effect.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_ptnivoslider_domain_model_effect');
$TCA['tx_ptnivoslider_domain_model_effect'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:pt_nivoslider/Resources/Private/Language/locallang_db.xml:tx_ptnivoslider_domain_model_effect',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Effect.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_ptnivoslider_domain_model_effect.gif'
	),
);
