<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "yag".
 *
 * Auto generated 19-07-2014 15:26
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Yet Another Gallery',
	'description' => 'Scalable, powerfull, easy-to-manage Gallery Extension for TYPO3. Features Frontend with lightbox, Backend Administration Module and Themes for personalization. Uses Caching-Framework for better performance. See http://www.yag-gallery.de for demonstration.',
	'category' => 'plugin',
	'version' => '3.2.7',
	'state' => 'stable',
	'uploadfolder' => true,
	'createDirs' => '',
	'clearcacheonload' => true,
	'author' => 'Daniel Lienert, Michael Knoll',
	'author_email' => 'daniel@lienert.cc, mimi@kaktusteam.de',
	'author_company' => '',
	'constraints' => 
	array (
		'depends' => 
		array (
			'extbase' => '',
			'fluid' => '',
			'pt_extlist' => '',
			'pt_extbase' => '',
			'typo3' => '4.5.0-6.2.99',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
);

